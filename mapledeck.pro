# Build
TEMPLATE = app
TARGET = mapledeck
OBJECTS_DIR = .build
MOC_DIR = .build
RCC_DIR = .build
CONFIG += debug link_pkgconfig
CONFIG -= app_bundle
QT = core network websockets
QXT = core network web
QMAKE_CXXFLAGS += -std=c++17
DEFINES -= QT_DEPRECATED_WARNINGS
DEFINES += QT_NO_DEPRECATED_WARNINGS
MAKEFILE = Makefile.qt

# Subdirectories
include(src/web/web.pri)
include(src/mpd/mpd.pri)
include(src/metadata/metadata.pri)
include(src/jukebox/jukebox.pri)
include(src/database/database.pri)

# Data
RESOURCES += resource/mapledeck.qrc
HEADERS += src/mapledb.h   src/mediaitem.h   src/tagsm3u.h   src/jsonseq.h
SOURCES += src/mapledb.cpp src/mediaitem.cpp src/tagsm3u.cpp src/jsonseq.cpp
HEADERS += src/dirscanner.h
SOURCES += src/dirscanner.cpp

# App
HEADERS += src/mapleapp.h   src/streamtranscode.h   src/maplesession.h
SOURCES += src/mapleapp.cpp src/streamtranscode.cpp src/maplesession.cpp
SOURCES += src/main.cpp

defineTest(optionalPkgConfig) {
  !system($$pkgConfigExecutable() $${1}): return(false)
  PKGCONFIG += $${1}
  DEFINES += PC_HAVE_$${2}
  export(PKGCONFIG)
  export(DEFINES)
  return(true)
}

optionalPkgConfig(taglib,TAGLIB) {
  optionalPkgConfig(taglib-extras,TAGLIB_EXTRAS)
}

QMAKE_EXTRA_TARGETS += qmake libmdbx_Makefile libmdbx_static

qmake.depends = $$_PRO_FILE_

libmdbx_Makefile.target = libmdbx/Makefile
libmdbx_Makefile.commands = git submodule update --init --recursive

libmdbx_static.target = libmdbx/libmdbx.a
libmdbx_static.depends = libmdbx_Makefile
libmdbx_static.commands = $(MAKE) -C libmdbx libmdbx.a MDBX_BUILD_OPTIONS=-DMDBX_DEBUG=1

PRE_TARGETDEPS += libmdbx/Makefile libmdbx/libmdbx.a
LIBS += libmdbx/libmdbx.a
INCLUDEPATH += libmdbx

include(libqxt_git.pri)
