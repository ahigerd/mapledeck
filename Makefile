mapledeck: libqxt/.qmake.cache Makefile.qt FORCE
	$(MAKE) -f Makefile.qt

clean distclean: Makefile.qt
	$(MAKE) -f Makefile.qt $@

qmake: Makefile.qt

QMAKE_BIN = qmake
Makefile.qt: mapledeck.pro libqxt_git.pri $(wildcard src/*/*.pri) $(wildcard libqxt/src/*/*.pri) libqxt/.qmake.cache
	$(QMAKE_BIN)

libqxt/.qmake.cache: libqxt/configure libqxt_git.pri
	$(QMAKE_BIN) && $(MAKE) -f Makefile.qt $@

libqxt/configure:
	git clone https://bitbucket.org/ahigerd/libqxt

libqxt_update: libqxt/.qmake.cache FORCE
	cd libqxt && git pull

FORCE:
