#ifndef MD_TAGSM3U_H
#define MD_TAGSM3U_H

#include <QHash>
#include <QList>
#include <QString>
#include <QIODevice>

using TagMap = QHash<QString, QString>;

class TagsM3U {
public:
  static QString relativeTo(const QString& trackPath);

  template <typename Iter>
  static TagsM3U fromData(Iter begin, Iter end) {
    TagsM3U m3u;
    for (Iter iter = begin, lineEnd = begin; iter < end; iter = lineEnd) {
      while (lineEnd < end && *lineEnd++ != '\n') {}
      if (lineEnd >= end) break;
      m3u.parseLine(QString(iter, lineEnd));
    }
    return m3u;
  }

  TagsM3U();
  TagsM3U(QIODevice* file);
  TagsM3U(const TagsM3U& other) = default;
  TagsM3U(TagsM3U&& other) = default;
  TagsM3U& operator=(const TagsM3U& other) = default;
  TagsM3U& operator=(TagsM3U&& other) = default;

  void parseLine(const QString& line);

  QString playlistName() const;

  void set(int trackIndex, const QString& tag, const QString& value);
  inline void set(const QString& trackName, const QString& tag, const QString& value) { return set(findTrack(trackName), tag, value); }

  QString get(int trackIndex, const QString& tag) const;
  inline QString get(const QString& trackName, const QString& tag) const { return get(findTrack(trackName), tag); }

  TagMap allTags(int trackIndex) const;
  inline TagMap allTags(const QString& trackName) const { return allTags(findTrack(trackName)); }

  int numTracks() const;
  QString trackName(int trackIndex) const;
  int findTrack(const QString& trackName) const;
  void addTrack(const QString& trackName);

private:
  void populateTag(bool globalTag, QString tag, QString value);

  QList<QString> tracks;
  QHash<int, TagMap> tags;
  TagMap globalTags, fileTags, invalid;
  QString name, m3uPath;
  bool autoTrack;
};

#endif
