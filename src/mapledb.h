#ifndef MD_MAPLEDB_H
#define MD_MAPLEDB_H

#include <QObject>
#include <QSet>
#include <QFileSystemWatcher>
#include <QFileInfo>
#include <QTimer>
#include <QPointer>
#include <QMutex>
#include <QHash>
#include <QPair>
#include "mediaitem.h"
class QThread;

class MapleDB : public QObject
{
Q_OBJECT
friend class MediaItem;
friend class Scanner;
public:
  static MapleDB* instance();

  struct Filter {
    Filter(const QString& tag, const QString& value);
    Filter(const QString& tag, const QString& min, const QString& max);
    QString tag, min, max;
  };

  MapleDB(QObject* parent = nullptr);

  bool start();

  void addRootPath(const QString& path);
  void addPath(const QString& path);
  inline QString mimeType(const QString& path) const { return mimeType(QFileInfo(path)); }
  QString mimeType(const QFileInfo& info) const;
  double currentScanProgress() const;

  QList<MediaItem> rootFolders() const;

  inline QList<MediaItem> searchTracks(const QString& tag, const QString& value) const { return searchTracks(QList<Filter>{{ tag, value}}); }
  inline QList<MediaItem> searchTracks(const Filter& filter) const { return searchTracks(QList<Filter>{ filter }); }
  QList<MediaItem> searchTracks(const QList<Filter>& filters) const;

signals:
  void scanStarted();
  void scanProgress(float progress);
  void scanFinished(bool updated);

private slots:
  void throttledScan(const QString& path);
  void runThrottledScan(bool recursive = false);
  void updateAlbums();
  void updateArtists();

private:
  QSet<QString> watchedPaths;
  QSet<QString> queuedPaths;
  QFileSystemWatcher watcher;
  QTimer throttle;
  QPointer<QThread> activeThread;
  QMutex mutex;
  double progress;

  QHash<QPair<QString,QString>, QString> mimeOverrides;
};

#define mDB MapleDB::instance()

#endif
