#include "streamtranscode.h"
#include "mapleapp.h"
#include <QProcess>
#include <QFile>
#include <QMetaObject>
#include <QTimer>
#include <QtDebug>

static const QString ffmpegCmd("ffmpeg -fflags nobuffer -flags low_delay -strict experimental -probesize 32 -analyzeduration 0 -nostdin");
static const QString rawFormat("-ar 44100 -ac 2 -f s16le");
static const QString riffFormat("-f wav");

QStringList StreamTranscode::splitCommand(const QString& cmd)
{
  QStringList result;
  QChar quoteChar = '\0';
  QString prefix;
  int len = cmd.size();
  int tokenStart = 0;
  int quoteStart = 0;

  for (int i = 0; i < len; i++) {
    QChar ch = cmd[i];
    if (!quoteChar.isNull()) {
      if (ch == '\\') {
        // Inside a quoted string, backslashes are preserved literally
        // unless they escape another backslash or the quote character.
        QString escaped = cmd.mid(i + 1, 1);
        if (escaped == quoteChar || escaped == "\\") {
          prefix += cmd.mid(quoteStart, i - quoteStart) + escaped;
          i++;
          quoteStart = i + 1;
        }
      } else if (ch == quoteChar) {
        // The closing quote means the quoted contents should be added to the token.
        prefix += cmd.mid(quoteStart, i - quoteStart);
        quoteChar = '\0';
        tokenStart = i + 1;
      }
    } else if (ch.isSpace()) {
      // If the token is non-empty, add it to the result.
      if (tokenStart < i) {
        result << (prefix + cmd.mid(tokenStart, i - tokenStart));
      } else if (!prefix.isEmpty()) {
        result << prefix;
      }
      // Next token starts on the next character.
      prefix.clear();
      tokenStart = i + 1;
    } else if (ch == '"' || ch == '\'' || ch == '\\') {
      // Move the current token (if non-empty) into the prefix.
      if (tokenStart < i) {
        prefix += cmd.mid(tokenStart, i - tokenStart);
      }
      if (ch == '\\') {
        // Outside of a quoted string, backslashes escape everything.
        tokenStart = i + 1;
      } else {
        // Start a quoted string.
        quoteChar = ch;
        quoteStart = i + 1;
      }
    }
  }

  if (!quoteChar.isNull()) {
    // An unpaired quote is preserved but treats the rest of the string as quoted.
    // XXX: Is this desirable or should it be treated as if it were escaped instead?
    result << (prefix + cmd.mid(quoteStart - 1));
  } else if (tokenStart < len) {
    result << (prefix + cmd.mid(tokenStart));
  } else if (!prefix.isEmpty()) {
    result << prefix;
  }

  return result;
}

static const QHash<StreamTranscode::OutputFormat, QPair<QString, QString>> formatInfo = {
  { StreamTranscode::Wave, { "", "audio/wave" } },
  { StreamTranscode::Riff, { "", "audio/wave" } },
  { StreamTranscode::Vorbis, { "vorbis", "audio/ogg;codecs=vorbis" } },
  { StreamTranscode::Opus, { "opus", "audio/ogg;codecs=opus" } },
  { StreamTranscode::MP3, { "mp3", "audio/mpeg" } },
};

StreamTranscode* StreamTranscode::transcode(MediaItem& item, OutputFormat format, QObject* parent)
{
  item.loadMetadata();
  double duration = item.metadata.value("$duration").toDouble();
  QString mimeType = item.metadata.value("$mimetype").simplified().replace(" ", "");

  QString formatName, outputMime;
  if (format == Copy) {
    outputMime = mimeType;
  } else {
    auto formatPair = formatInfo.value(format);
    if (formatPair.second.isEmpty()) {
      return nullptr;
    }
    formatName = formatPair.first;
    outputMime = formatPair.second;
  }

  if (outputMime == mimeType) {
    // No transcoding necessary
    // TODO: is this actually safe?
    StreamTranscode* tc = new StreamTranscode(item.fullPath, parent);
    tc->outputMime = outputMime;
    return tc;
  }

  QString transcodeCmd = mApp->transcoder(mimeType);

  QString durationFlags;
  if (duration > 0) {
    durationFlags = QString("-t %1").arg(duration);
  }

  StreamTranscode* tc = nullptr;
  if (format != Wave && format != Riff && !transcodeCmd.isEmpty()) {
    tc = new StreamTranscode(item.fullPath,
      transcodeCmd,
      QStringLiteral("%1 -i - -f %2 %3 -").arg(ffmpegCmd).arg(formatName).arg(durationFlags),
      parent);
  } else {
    if (!durationFlags.isEmpty()) {
      // ffmpeg doesn't automatically fade out the looping formats it supports
      double fade = item.metadata.value("FADE").toDouble();
      if (fade) {
        durationFlags += QStringLiteral(" -af 'afade=t=out:st=%1:d=%2'").arg(duration - fade).arg(fade);
      }
    }
    if (format == Wave || format == Riff) {
      if (transcodeCmd.isEmpty()) {
        transcodeCmd = QStringLiteral("%1 -i {} %2 %3 -").arg(ffmpegCmd).arg(format == Wave ? rawFormat : riffFormat).arg(durationFlags);
      }
      tc = new StreamTranscode(item.fullPath, transcodeCmd, parent);
    } else {
      tc = new StreamTranscode(item.fullPath,
        QStringLiteral("%1 -i {} -f %2 %3 -").arg(ffmpegCmd).arg(formatName).arg(durationFlags),
        parent);
    }
  }
  tc->outputMime = outputMime;
  return tc;
}

StreamTranscode::StreamTranscode(const QString& filename, const QString& decodeCmd, const QString& encodeCmd, QObject* parent)
: QObject(parent), wroteData(false)
{
  source = new QProcess(this);
  pipe = new QProcess(this);
  source->setStandardOutputProcess(pipe);
  QObject::connect(source, SIGNAL(finished(int,QProcess::ExitStatus)), this, SLOT(handleStatus()));
  QObject::connect(pipe, SIGNAL(finished(int,QProcess::ExitStatus)), this, SLOT(handleStatus()));
  QObject::connect(pipe, SIGNAL(readyRead()), this, SLOT(didWriteData()));
  QObject::connect(pipe, SIGNAL(destroyed()), this, SLOT(deleteLater()));

  QStringList sourceArgs = splitCommand(decodeCmd);
  QString sourceCmd = sourceArgs.takeFirst();
  for (QString& arg : sourceArgs) {
    if (arg == "{}") {
      arg = filename;
    }
  }

  QStringList pipeArgs = splitCommand(encodeCmd);
  QString pipeCmd = pipeArgs.takeFirst();

  if (!qEnvironmentVariableIsEmpty("MAPLEDECK_VERBOSE")) {
    source->setProcessChannelMode(QProcess::ForwardedErrorChannel);
    pipe->setProcessChannelMode(QProcess::ForwardedErrorChannel);
    qDebug() << sourceCmd << sourceArgs;
    qDebug() << pipeCmd << pipeArgs;
  }

  source->start(sourceCmd, sourceArgs);
  pipe->start(pipeCmd, pipeArgs);
}

StreamTranscode::StreamTranscode(const QString& filename, const QString& transcodeCmd, QObject* parent)
: QObject(parent), source(nullptr), wroteData(false)
{
  pipe = new QProcess(this);
  pipe->setStandardInputFile(filename);

  QObject::connect(pipe, SIGNAL(finished(int,QProcess::ExitStatus)), this, SLOT(handleStatus()));
  QObject::connect(pipe, SIGNAL(readyRead()), this, SLOT(didWriteData()));
  QObject::connect(pipe, SIGNAL(destroyed()), this, SLOT(deleteLater()));

  QStringList pipeArgs = splitCommand(transcodeCmd);
  QString pipeCmd = pipeArgs.takeFirst();
  for (QString& arg : pipeArgs) {
    if (arg == "{}") {
      arg = filename;
    }
  }

  if (!qEnvironmentVariableIsEmpty("MAPLEDECK_VERBOSE")) {
    pipe->setProcessChannelMode(QProcess::ForwardedErrorChannel);
    qDebug() << pipeCmd << pipeArgs;
  }

  pipe->start(pipeCmd, pipeArgs);
}

StreamTranscode::StreamTranscode(const QString& filename, QObject* parent)
: QObject(parent), source(nullptr), wroteData(false)
{
  file = new QFile(filename, this);
  if (file->open(QIODevice::ReadOnly)) {
    QTimer::singleShot(0, this, SIGNAL(transcodeStarted()));
  } else {
    QTimer::singleShot(0, this, SIGNAL(transcodeFailed()));
  }
}

StreamTranscode::StreamTranscode(QIODevice* source, OutputFormat format, QObject* parent)
: QObject(parent), sourceDevice(source), wroteData(false)
{
  if (format == Wave || format == Riff) {
    qWarning("mapledeck: cannot transcode wave to wave");
    QTimer::singleShot(0, this, SIGNAL(transcodeFailed()));
    return;
  }

  pipe = new QProcess(this);

  QObject::connect(source, SIGNAL(readyRead()), this, SLOT(sourceReadyRead()));
  QObject::connect(pipe, SIGNAL(bytesWritten(qint64)), this, SLOT(sourceWritten(qint64)));
  QObject::connect(pipe, SIGNAL(finished(int,QProcess::ExitStatus)), this, SLOT(handleStatus()));
  QObject::connect(pipe, SIGNAL(readyRead()), this, SLOT(didWriteData()));
  QObject::connect(pipe, SIGNAL(destroyed()), this, SLOT(deleteLater()));

  auto formatPair = formatInfo.value(format);
  QString formatName = formatPair.first;
  outputMime = formatPair.second;
  QString transcodeCmd = QStringLiteral("%1 %2 -i - -f %3 -").arg(ffmpegCmd).arg(rawFormat).arg(formatName);

  QStringList pipeArgs = splitCommand(transcodeCmd);
  QString pipeCmd = pipeArgs.takeFirst();

  if (!qEnvironmentVariableIsEmpty("MAPLEDECK_VERBOSE")) {
    pipe->setProcessChannelMode(QProcess::ForwardedErrorChannel);
    qDebug() << "|" << pipeCmd << pipeArgs;
  }

  pipe->start(pipeCmd, pipeArgs);
}

StreamTranscode::~StreamTranscode()
{
  qDebug() << "~StreamTranscode" << this;
}

QIODevice* StreamTranscode::output() const
{
  if (pipe.isNull()) {
    return file;
  }
  return pipe;
}

QString StreamTranscode::mimeType() const
{
  return outputMime;
}

void StreamTranscode::sourceReadyRead()
{
  QByteArray data = sourceDevice->read(sourceDevice->bytesAvailable());
  pipe->write(data);
}

void StreamTranscode::sourceWritten(qint64 /* bytes */)
{
  qint64 avail = sourceDevice ? sourceDevice->bytesAvailable() : 0;
  if (avail > 0) {
    QByteArray data = sourceDevice->read(avail > 32768 ? 32768 : avail);
    pipe->write(data);
  }
}

void StreamTranscode::didWriteData()
{
  if (!wroteData) {
    wroteData = true;
    QObject::disconnect(pipe, SIGNAL(readyRead()), this, SLOT(didWriteData()));
    emit transcodeStarted();
  }
}

void StreamTranscode::handleStatus()
{
  qDebug() << "handle status" << source << (source ? source->state() : QProcess::NotRunning) << pipe << (pipe ? pipe->state() : QProcess::NotRunning);;
  if (!pipe || pipe->state() == QProcess::NotRunning) {
    if (!wroteData) {
      // The pipe was closed without reading any data.
      emit transcodeFailed();
    } else {
      emit transcodeFinished();
    }
    if (source) {
      source->kill();
    }
    if (!pipe) {
      deleteLater();
    }
  }
}
