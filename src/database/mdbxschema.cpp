#include "mdbxschema.h"

namespace {

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
static const MdbxSchema schemas[] {
  {
    .tableName = "paths",
    .primaryKey = "path",
    .indexedColumns = {
      "parent",
    },
    // modified
  },
  {
    .tableName = "tracks",
    .relationships = {
      "paths",
      "collections",
    },
    .indexedColumns = {
      "mimetype",
      "no_album", // 1 = no tag, 2 = tag but no ID
      "no_artist",
    },
    .indexedSets = {
      "tags",
    },
  },
  {
    .tableName = "collections",
    .relationships = {
      "paths",
    },
    .indexedColumns = {
      "parent", // another collection
      "type",
      "name",
    },
    .indexedSets = {
      "tags",
    },
    // items
    // folderId
  },
  {
    .tableName = "users",
    .primaryKey = "username",
  },
  {
    .tableName = "sessions",
    .primaryKey = "cookie",
    .relationships = {
      "users",
    },
    .indexedColumns = {
      "type",
    },
  },
  {
    .tableName = "tagtypes",
    .primaryKey = "tag",
  },
};
#pragma GCC diagnostic pop

static std::map<std::string, const MdbxSchema*> makeMap()
{
  std::map<std::string, const MdbxSchema*> map;
  for (const MdbxSchema& s : schemas) {
    map.emplace(s.tableName, &s);
  }
  return map;
}

struct MdbxSchemaRepositoryMaker : public MdbxSchemaRepository {
  MdbxSchemaRepositoryMaker() : MdbxSchemaRepository(makeMap()) {}
};

}

static MdbxSchemaRepositoryMaker repo;

MdbxSchemaRepository::MdbxSchemaRepository(std::map<std::string, const MdbxSchema*>&& schemas)
: schemas(schemas)
{
  // initializers only
}

const MdbxSchemaRepository& MdbxSchemaRepository::instance()
{
  return repo;
}

const MdbxSchema* MdbxSchemaRepository::operator[](const std::string& name) const
{
  auto iter = schemas.find(name);
  if (iter == schemas.end()) {
    return nullptr;
  }
  return iter->second;
}

std::vector<const MdbxSchema*> MdbxSchemaRepository::allSchemas() const
{
  std::vector<const MdbxSchema*> result;
  for (const auto& iter : schemas) {
    result.push_back(iter.second);
  }
  return result;
}
