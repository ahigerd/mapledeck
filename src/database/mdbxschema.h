#ifndef MD_MDBXSCHEMA_H
#define MD_MDBXSCHEMA_H

#include <vector>
#include <string>
#include <map>

struct MdbxSchema
{
  const std::string tableName;
  const std::string primaryKey;
  const std::vector<std::string> relationships;
  const std::vector<std::string> indexedColumns;
  const std::vector<std::string> indexedSets;
};

class MdbxSchemaRepository
{
protected:
  MdbxSchemaRepository(std::map<std::string, const MdbxSchema*>&&);
  const std::map<std::string, const MdbxSchema*> schemas;

public:
  static const MdbxSchemaRepository& instance();

  std::vector<const MdbxSchema*> allSchemas() const;
  const MdbxSchema* operator[](const std::string&) const;
};

#define mRepo (MdbxSchemaRepository::instance())

#endif
