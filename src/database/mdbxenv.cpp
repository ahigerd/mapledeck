#include "mdbxenv.h"
#include "mdbxtable.h"
#include <QIODevice>
#include <QDataStream>
#include <QMutex>
#include <QThread>

static QMutex writersLock;
static QHash<Qt::HANDLE, QList<MdbxTxn*>> writers;
static MdbxEnv* mdbxEnv = nullptr;
static const MDBX_env_flags_t opFlags = MDBX_NOSUBDIR | MDBX_EXCLUSIVE;

MdbxEnv* MdbxEnv::instance()
{
  return mdbxEnv;
}

MdbxEnv::MdbxEnv(const QString& path)
{
  auto allSchemas = mRepo.allSchemas();

  int numMaps = 0;
  for (const MdbxSchema* schema : allSchemas) {
    numMaps += schema->primaryKey.empty() ? 1 : 2;
    numMaps += schema->relationships.size();
    numMaps += schema->indexedColumns.size();
    numMaps += schema->indexedSets.size();
  }

  mdbx::env_managed::create_parameters createParams;
  createParams.use_subdirectory = false;

  mdbx::env::operate_parameters opParams(numMaps * 20, 100);
  opParams.mode = mdbx::env::mode::write_mapped_io;
  opParams.options.exclusive = true;
  opParams.options.orphan_read_transactions = true;
  //opParams.options.nested_write_transactions = true;

  env = mdbx::env_managed(path.toStdString(), createParams, opParams, false);

  mdbx::txn_managed txn(env.start_write());

  for (const MdbxSchema* schema : allSchemas) {
    ensureCreated(schema, &txn);
  }

  txn.commit();
  env.sync_to_disk();

  for (const mdbx::map_handle& handle : initHandles) {
    env.close_map(handle);
  }
  initHandles.clear();

  mdbxEnv = this;
}

void MdbxEnv::ensureCreated(const MdbxSchema* schema, mdbx::txn* txn)
{
  ensureCreated(schema->tableName, txn, false, true);
  if (!schema->primaryKey.empty()) {
    ensureCreated(MdbxTable::indexName(schema, schema->primaryKey, "pk"), txn, false, false);
  }
  for (const std::string& rel : schema->relationships) {
    ensureCreated(MdbxTable::indexName(schema, rel, "rel"), txn, true, true);
  }
  for (const std::string& col : schema->indexedColumns) {
    ensureCreated(MdbxTable::indexName(schema, col), txn, true, false);
  }
  for (const std::string& set : schema->indexedSets) {
    ensureCreated(MdbxTable::indexName(schema, set), txn, true, false);
  }
}

void MdbxEnv::ensureCreated(const std::string& tableName, mdbx::txn* txn, bool multi, bool intKeys)
{
  initHandles << txn->create_map(
    tableName,
    intKeys ? mdbx::key_mode::ordinal : mdbx::key_mode::usual,
    multi ? mdbx::value_mode::multi : mdbx::value_mode::single
  );
}

MdbxTxn::MdbxTxn(bool isReadWrite)
{
  if (isReadWrite) {
    QMutexLocker locker(&writersLock);

    Qt::HANDLE thread = QThread::currentThreadId();
    QList<MdbxTxn*> nested = writers.value(thread);
    if (nested.count()) {
      txn = nested.back()->txn.start_nested();
    } else {
      txn = (*mdbxEnv)->start_write();
    }
    writers[thread] = (nested << this);
  } else {
    txn = (*mdbxEnv)->start_read();
  }
  mdbx_txn_set_userctx(txn, this);
}

MdbxTxn::~MdbxTxn()
{
  dispose(txn && txn.is_readwrite());
}

void MdbxTxn::commit()
{
  txn.commit();
  dispose(true);
}

void MdbxTxn::dispose(bool isWriter)
{
  if (txn) {
    txn.abort();
  }
  for (const mdbx::map_handle& map : handles) {
    (*mdbxEnv)->close_map(map);
  }
  handles.clear();
  if (isWriter) {
    QMutexLocker locker(&writersLock);
    writers[QThread::currentThreadId()].removeAll(this);
  }
}

qint64 MdbxTxn::nextSequence(const mdbx::map_handle& map)
{
  if (!txn || !txn.is_readwrite()) {
    throw mdbx::error(MDBX_BAD_TXN);
  }
  qint64 id = txn.sequence(map, 1);
  if (!id) {
    id = txn.sequence(map, 1);
  }
  return id;
}

mdbx::map_handle MdbxTxn::open(const std::string& tableName)
{
  return open(tableName, mdbx::key_mode::ordinal, mdbx::value_mode::single);
}

mdbx::map_handle MdbxTxn::open(const MdbxSchema* schema, const std::string& key)
{
  bool isPrimary = key == schema->primaryKey;
  bool isRelationship = !isPrimary && std::find(schema->relationships.begin(), schema->relationships.end(), key) != schema->relationships.end();
  mdbx::key_mode keyMode = mdbx::key_mode::usual;
  mdbx::value_mode valueMode = mdbx::value_mode::multi;
  std::string prefix("idx");
  if (isPrimary) {
    prefix = "pk";
    valueMode = mdbx::value_mode::single;
  } else if (isRelationship) {
    prefix = "rel";
    keyMode = mdbx::key_mode::ordinal;
  }
  return open(prefix + "_" + schema->tableName + "_" + key, keyMode, valueMode);
}

mdbx::map_handle MdbxTxn::open(const std::string& tableName, mdbx::key_mode keyMode, mdbx::value_mode valueMode)
{
  QByteArray tn = QByteArray::fromStdString(tableName);
  mdbx::map_handle handle = handles.value(tn);
  if (!handle) {
    handle = txn.open_map(tableName, keyMode, valueMode);
    handles[tn] = handle;
  }
  return handle;
}

mdbx::slice MdbxTxn::get(const mdbx::map_handle& map, const mdbx::slice& key)
{
  return txn.get(map, key);
}

void MdbxTxn::erase(const mdbx::map_handle& map, const mdbx::slice& key)
{
  txn.erase(map, key);
}

void MdbxTxn::erase(const mdbx::map_handle& map, const mdbx::slice& key, const mdbx::slice& value)
{
  txn.erase(map, key, value);
}

void MdbxTxn::upsert(const mdbx::map_handle& map, const mdbx::slice& key, const mdbx::slice& value)
{
  txn.upsert(map, key, value);
}

void MdbxTxn::upsert(const mdbx::map_handle& map, const buffer& key, const buffer& value)
{
  txn.upsert(map, key, value);
}

buffer MdbxTxn::slice(signed long long v, bool)
{
  return key_from(v);
}

buffer MdbxTxn::slice(const QVariant& v, bool asKey)
{
  if (asKey) {
    int type = v.userType();
    if (type == qMetaTypeId<QString>()) {
      QByteArray ba = v.toString().toUtf8();
      return buffer(ba.data(), ba.size(), false);
    } else if (type == qMetaTypeId<QByteArray>()) {
      QByteArray ba = v.toByteArray();
      return buffer(ba.data(), ba.size(), false);
    } else if (type == qMetaTypeId<quint64>()) {
      return slice(v.value<quint64>());
    } else if (v.canConvert<qint64>()) {
      return slice(v.value<qint64>());
    }
  }
  QByteArray ba;
  QDataStream ds(&ba, QIODevice::WriteOnly);
  ds << v;
  return buffer(ba.data(), ba.size(), false);
}

buffer MdbxTxn::slice(const QByteArray& v, bool asKey)
{
  if (asKey) {
    return buffer(v.data(), v.size(), false);
  }
  return slice(QVariant(v));
}

qint64 MdbxTxn::fromKey(const mdbx::slice& k)
{
  if (qintptr(k.byte_ptr()) & 0x3) {
    buffer alignedKey = k;
    return mdbx_int64_from_key(alignedKey);
  }
  return mdbx_int64_from_key(k);
}
