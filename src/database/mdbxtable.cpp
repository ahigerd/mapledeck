#include "mdbxtable.h"
#include "mdbxschema.h"
#include "mdbx.h"
#include "mdbx.h"
#include <QDataStream>
#include <stdexcept>
#include <algorithm>
#include <set>

template <typename T, typename K>
static inline bool listContains(const T& container, const K& value)
{
  auto end = container.end();
  return std::find(container.begin(), end, value) != end;
}

static qint64 findRelationship(const MdbxSchema* schema, const MdbxSchema* other, qint64 srcId)
{
  if (schema && other) {
    if (listContains(schema->relationships, other->tableName)) {
      return srcId;
    } else if (listContains(other->relationships, schema->tableName)) {
      return -srcId;
    }
  }
  return 0;
}

static mdbx::map_handle openLinkMap(MdbxTxn* txn, const MdbxSchema* left, const MdbxSchema* right, qint64 id)
{
  if (id > 0) {
    return txn->open(left, right->tableName);
  } else {
    return txn->open(right, left->tableName);
  }
}

static QByteArray variantToBytes(const QVariant& v)
{
  buffer buf = MdbxTxn::slice(v, true);
  return QByteArray(buf.char_ptr(), buf.size());
}

std::string MdbxTable::indexName(const MdbxSchema* schema, const std::string& key, const std::string& prefix)
{
  return prefix + "_" + schema->tableName + "_" + key;
}

MdbxRecord::MdbxRecord()
: schema(nullptr), _id(0)
{
  // initializers only
}

MdbxRecord::MdbxRecord(const MdbxSchema* schema, qint64 id, const mdbx::map_handle& map, MdbxTxn* _txn, bool isNew)
: schema(schema), txn(_txn), map(map), _id(id), dirty(isNew)
{
  mdbx::cursor_managed cursor(txn->cursor(map));
  cursor.find(key_from(id), false);
  load(&cursor);
}

MdbxRecord::MdbxRecord(const MdbxSchema* schema, MdbxTxn* _txn, mdbx::cursor* cursor)
: schema(schema), txn(_txn), map(cursor->map()), dirty(false)
{
  load(cursor);
}

bool MdbxRecord::operator==(const MdbxRecord& other) const
{
  if (!isValid()) {
    return !other.isValid();
  } else if (!other.isValid()) {
    return false;
  }
  return schema == other.schema && _id == other._id;
}

void MdbxRecord::load(mdbx::cursor* cursor)
{
  try {
    mdbx::cursor::move_result result = cursor->current(false);
    if (result) {
      _id = MdbxTxn::fromKey(result.key);
      QByteArray ba = QByteArray::fromRawData(result.value.char_ptr(), result.value.length());
      QDataStream ds(&ba, QIODevice::ReadOnly);
      data = QVariant(ds).toHash();
    }
  } catch (mdbx::no_data& e) {
    if (!dirty) {
      // If the record is already dirty, it's newly created.
      // If it's not dirty, then that means it doesn't exist.
      schema = nullptr;
      _id = 0;
      data.clear();
    }
  }
}

void MdbxRecord::save()
{
  if (!txn || !txn->isReadWrite()) {
    throw mdbx::error(MDBX_BAD_TXN);
  }
  if (dirty) {
    // TODO: validate pkey uniqueness
    txn->upsert(map, _id, data);
    for (const QString& idx : dirtyIndexes.keys()) {
      saveIndex(idx);
    }
    dirtyIndexes.clear();
    dirty = false;
  }
  // TODO: is it worth optimizing?
  for (const Link& link : pendingUnlinks) {
    qint64 rel = findRelationship(schema, link.schema, _id);
    mdbx::map_handle idxMap = openLinkMap(txn, schema, link.schema, rel);
    txn->erase(idxMap, rel, link.id);
    txn->erase(idxMap, (rel < 0 ? link.id : -link.id), _id);
  }
  for (const Link& link : pendingLinks) {
    qint64 rel = findRelationship(schema, link.schema, _id);
    mdbx::map_handle idxMap = openLinkMap(txn, schema, link.schema, rel);
    txn->upsert(idxMap, rel, link.id);
    txn->upsert(idxMap, (rel < 0 ? link.id : -link.id), _id);
  }
  pendingUnlinks.clear();
  pendingLinks.clear();
}

void MdbxRecord::wipeIndex(const std::string& key, const mdbx::slice& slice)
{
  mdbx::map_handle idxMap(txn->open(schema, key));
  QString qKey = QString::fromStdString(key);
  QSet<QByteArray> values = dirtyIndexes.value(qKey);
  if (values.isEmpty()) {
    values = buildIndex(qKey);
  }
  for (const QByteArray& value : values) {
    txn->erase(idxMap, value, slice);
  }
}

void MdbxRecord::erase()
{
  if (!txn || !txn->isReadWrite()) {
    throw mdbx::error(MDBX_BAD_TXN);
  }
  pendingLinks.clear();
  pendingUnlinks.clear();

  buffer keySlice = MdbxTxn::slice(_id);

  if (!schema->primaryKey.empty()) {
    wipeIndex(schema->primaryKey, keySlice);
  }
  for (const std::string& idx : schema->indexedColumns) {
    wipeIndex(idx, keySlice);
  }
  for (const std::string& idx : schema->indexedSets) {
    wipeIndex(idx, keySlice);
  }
  for (const std::string& otherTable : schema->relationships) {
    const MdbxSchema* other = mRepo[otherTable];
    qint64 rel = findRelationship(schema, other, _id);
    mdbx::map_handle idxMap = openLinkMap(txn, schema, other, rel);
    mdbx::cursor_managed lookup(txn->cursor(idxMap));
    QList<buffer> reverse;
    buffer relSlice = MdbxTxn::slice(rel);
    mdbx::cursor::move_result res(lookup.find(relSlice, false));
    while (res) {
      if (rel < 0) {
        reverse << res.value;
      } else {
        reverse << MdbxTxn::slice(-MdbxTxn::fromKey(res.value));
      }
      res = lookup.to_current_next_multi(false);
    }
    txn->erase(idxMap, relSlice);
    for (const buffer& revSlice : reverse) {
      txn->erase(idxMap, revSlice, keySlice);
    }
  }

  dirtyIndexes.clear();
  dirty = false;
  schema = nullptr;
  _id = 0;
}

QSet<QByteArray> MdbxRecord::buildIndex(const QString& key) const
{
  QVariant value = data[key];
  QSet<QByteArray> values;
  const int maxkey = 1000;
  if (value.canConvert<QVariantHash>()) {
    QVariantHash hash(value.value<QVariantHash>());
    for (const QString& subkey : hash.keys()) {
      values << (subkey.toUtf8() + "\t" + variantToBytes(hash[subkey])).left(maxkey);
    }
  } else if (value.canConvert<QVariantList>()) {
    for (const QVariant& v : value.value<QVariantList>()) {
      values << variantToBytes(v).left(maxkey);
    }
  } else if (value.isValid()) {
    values << variantToBytes(value).left(maxkey);
  }
  return values;
}

void MdbxRecord::saveIndex(const QString& key)
{
  mdbx::map_handle idxMap(txn->open(schema, key));
  QSet<QByteArray> before(dirtyIndexes[key]);
  QSet<QByteArray> values(buildIndex(key));
  for (const QByteArray& toRemove : before - values) {
    txn->erase(idxMap, toRemove, _id);
  }
  for (const QByteArray& toInsert : values - before) {
    txn->upsert(idxMap, toInsert, _id);
  }
}

QVariant MdbxRecord::value(const QString& key, const QString& subkey) const
{
  if (!subkey.isEmpty()) {
    QVariantHash hash(data[key].value<QVariantHash>());
    return hash[subkey];
  }
  return data[key];
}

void MdbxRecord::setValue(const QString& key, const QVariant& value)
{
  if (value == data[key]) {
    // unchanged
    return;
  }
  std::string sKey(key.toStdString());
  bool isIndex = false;
  if (schema->primaryKey == sKey) {
    isIndex = true;
  } else {
    bool isSet = listContains(schema->indexedSets, sKey);
    isIndex = isSet || listContains(schema->indexedColumns, sKey);
    if (isSet && !value.isNull()) {
      if (!value.canConvert<QVariantHash>() && !value.canConvert<QVariantList>()) {
        throw mdbx::error(MDBX_INCOMPATIBLE);
      }
    }
  }
  if (isIndex && !dirtyIndexes.contains(key)) {
    dirtyIndexes[key] = buildIndex(key);
  }
  if (value.isNull()) {
    data.remove(key);
  } else {
    data[key] = value;
  }
  dirty = true;
}

void MdbxRecord::setValue(const QString& key, const QString& subkey, const QVariant& value)
{
  QVariant existing(data[key]);
  if (!existing.isNull() && !existing.canConvert<QVariantHash>()) {
    throw mdbx::error(MDBX_INCOMPATIBLE);
  }
  QVariantHash hash(existing.value<QVariantHash>());
  if (value.isNull()) {
    hash.remove(subkey);
  } else {
    hash[subkey] = value;
  }
  setValue(key, hash);
}

void MdbxRecord::merge(const QVariantHash& values)
{
  for (const QString& key : values.keys()) {
    setValue(key, values[key]);
  }
}

void MdbxRecord::remove(const QString& key, const QString& subkey)
{
  if (subkey.isEmpty()) {
    setValue(key, QVariant());
  } else {
    setValue(key, subkey, QVariant());
  }
}

QList<qint64> MdbxRecord::links(const QString& tableName) const
{
  QSet<qint64> result = QSet<qint64>::fromList(MdbxTable(this).searchLinks(tableName, _id));
  for (const Link& link : pendingLinks) {
    if (link.schema->tableName == tableName.toStdString()) {
      result << link.id;
    }
  }
  for (const Link& link : pendingUnlinks) {
    if (link.schema->tableName == tableName.toStdString()) {
      result -= link.id;
    }
  }
  return result.values();
}

void MdbxRecord::link(const QString& tableName, qint64 id)
{
  Link link{ mRepo[tableName.toStdString()], id };
  if (!link.schema) {
    throw mdbx::error(MDBX_INCOMPATIBLE);
  }
  pendingLinks.removeAll(link);
  pendingUnlinks.removeAll(link);
  pendingLinks << link;
}

void MdbxRecord::unlink(const QString& tableName, qint64 id)
{
  Link link{ mRepo[tableName.toStdString()], id };
  if (!link.schema) {
    throw mdbx::error(MDBX_INCOMPATIBLE);
  }
  pendingLinks.removeAll(link);
  pendingUnlinks.removeAll(link);
  pendingUnlinks << link;
}

MdbxTable::MdbxTable(const QString& tableName)
: schema(mRepo[tableName.toStdString()]), mTxn(mEnv->startRead()), txn(&mTxn), map(txn->open(tableName))
{
  // initializers only
}

MdbxTable::MdbxTable(const QString& tableName, MdbxTxn* _txn)
: schema(mRepo[tableName.toStdString()]), txn(_txn), map(txn->open(tableName))
{
  // initializers only
}

MdbxTable::MdbxTable(const MdbxRecord* rec)
: schema(rec->schema), txn(rec->txn), map(rec->map)
{
  // initializers only
}

qint64 MdbxTable::find(const QString& pKey) const
{
  if (schema->primaryKey.empty()) {
    return 0;
  }
  mdbx::map_handle idxMap(txn->open(schema, schema->primaryKey));
  mdbx::cursor_managed idxCursor(txn->cursor(idxMap));
  std::string sKey = pKey.toStdString();
  mdbx::cursor::move_result idxResult = idxCursor.find(mdbx::slice(sKey), false);
  if (!idxResult) {
    return 0;
  }
  return MdbxTxn::fromKey(idxResult.value);
}

MdbxRecord MdbxTable::get(const QString& pKey) const
{
  qint64 id = find(pKey);
  if (!id) {
    return MdbxRecord();
  }
  return get(id);
}

MdbxRecord MdbxTable::get(qint64 id) const
{
  mdbx::map_handle map(txn->open(schema));
  mdbx::cursor_managed cursor(txn->cursor(map));
  cursor.find(key_from(id), false);
  return MdbxRecord(schema, txn, &cursor);
}

QList<qint64> MdbxTable::search(const QString& column, const QString& value) const
{
  return search(column.toStdString(), MdbxTxn::slice(value, true));
}

QList<qint64> MdbxTable::search(const QString& column, qint64 value) const
{
  return search(column.toStdString(), MdbxTxn::slice(value, true));
}

QList<qint64> MdbxTable::search(const QString& column, const QString& key, const QVariant& value) const
{
  return search(column.toStdString(), MdbxTxn::slice(key.toUtf8() + "\t" + variantToBytes(value), true));
}

QList<qint64> MdbxTable::search(const std::string& column, const mdbx::slice& value) const
{
  QList<qint64> result;

  mdbx::map_handle idxMap(txn->open(schema, column));
  mdbx::cursor_managed idxCursor(txn->cursor(idxMap));
  mdbx::cursor::move_result idxResult = idxCursor.find(value, false);
  while (idxResult) {
    result << MdbxTxn::fromKey(idxResult.value);
    idxResult = idxCursor.to_current_next_multi(false);
  }

  return result;
}

QSet<qint64> MdbxTable::searchRange(const QString& column, qint64 min, qint64 max) const
{
  return searchRange(column.toStdString(), MdbxTxn::slice(min, true), MdbxTxn::slice(max, true));
}

QSet<qint64> MdbxTable::searchRange(const QString& column, const QString& key, qint64 min, qint64 max) const
{
  buffer minKey = MdbxTxn::slice(key.toUtf8() + "\t" + variantToBytes(min), true);
  buffer maxKey = MdbxTxn::slice(key.toUtf8() + "\t" + variantToBytes(max), true);
  return searchRange(column.toStdString(), MdbxTxn::slice(min, true), MdbxTxn::slice(max, true));
}

QSet<qint64> MdbxTable::searchRange(const QString& column, const QString& min, const QString& max) const
{
  return searchRange(column.toStdString(), MdbxTxn::slice(min, true), MdbxTxn::slice(max, true));
}

QSet<qint64> MdbxTable::searchRange(const QString& column, const QString& key, const QString& min, const QString& max) const
{
  buffer minKey = MdbxTxn::slice(key.toUtf8() + "\t" + variantToBytes(min), true);
  buffer maxKey = MdbxTxn::slice(key.toUtf8() + "\t" + variantToBytes(max), true);
  return searchRange(column.toStdString(), MdbxTxn::slice(min, true), MdbxTxn::slice(max, true));
}

QSet<qint64> MdbxTable::searchRange(const std::string& column, const mdbx::slice& min, const mdbx::slice& max) const
{
  QSet<qint64> result;

  mdbx::map_handle idxMap(txn->open(schema, column));
  mdbx::cursor_managed idxCursor(txn->cursor(idxMap));
  mdbx::cursor::move_result idxResult = idxCursor.lower_bound(min, false);
  do {
    if (idxResult.key > max) {
      break;
    }
    do {
      result << MdbxTxn::fromKey(idxResult.value);
      idxResult = idxCursor.to_current_next_multi(false);
    } while (idxResult);
    idxResult = idxCursor.to_next_first_multi(false);
  } while (idxResult);

  return result;
}

MdbxRecord MdbxTable::create(const QVariantHash& data)
{
  qint64 newId = txn->nextSequence(map);
  MdbxRecord record(schema, newId, map, txn, true);
  if (!data.isEmpty()) {
    record.merge(data);
  }
  record.save();
  return record;
}

int MdbxTable::rowCount() const
{
  mdbx::map_handle map(txn->open(schema));
  return txn->getTxn()->get_map_stat(map).ms_entries;
}

qint64 MdbxTable::maxId() const
{
  mdbx::map_handle map(txn->open(schema));
  return txn->getTxn()->sequence(map);
}

MdbxTable::Iterator::Iterator(const Iterator& other)
: schema(other.schema), cursor(other.cursor.txn().open_cursor(other.cursor.map())), txn(nullptr)
{
  // initializers only
}

MdbxTable::Iterator::Iterator(const MdbxSchema* schema, MdbxTxn* txn, mdbx::cursor_managed&& cursor)
: schema(schema), cursor(std::move(cursor)), txn(txn)
{
  // initializers only
}

MdbxTable::Iterator::operator bool() const
{
  return schema && cursor;
}

bool MdbxTable::Iterator::operator==(const Iterator& other) const
{
  if (!*this) {
    return !!other;
  }

  if (schema != other.schema) {
    return false;
  }

  auto lhs = cursor.current(false);
  auto rhs = other.cursor.current(false);

  return lhs.key == rhs.key && lhs.value == rhs.value;

  return false;
}

MdbxTable::Iterator& MdbxTable::Iterator::operator=(const Iterator& other)
{
  schema = other.schema;
  cursor = other.cursor.txn().open_cursor(other.cursor.map());
  return *this;
}

MdbxRecord MdbxTable::Iterator::operator*()
{
  return MdbxRecord(schema, txn, &cursor);
}

MdbxTable::Iterator& MdbxTable::Iterator::operator++()
{
  if (!cursor.to_next(false) || cursor.eof()) {
    schema = nullptr;
  }
  return *this;
}

MdbxTable::Iterator MdbxTable::begin()
{
  mdbx::map_handle map(txn->open(schema->tableName));
  mdbx::cursor_managed cursor = txn->cursor(map);
  if (!cursor.to_first(false)) {
    return end();
  }
  return Iterator(schema, txn, std::move(cursor));
}

MdbxTable::Iterator MdbxTable::end()
{
  return Iterator();
}

QList<qint64> MdbxTable::searchLinks(const QString& tableName, qint64 _id) const
{
  QList<qint64> result;
  const MdbxSchema* other = mRepo[tableName.toStdString()];
  qint64 rel = findRelationship(schema, other, _id);
  if (rel == 0) {
    // No relationship means no links
    return result;
  }

  mdbx::map_handle otherMap(txn->open(other->tableName));
  mdbx::cursor_managed lookup(txn->cursor(otherMap));

  mdbx::map_handle idxMap = openLinkMap(txn, schema, other, rel);
  mdbx::cursor_managed cursor(txn->cursor(idxMap));

  mdbx::cursor::move_result cRes = cursor.find(key_from(rel), false);
  while (cRes) {
    qint64 cKey = MdbxTxn::fromKey(cRes.value);
    mdbx::cursor::move_result oRes = lookup.find(cRes.value, false);
    if (oRes) {
      result << cKey;
    }
    cRes = cursor.to_current_next_multi(false);
  }

  return result;
}

bool MdbxTable::exists(qint64 id) const
{
  mdbx::map_handle map(txn->open(schema));
  mdbx::cursor_managed cursor(txn->cursor(map));
  return cursor.find(key_from(id), false);
}

bool MdbxTable::exists(const QString& pKey) const
{
  return find(pKey) != 0;
}

int MdbxTable::count(const QString& column, const QString& value) const
{
  return count(column.toStdString(), MdbxTxn::slice(value));
}

int MdbxTable::count(const QString& column, qint64 value) const
{
  return count(column.toStdString(), MdbxTxn::slice(value));
}

int MdbxTable::count(const std::string& column, const mdbx::slice& value) const
{
  mdbx::map_handle idxMap(txn->open(schema, column));
  mdbx::cursor_managed idxCursor(txn->cursor(idxMap));
  mdbx::cursor::move_result idxResult = idxCursor.find(value, false);
  if (idxResult) {
    return idxCursor.count_multivalue();
  }
  return 0;
}
