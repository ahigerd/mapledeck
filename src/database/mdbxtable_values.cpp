#ifndef MD_MDBXTABLE_VALUES_CPP
#define MD_MDBXTABLE_VALUES_CPP

#ifdef _GCCLINT
#include "mdbxtable.h"
#endif
#ifdef MD_MDBXTABLE_H
#include <QDataStream>
#include <QIODevice>
#include <iostream>

namespace MdbxTableValues {
  template<typename T>
  inline T value(const mdbx::slice& v);

  template<>
  inline QByteArray value<QByteArray>(const mdbx::slice& v) {
    return QByteArray::fromRawData(v.char_ptr(), v.length());
  }

  template<>
  inline QVariant value<QVariant>(const mdbx::slice& v) {
    QByteArray ba = value<QByteArray>(v);
    QDataStream ds(&ba, QIODevice::ReadOnly);
    return QVariant(ds);
  }

  template<>
  inline QString value<QString>(const mdbx::slice& v) {
    return QString::fromUtf8(value<QByteArray>(v));
  }

  template<>
  inline qint64 value<qint64>(const mdbx::slice& v) {
    return MdbxTxn::fromKey(v);
  }

  template<typename T>
  inline T value(const mdbx::slice& v) {
    return value<QVariant>(v).value<T>();
  }
}

template <typename T>
QList<T> MdbxTable::values(const QString& index) const
{
  QList<T> result;

  mdbx::map_handle idxMap(txn->open(schema, index));
  mdbx::cursor_managed idxCursor(txn->cursor(idxMap));
  mdbx::cursor::move_result idxResult = idxCursor.to_first(false);
  while (idxResult) {
    result << MdbxTableValues::value<T>(idxResult.key);
    idxResult = idxCursor.to_next_first_multi(false);
  }

  return result;
}

template <typename T>
QHash<QString, QList<T>> MdbxTable::mapValues(const QString& index) const
{
  QHash<QString, QList<T>> result;
  for (const QByteArray& ba : values<QByteArray>(index)) {
    int sep = ba.indexOf('\t');
    if (sep < 0) {
      continue;
    }
    QString key = QString::fromUtf8(ba.left(sep));
    mdbx::slice val(ba.constData() + sep + 1, ba.size() - sep - 1);
    result[key] << MdbxTableValues::value<T>(val);
  }
  return result;
}

template <typename T>
QList<T> MdbxTable::mapValues(const QString& index, const QString& searchKey) const
{
  QList<T> result;

  mdbx::map_handle idxMap(txn->open(schema, index));
  mdbx::cursor_managed idxCursor(txn->cursor(idxMap));
  mdbx::cursor::move_result idxResult = idxCursor.lower_bound(MdbxTxn::slice(searchKey.toUtf8(), true), false);
  do {
    QByteArray ba = QByteArray::fromRawData(idxResult.key.char_ptr(), idxResult.key.length());
    int sep = ba.indexOf('\t');
    if (sep >= 0) {
      QString key = QString::fromUtf8(ba.left(sep));
      mdbx::slice val(ba.constData() + sep + 1, ba.size() - sep - 1);
      if (key == searchKey) {
        result << MdbxTableValues::value<T>(val);
      } else if (key > searchKey) {
        break;
      }
    }
    idxResult = idxCursor.to_next_first_multi(false);
  } while (idxResult);

  return result;
}

#endif
#endif
