#ifndef MD_MDBXENV_H
#define MD_MDBXENV_H

#include <QString>
#include <QByteArray>
#include <QHash>
#include <QVariant>
#include "mdbx.h++"
#include "mdbxschema.h"

using buffer = mdbx::buffer<mdbx::legacy_allocator, mdbx::default_capacity_policy>;
static inline buffer key_from(qint64 v) { return buffer::key_from(int64_t(v)); }
static inline buffer key_from(quint64 v) { return buffer::key_from(uint64_t(v)); }

class MdbxTxn {
public:
  MdbxTxn() = default;
  ~MdbxTxn();

  MdbxTxn& operator=(MdbxTxn&& other) = default;

  inline bool isReadWrite() const { return txn.is_readwrite(); }
  qint64 nextSequence(const mdbx::map_handle& map);

  mdbx::map_handle open(const std::string& tableName);
  inline mdbx::map_handle open(const MdbxSchema* schema) { return open(schema->tableName); }
  inline mdbx::map_handle open(const char* tableName) { return open(std::string(tableName)); }
  inline mdbx::map_handle open(const QString& tableName) { return open(tableName.toStdString()); }

  mdbx::map_handle open(const MdbxSchema* schema, const std::string& key);
  inline mdbx::map_handle open(const MdbxSchema* schema, const char* key) { return open(schema, std::string(key)); }
  inline mdbx::map_handle open(const MdbxSchema* schema, const QString& key) { return open(schema, key.toStdString()); }
  mdbx::map_handle open(const std::string& tableName, mdbx::key_mode keyMode, mdbx::value_mode valueMode);

  mdbx::slice get(const mdbx::map_handle& map, const mdbx::slice& key);
  template <typename K> inline mdbx::slice get(const mdbx::map_handle& map, K key) { return get(map, mdbx::slice(slice(key, true))); }

  void erase(const mdbx::map_handle& map, const mdbx::slice& key);
  void erase(const mdbx::map_handle& map, const mdbx::slice& key, const mdbx::slice& value);
  template <typename K> inline void erase(const mdbx::map_handle& map, K key) { erase(map, slice(key, true)); }
  template <typename K, typename V> inline void erase(const mdbx::map_handle& map, K key, V value) { erase(map, mdbx::slice(slice(key, true)), mdbx::slice(slice(value))); }

  void upsert(const mdbx::map_handle& map, const mdbx::slice& key, const mdbx::slice& value);
  void upsert(const mdbx::map_handle& map, const buffer& key, const buffer& value);
  template <typename K, typename V> inline void upsert(const mdbx::map_handle& map, K key, V value) { upsert(map, slice(key, true), slice(value)); }

  inline mdbx::cursor_managed cursor(const mdbx::map_handle& map) { return txn.open_cursor(map); }

  void commit();

  inline mdbx::txn* getTxn() { return &txn; }

  static buffer slice(signed long long v, bool asKey = false);
  static inline buffer slice(unsigned long long v, bool asKey = false) { return slice((signed long long)v, asKey); }
  static inline buffer slice(signed long v, bool asKey = false) { return slice((signed long long)v, asKey); }
  static inline buffer slice(unsigned long v, bool asKey = false) { return slice((signed long long)v, asKey); }
  static inline buffer slice(signed int v, bool asKey = false) { return slice((signed long long)v, asKey); }
  static inline buffer slice(unsigned int v, bool asKey = false) { return slice((signed long long)v, asKey); }
  static buffer slice(const QVariant& v, bool asKey = false);
  static buffer slice(const QByteArray& v, bool asKey = false);
  static inline buffer slice(const buffer& v, bool = false) { return v; }
  static inline mdbx::slice slice(const mdbx::slice& v, bool = false) { return v; }
  static qint64 fromKey(const mdbx::slice& k);

private:
  friend class MdbxEnv;
  MdbxTxn(bool isReadWrite);

  void dispose(bool isWriter = false);

  QHash<QByteArray, mdbx::map_handle> handles;
  mdbx::txn_managed txn;
  qint64 txnId;
};

class MdbxEnv {
public:
  static MdbxEnv* instance();

  MdbxEnv(const QString& path);

  inline operator mdbx::env&() { return env; }
  inline mdbx::env* operator->() { return &env; }

  inline MdbxTxn startRead() { return MdbxTxn(false); }
  inline MdbxTxn startWrite() { return MdbxTxn(true); }

private:
  friend class MdbxTxn;

  mdbx::env_managed env;
  QList<mdbx::map_handle> initHandles;

  void ensureCreated(const MdbxSchema* schema, mdbx::txn* txn);
  void ensureCreated(const std::string& tableName, mdbx::txn* txn, bool multi, bool intKeys);
};

#define mEnv (MdbxEnv::instance())

#endif
