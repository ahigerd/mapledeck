#ifndef MD_MDBXTABLE_H
#define MD_MDBXTABLE_H

#include <QHash>
#include <QList>
#include <QSet>
#include <QString>
#include <QVariant>
#include <string>
#include <vector>
#include "mdbx.h++"
#include "mdbxenv.h"
class MdbxSchema;
class MdbxTable;

class MdbxRecord
{
public:
  MdbxRecord();
  MdbxRecord(const MdbxRecord& other) = default;
  MdbxRecord(MdbxRecord&& other) = default;

  MdbxRecord& operator=(const MdbxRecord& other) = default;
  MdbxRecord& operator=(MdbxRecord&& other) = default;
  bool operator==(const MdbxRecord& other) const;

  inline bool isValid() const { return !!schema; }
  inline qint64 id() const { return _id; }

  QList<qint64> links(const QString& tableName) const;
  void link(const QString& tableName, qint64 id);
  void unlink(const QString& tableName, qint64 id);

  QVariant value(const QString& key, const QString& subkey = QString()) const;
  template <typename T> inline T value(const QString& key, const QString& subkey = QString()) const { return value(key, subkey).value<T>(); }
  QVariantList values(const QString& key) const;
  template <typename T> inline QList<T> values(const QString& key) const {
    QList<T> result;
    for (const QVariant& v : values(key)) {
      result << v.value<T>();
    }
    return result;
  }
  void setValue(const QString& key, const QVariant& value);
  void setValue(const QString& key, const QString& subkey, const QVariant& value);
  inline void setValue(const QString& key, const QVariantList& value) { setValue(key, QVariant(value)); }
  template <typename T> inline void setValue(const QString& key, const QList<T>& values) {
    QVariantList vs;
    for (const auto& v : values) {
      vs << v;
    }
    setValue(key, QVariant(vs));
  }
  void merge(const QVariantHash& values);
  void remove(const QString& key, const QString& subkey = QString());

  void save();
  void erase();

  inline MdbxTxn* transaction() const { return txn; }

private:
  friend class MdbxTable;
  friend class std::vector<MdbxRecord>;
  MdbxRecord(const MdbxSchema* schema, qint64 id, const mdbx::map_handle& map, MdbxTxn* txn, bool isNew = false);
  MdbxRecord(const MdbxSchema* schema, MdbxTxn* txn, mdbx::cursor* cursor);
  void load(mdbx::cursor* cursor);
  QSet<QByteArray> buildIndex(const QString& key) const;
  void saveIndex(const QString& key);
  void wipeIndex(const std::string& key, const mdbx::slice& slice);

  struct Link {
    const MdbxSchema* schema;
    qint64 id;

    inline bool operator==(const Link& other) {
      return schema == other.schema && id == other.id;
    }
  };

  const MdbxSchema* schema;
  MdbxTxn* txn;
  mdbx::map_handle map;
  qint64 _id;
  bool dirty;
public:
  QVariantHash data;
private:
  // maps keys to original values
  QHash<QString, QSet<QByteArray>> dirtyIndexes;
  QList<Link> pendingLinks;
  QList<Link> pendingUnlinks;
};

class MdbxTable
{
public:
  class Iterator {
  public:
    Iterator(const Iterator& other);
    Iterator(Iterator&& other) = default;
    ~Iterator() = default;

    operator bool() const;
    bool operator==(const Iterator& other) const;

    Iterator& operator=(const Iterator& other);
    Iterator& operator=(Iterator&& other) = default;

    MdbxRecord operator*();

    Iterator& operator++();

  protected:
    friend class MdbxTable;
    Iterator() = default;
    Iterator(const MdbxSchema* schema, MdbxTxn* txn, mdbx::cursor_managed&& cursor);

    const MdbxSchema* schema;
    mdbx::cursor_managed cursor;
    MdbxTxn* txn;
  };

  static std::string indexName(const MdbxSchema* schema, const std::string& key, const std::string& prefix = "idx");

  MdbxTable(const QString& tableName);
  MdbxTable(const QString& tableName, MdbxTxn* txn);

  MdbxRecord get(const QString& pKey) const;
  MdbxRecord get(qint64 id) const;
  qint64 find(const QString& pKey) const;
  QList<qint64> search(const QString& column, const QString& value) const;
  QList<qint64> search(const QString& column, qint64 value) const;
  QList<qint64> search(const QString& column, const QString& key, const QVariant& value) const;
  QSet<qint64> searchRange(const QString& column, qint64 min, qint64 max) const;
  QSet<qint64> searchRange(const QString& column, const QString& key, qint64 min, qint64 max) const;
  QSet<qint64> searchRange(const QString& column, const QString& min, const QString& max) const;
  QSet<qint64> searchRange(const QString& column, const QString& key, const QString& min, const QString& max) const;
  QList<qint64> searchLinks(const QString& otherTable, qint64 id) const;
  bool exists(qint64 id) const;
  bool exists(const QString& pKey) const;
  int count(const QString& column, const QString& value) const;
  int count(const QString& column, qint64 value) const;
  template <typename T>
  QList<T> values(const QString& index) const;
  template <typename T>
  QHash<QString, QList<T>> mapValues(const QString& index) const;
  template <typename T>
  QList<T> mapValues(const QString& index, const QString& key) const;

  MdbxRecord create(const QVariantHash& data = QVariantHash());

  int rowCount() const;
  qint64 maxId() const;
  Iterator begin();
  Iterator end();

  const MdbxSchema* const schema;

private:
  friend class MdbxRecord;
  explicit MdbxTable(const MdbxRecord* rec);

  QList<qint64> search(const std::string& column, const mdbx::slice& value) const;
  QSet<qint64> searchRange(const std::string& column, const mdbx::slice& min, const mdbx::slice& max) const;
  int count(const std::string& column, const mdbx::slice& value) const;
  MdbxTxn mTxn;
  MdbxTxn* txn;
  mdbx::map_handle map;
};

#include "mdbxtable_values.cpp"

#endif
