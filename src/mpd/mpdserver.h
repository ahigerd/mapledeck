#ifndef MD_MPDSERVER_H
#define MD_MPDSERVER_H

#include <QxtRPCPeer>
#include <QPointer>
#include <QTime>
#include "mpdprotocol.h"
#include "mpdsession.h"

class MpdServer : public QxtRPCPeer
{
Q_OBJECT
public:
  static MpdServer* instance();
  static QPair<QVariantHash, QByteArray> binaryChunk(const QVariantHash& values, const QByteArray& chunk, int offset, int size);

  MpdServer(QObject* parent = nullptr);

  bool start();

  QHash<quint64, QPointer<MpdSession>> sessions;

protected slots:
  void ping(quint64 id);
  inline void plchanges(quint64 id) { playlistinfo(id); }
  inline void replay_gain_status(quint64 id) { ok(id, {{ "replay_gain_mode", "off" }}); }
  void password(quint64 id, const QString& password);
  void commands(quint64 id);
  void notcommands(quint64 id);
  void tagtypes(quint64 id);
  void outputs(quint64 id);
  void disableoutput(quint64 id, const QString& outputId);
  void enableoutput(quint64 id, const QString& outputId);
  void toggleoutput(quint64 id, const QString& outputId);
  void status(quint64 id);
  void stats(quint64 id);
  void urlhandlers(quint64 id);
  void binarylimit(quint64 id, const QString& limit);
  void idle(quint64 id, const QStringList& subsystems);
  void noidle(quint64 id);
  void currentsong(quint64 id);
  void listall(quint64 id, const QString& path);
  inline void listallinfo(quint64 id, const QString& path) { lsinfo(id, { path }); }
  void lsinfo(quint64 id, const QStringList& path);
  void list(quint64 id, const QString& type, const QStringList& filters);
  void count(quint64 id, const QString& type, const QStringList& filters);
  void find(quint64 id, const QString& type, const QStringList& filters);
  void add(quint64 id, const QString& path);
  void addid(quint64 id, const QString& path, const QString& pos = QString());
  void delete_(quint64 id, const QString& range);
  void deleteid(quint64 id, const QString& songId);
  void load(quint64 id, const QString& playlist, const QString& range = QString(), const QString& pos = QString());
  void clear(quint64 id);
  void albumart(quint64 id, const QString& uri, const QString& offset = QString());
  void readpicture(quint64 id, const QString& uri, const QString& offset = QString());
  void listplaylist(quint64 id, const QString& name);
  void listplaylistinfo(quint64 id, const QString& name);
  void listplaylists(quint64 id);
  void playlist(quint64 id);
  void playlistid(quint64 id, const QString& songId = QString());
  void playlistinfo(quint64 id, const QString& range = QString());
  void playlistfind(quint64 id, const QString& type, const QStringList& filters);
  void playid(quint64 id, const QString& trackId);
  void play(quint64 id);
  void pause(quint64 id, const QString& state = QString());
  void stop(quint64 id);
  void seek(quint64 id);
  void next(quint64 id);
  void previous(quint64 id);
  void getvol(quint64 id);
  void setvol(quint64 id, const QString& volume);
  void volume(quint64 id, const QString& delta);
  void single(quint64 id, const QString& state);
  void repeat(quint64 id, const QString& state);
  void command_list_begin(quint64 id);
  void command_list_ok_begin(quint64 id);
  void command_list_end(quint64 id);

private slots:
  void newClient(quint64 id);
  void destroyClient(quint64 id);
  void unimplemented(quint64 id, const QString& fn);

private:
  friend class MpdSession;
  QTime uptime;
  bool checkSession(quint64 id);
  void partial(quint64 id, const QVariantHash& values = QVariantHash(), const QByteArray& binary = QByteArray(), int offset = 0);
  void ok(quint64 id, const QVariantHash& values = QVariantHash(), const QByteArray& binary = QByteArray(), int offset = 0);
  void list_ok(quint64 id, const QVariantHash& values = QVariantHash(), const QByteArray& binary = QByteArray(), int offset = 0);
  void ack(quint64 id, MpdProtocol::Ack error, int cmdIndex, const QString& command, const QString& message);

  template <typename FP, typename... Params>
  inline void forward(quint64 id, const QString& name, FP func, Params... params) {
    QPointer<MpdSession> s = sessions.value(id);
    if (s.isNull()) {
      ack(id, MpdProtocol::PasswordError, 0, name, "invalid session");
    } else {
      (s->*func)(params...);
    }
  };
};

#endif
