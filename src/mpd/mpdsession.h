#ifndef MD_MPDSESSION_H
#define MD_MPDSESSION_H

#include <QObject>
#include <QVariant>
#include <QSet>
#include <QTimer>
#include <QExplicitlySharedDataPointer>
#include <QSharedData>
#include <QPointer>
#include "mpdprotocol.h"
#include "../mediaitem.h"
class MpdServer;
class MpdSession;
class MapleSession;

class MpdSessionData : public QObject, public QSharedData
{
friend class MpdSession;
Q_OBJECT
public:
  static QHash<MapleSession*, QExplicitlySharedDataPointer<MpdSessionData>> groupedSessions;

  MpdSessionData(MapleSession* session);
  ~MpdSessionData();

  void setQueuePos(int newPos);
  void broadcastEvent(const QString& event);

  MapleSession* baseSession;
  QList<QPointer<MpdSession>> sessions;
  QSet<MapleSession*> activeTargets;

signals:
  // TODO: move to MapleSession
  void playerPlay();
  void playerPause();
  void playerResume();
  void playerStop();
  void updateCurrentSong();
};

class MpdSession : public QObject
{
Q_OBJECT
friend class MpdServer;
friend class MpdSessionData;
public:
  MpdSession(quint64 id, int userId, const QString& name, MpdServer* parent);

  int binaryLimit;

  QSet<QString> idling, events;
  int queueAt(int pos) const;
  QString sessionName() const;

  void fillStatus(QVariantHash& status);

  void outputs();
  void disableoutput(const QString& outputId);
  void enableoutput(const QString& outputId);
  void toggleoutput(const QString& outputId);
  void binarylimit(const QString& limit);
  void idle(const QStringList& subsystems);
  void noidle();
  void currentsong();
  void playlist();
  void playlistid(const QString& songId);
  void playlistinfo(const QString& range = QString());
  void playlistfind(const QString& type, const QStringList& filters);
  void listplaylists();
  void listplaylist(const QString& name);
  void listplaylistinfo(const QString& name);
  void listall(const QString& path);
  void lsinfo(const QStringList& path);
  void list(const QString& type, const QStringList& filters);
  void find(const QString& type, const QStringList& filters);
  void count(const QString& type, const QStringList& filters);
  void add(const QString& path);
  void addid(const QString& path, const QString& pos);
  void delete_(const QString& range);
  void deleteid(const QString& songId);
  void load(const QString& playlist, const QString& range = QString(), const QString& pos = QString());
  void clear();
  void playid(const QString& trackId);
  void play();
  void pause(const QString& newState);
  void stop();
  void seek() { ok(); }
  void next();
  void previous();
  void getvol();
  void setvol(const QString& volume);
  void volume(const QString& delta);
  void single(const QString& state);
  void repeat(const QString& state);
  void command_list_begin();
  void command_list_ok_begin();
  void command_list_end();

private slots:
  void scanStarted();
  void scanFinished(bool updated);
  void idleTimeout();
  void updateStatus();

private:
  QList<MediaItem> filter(const QString& type, const QString& filter, bool getMeta) const;
  void outputTrackInfo(int songId, int pos);
  void broadcastEvent(const QString& event);
  bool setOutputEnabled(qint64 outputId, bool enable);
  bool isOutputEnabled(qint64 outputId);

  void partial(const QVariantHash& values = QVariantHash(), const QByteArray& binary = QByteArray(), int offset = 0);
  void ok(const QVariantHash& values = QVariantHash(), const QByteArray& binary = QByteArray(), int offset = 0);
  void ack(MpdProtocol::Ack error, const QString& command, const QString& message);
  bool checkIdle();

  QTimer timeout;

  MpdServer* server;
  quint64 id;
  struct CommandResponse {
    MpdProtocol::Ack error;
    bool partial;
    QVariantHash values;
    QByteArray binary;
  };
  bool useList, useListOK, listHalt;
  QList<CommandResponse> cmdList;

  QExplicitlySharedDataPointer<MpdSessionData> d;
};

#endif
