#include "mpdprotocol.h"
#include <QtDebug>

static const QHash<QString, int> varargsMin = {
  { "idle", 0 },
  { "list", 1 },
  { "find", 1 },
  { "count", 1 },
  { "playlistfind", 1 },
  { "prio", 1 },
  { "prioid", 1 },
  { "tagtypes", 1 },
  { "lsinfo", 0 },
};

MpdProtocol::MpdProtocol()
{
  // initializers only
}

QByteArray MpdProtocol::serialize(const QString& fn, const QVariant& p1, const QVariant& p2, const QVariant& p3, const QVariant& p4,
  const QVariant&, const QVariant&, const QVariant&, const QVariant&) const
{
  if (fn == "ACK") {
    QByteArray msg = QStringLiteral("ACK [%1@%2] {%3} %4\n").arg(p1.toInt()).arg(p2.toInt()).arg(p3.toString()).arg(p4.toString()).toUtf8();
    if (!qEnvironmentVariableIsEmpty("MAPLEDECK_VERBOSE")) {
      qDebug() << "[OUT]" << msg;
    }
    return msg;
  }
  QByteArray msg;
  if (p1.canConvert<QVariantHash>()) {
    const QVariantHash h = p1.value<QVariantHash>();
    for (const QString& key : h.keys()) {
      QVariant value = h[key];
      if (value.canConvert<QStringList>()) {
        for (const QString& v : value.value<QStringList>()) {
          msg += QStringLiteral("%1: %2\n").arg(key).arg(v).toUtf8();
        }
      } else {
        msg += QStringLiteral("%1: %2\n").arg(key).arg(value.toString()).toUtf8();
      }
    }
  }
  if (p2.canConvert<QByteArray>()) {
    const QByteArray payload = p2.value<QByteArray>();
    if (!payload.isEmpty()) {
      msg += QStringLiteral("binary: %1\n").arg(payload.size()).toUtf8() + payload + "\n";
    }
  }
  if (!fn.isEmpty()) {
    msg += fn.toUtf8() + "\n";
  }
  if (!qEnvironmentVariableIsEmpty("MAPLEDECK_VERBOSE")) {
    if (!p2.value<QByteArray>().isEmpty()) {
      qDebug() << "[OUT] (binary)" << p1.value<QVariantHash>();
    } else {
      qDebug() << "[OUT]" << msg;
    }
  }
  return msg;
}

MpdProtocol::DeserializedData MpdProtocol::deserialize(QByteArray& data)
{
  int nl = data.indexOf('\n');
  QString cmd = QString::fromUtf8(data.left(nl + 1));
  data.remove(0, nl + 1);
  if (cmd.trimmed().isEmpty()) {
    return NoOp();
  }
  DeserializedData msg;
  int minVA = -1;
  int len = cmd.size();
  bool inQuote = false;
  bool escape = false;
  bool needsCommand = true;
  QString token;
  QStringList varargs;
  for (int i = 0; i < len; i++) {
    QChar ch = cmd[i];
    if (escape) {
      token += ch;
      escape = false;
    } else if (inQuote) {
      if (ch == '\\') {
        escape = true;
      } else if (ch == '"') {
        inQuote = false;
      } else {
        token += ch;
      }
    } else if (ch.isSpace()) {
      if (needsCommand) {
        msg.first = token;
        minVA = varargsMin.value(token, -1);
        needsCommand = false;
      } else if (minVA >= 0 && msg.second.length() >= minVA) {
        varargs << token;
      } else {
        msg.second << token;
      }
      token.clear();
    } else if (ch == '"') {
      inQuote = true;
    } else {
      token += ch;
    }
  }
  if (!token.isEmpty()) {
    if (needsCommand) {
      msg.first = token;
    } else if (minVA >= 0 && msg.second.length() >= minVA) {
      varargs << token;
    } else {
      msg.second << token;
    }
  }
  if (minVA >= 0) {
    while (msg.second.length() < minVA) {
      msg.second << QString();
    }
    msg.second << varargs;
  }
  if (!qEnvironmentVariableIsEmpty("MAPLEDECK_VERBOSE")) {
    if (msg.first == "password") {
      qDebug() << "[IN] QPair(\"password\", (\"redacted\"))";
    } else {
      qDebug() << "[IN]" << msg;
    }
  }
  return msg;
}

bool MpdProtocol::canDeserialize(const QByteArray& buffer) const
{
  return buffer.contains('\n');
}
