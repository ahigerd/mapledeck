#include "mpdserver.h"
#include "mpdprotocol.h"
#include "../mapleapp.h"
#include "../mapledb.h"
#include "../metadata/metadataprovider.h"
#include "../database/mdbxtable.h"
#include <QMimeDatabase>
#include <QMetaObject>
#include <QMetaMethod>
#include <QSettings>

#define LITERAL_COMMA ,
#define C_THIRD(A, B, C, ...) C
#define COMMA_FN(A, ...) ,
#define COMMA_IF_EMPTY(N, ...) COMMA_FN N ()
#define COMMA_EXPAND(...) C_THIRD(__VA_ARGS__ A, , LITERAL_COMMA)
#define COMMA(...) COMMA_EXPAND(COMMA_IF_EMPTY(__VA_ARGS__))

#define FORWARD(func, args, ...) void MpdServer::func args { forward(id, QStringLiteral(#func), &MpdSession::func COMMA(__VA_ARGS__) __VA_ARGS__); }

static MpdServer* MpdServer_instance = nullptr;

MpdServer* MpdServer::instance()
{
  return MpdServer_instance;
}

QPair<QVariantHash, QByteArray> MpdServer::binaryChunk(const QVariantHash& values, const QByteArray& chunk, int offset, int size)
{
  QVariantHash modified = values;
  if (!chunk.isEmpty() && !modified.contains("size")) {
    modified["size"] = chunk.size();
  }
  if (offset == 0 && (size < 0 || size > chunk.size())) {
    return { modified, chunk };
  } else {
    return { modified, chunk.mid(offset, size) };
  }
}

MpdServer::MpdServer(QObject* parent)
: QxtRPCPeer(parent)
{
  MpdServer_instance = this;

  QObject::connect(this, SIGNAL(clientConnected(quint64)), this, SLOT(newClient(quint64)));
  QObject::connect(this, SIGNAL(clientDisconnected(quint64)), this, SLOT(destroyClient(quint64)));
  setFallbackSlot(this, SLOT(unimplemented(quint64,QString)));

  int ct = staticMetaObject.methodCount();
  QByteArray lastMethod;
  for (int i = staticMetaObject.methodOffset(); i < ct; i++) {
    QMetaMethod method = staticMetaObject.method(i);
    if (method.access() == QMetaMethod::Protected) {
      if (lastMethod == method.name()) {
        // Only keep one overload. Because the list is being iterated in reverse
        // order, this will be the one with all optional parameters included.
        continue;
      }
      QByteArray name = method.name();
      lastMethod = name;
      if (name.endsWith("_")) {
        name.chop(1);
      }
      attachSlot(name.constData(), this, method.methodSignature().constData());
    }
  }

  setSerializer(new MpdProtocol);
}

bool MpdServer::start()
{
  QSettings settings;
  if (!listen(QHostAddress::Any, settings.value("mpd/port", 6600).toInt())) {
    return false;
  }
  uptime.start();
  return true;
}

void MpdServer::partial(quint64 id, const QVariantHash& values, const QByteArray& binary, int offset)
{
  QPointer<MpdSession> s = sessions.value(id);
  int binaryLimit = -1;
  if (!s.isNull()) {
    s->timeout.start();
    if (s->useList) {
      s->partial(values, binary);
      return;
    }
    binaryLimit = s->binaryLimit;
  }
  if (!binary.isEmpty()) {
    auto chunk = binaryChunk(values, binary, offset, binaryLimit);
    call(id, QString(), chunk.first, chunk.second);
  } else {
    call(id, QString(), values, binary);
  }
}

void MpdServer::ok(quint64 id, const QVariantHash& values, const QByteArray& binary, int offset)
{
  QPointer<MpdSession> s = sessions.value(id);
  int binaryLimit = -1;
  if (!s.isNull()) {
    s->timeout.start();
    if (s->useList) {
      s->ok(values, binary);
      return;
    }
    binaryLimit = s->binaryLimit;
  }
  if (!binary.isEmpty()) {
    auto chunk = binaryChunk(values, binary, offset, binaryLimit);
    call(id, "OK", chunk.first, chunk.second);
  } else {
    call(id, "OK", values, binary);
  }
}

void MpdServer::list_ok(quint64 id, const QVariantHash& values, const QByteArray& binary, int offset)
{
  if (!binary.isEmpty()) {
    QPointer<MpdSession> s = sessions.value(id);
    int binaryLimit = s.isNull() ? -1 : s->binaryLimit;
    auto chunk = binaryChunk(values, binary, offset, binaryLimit);
    call(id, "list_OK", chunk.first, chunk.second);
  } else {
    call(id, "list_OK", values, binary);
  }
}

void MpdServer::ack(quint64 id, MpdProtocol::Ack error, int cmdIndex, const QString& command, const QString& message)
{
  QPointer<MpdSession> s = sessions.value(id);
  if (!s.isNull()) {
    s->timeout.start();
    if (s->useList) {
      s->ack(error, command, message);
      return;
    }
  }
  call(id, "ACK", error, cmdIndex, command, message);
}

void MpdServer::newClient(quint64 id)
{
  qDebug() << id << "newClient";
  call(id, "OK MPD 0.23.0");
}

void MpdServer::destroyClient(quint64 id)
{
  MpdSession* s = sessions.take(id);
  if (s) {
    s->deleteLater();
  }
  if (clients().contains(id)) {
    disconnectClient(id);
  }
}

void MpdServer::ping(quint64 id)
{
  ok(id);
}

void MpdServer::commands(quint64 id)
{
  static QStringList allCommands;
  if (allCommands.isEmpty()) {
    int ct = staticMetaObject.methodCount();
    for (int i = staticMetaObject.methodOffset(); i < ct; i++) {
      QMetaMethod method = staticMetaObject.method(i);
      if (method.access() == QMetaMethod::Protected) {
        allCommands << method.name();
      }
    }
  }
  ok(id, {{ "commands", allCommands }});
}

void MpdServer::notcommands(quint64 id)
{
  ok(id);
}

void MpdServer::tagtypes(quint64 id)
{
  QStringList types;
  MdbxTable tblTagTypes("tagtypes");
  for (const MdbxRecord& rec : tblTagTypes) {
    types << rec.value<QString>("tag");
  }
  ok(id, {{ "tagtype", types }});
}

void MpdServer::status(quint64 id)
{
  QVariantHash status{{ "single", "0" }, { "consume", "0" }, { "random", "0" }, { "repeat", "0" }, { "volume", "100" }};
  QPointer<MpdSession> s = sessions.value(id);
  if (s.isNull()) {
    ack(id, MpdProtocol::PasswordError, 0, "status", "invalid session");
    return;
  } else {
    s->fillStatus(status);
  }
  if (mDB->currentScanProgress() >= 0) {
    status["updating_db"] = 1;
  }
  ok(id, status);
}

void MpdServer::stats(quint64 id)
{
  MdbxTable tblCollections("collections");
  MdbxTable tblTracks("tracks");
  ok(id, {
    { "uptime", uptime.elapsed() / 1000 },
    { "artists", tblCollections.count("type", int(MediaItem::Artist)) },
    { "albums", tblCollections.count("type", int(MediaItem::Album)) },
    { "songs", tblTracks.rowCount() },
    // { "db_playtime", 0 },
  });
}

void MpdServer::urlhandlers(quint64 id)
{
  ok(id);
}

void MpdServer::albumart(quint64 id, const QString& uri, const QString& offset)
{
  Q_UNUSED(uri);
  Q_UNUSED(offset);
  int trackId = uri.section('/', -2, -2).toInt();
  MediaItem item(MediaItem::Track, trackId);
  QByteArray image = item.coverArt();
  if (!image.isEmpty()) {
    QString mimeType = QMimeDatabase().mimeTypeForData(image).name().toUtf8();
    ok(id, {{ "size", image.size() }, { "type", mimeType }}, image.mid(offset.toInt(), 8192));
  } else {
    ack(id, MpdProtocol::DoesNotExistError, 0, "albumart", "not found");
  }
}

void MpdServer::readpicture(quint64 id, const QString& uri, const QString& offset)
{
  albumart(id, uri, offset);
}

void MpdServer::unimplemented(quint64 id, const QString& fn)
{
  qDebug() << "***UNIMPLEMENTED***" << id << fn;
  ack(id, MpdProtocol::UnknownError, 0, fn, "unimplemented");
}

void MpdServer::password(quint64 id, const QString& pw)
{
  QString username = pw.section(':', 0, 0);
  QString password = pw.section(':', 1);
  QString session = username;
  if (username.contains('@')) {
    username = username.section('@', 0, 0);
  } else if (username.contains('+')) {
    username = username.section('+', 0, 0);
  }
  int userId = MapleApp::authenticateUser(username, password);
  if (userId < 0) {
    ack(id, MpdProtocol::PasswordError, 0, "password", "Bad username or password");
    return;
  }
  sessions[id] = new MpdSession(id, userId, session, this);
  ok(id);
}

FORWARD(outputs, (quint64 id))
FORWARD(disableoutput, (quint64 id, const QString& outputId), outputId)
FORWARD(enableoutput, (quint64 id, const QString& outputId), outputId)
FORWARD(toggleoutput, (quint64 id, const QString& outputId), outputId)
FORWARD(binarylimit, (quint64 id, const QString& limit), limit)
FORWARD(idle, (quint64 id, const QStringList& v), v)
FORWARD(noidle, (quint64 id))
FORWARD(currentsong, (quint64 id))
FORWARD(listplaylists, (quint64 id))
FORWARD(listplaylist, (quint64 id, const QString& name), name)
FORWARD(listplaylistinfo, (quint64 id, const QString& name), name)
FORWARD(playlist, (quint64 id))
FORWARD(playlistid, (quint64 id, const QString& songId), songId)
FORWARD(playlistinfo, (quint64 id, const QString& range), range)
FORWARD(playlistfind, (quint64 id, const QString& type, const QStringList& filters), type, filters)
FORWARD(listall, (quint64 id, const QString& path), path)
FORWARD(lsinfo, (quint64 id, const QStringList& path), path)
FORWARD(list, (quint64 id, const QString& type, const QStringList& filters), type, filters)
FORWARD(count, (quint64 id, const QString& type, const QStringList& filters), type, filters)
FORWARD(find, (quint64 id, const QString& type, const QStringList& filters), type, filters)
FORWARD(add, (quint64 id, const QString& path), path)
FORWARD(addid, (quint64 id, const QString& path, const QString& pos), path, pos)
FORWARD(delete_, (quint64 id, const QString& range), range)
FORWARD(deleteid, (quint64 id, const QString& songId), songId)
FORWARD(load, (quint64 id, const QString& playlist, const QString& range, const QString& pos), playlist, range, pos)
FORWARD(clear, (quint64 id))
FORWARD(command_list_begin, (quint64 id))
FORWARD(command_list_ok_begin, (quint64 id))
FORWARD(command_list_end, (quint64 id))
FORWARD(playid, (quint64 id, const QString& trackId), trackId)
FORWARD(play, (quint64 id))
FORWARD(pause, (quint64 id, const QString& state), state)
FORWARD(stop, (quint64 id))
FORWARD(seek, (quint64 id))
FORWARD(next, (quint64 id))
FORWARD(previous, (quint64 id))
FORWARD(getvol, (quint64 id))
FORWARD(setvol, (quint64 id, const QString& volume), volume)
FORWARD(volume, (quint64 id, const QString& delta), delta)
FORWARD(single, (quint64 id, const QString& state), state)
FORWARD(repeat, (quint64 id, const QString& state), state)
