#ifndef MD_MPDPROTOCOL_H
#define MD_MPDPROTOCOL_H

#include <QxtAbstractSignalSerializer>

class MpdProtocol : public QxtAbstractSignalSerializer
{
public:
  MpdProtocol();

  enum Ack {
    NoError = 0,
    NotListError = 1,
    ArgumentError = 2,
    PasswordError = 3,
    PermissionError = 4,
    UnknownError = 5,
    DoesNotExistError = 50,
    PlaylistMaxError = 51,
    SystemError = 52,
    PlaylistLoadError = 53,
    UpdateAlreadyError = 54,
    PlayerSyncError = 55,
    ExistsError = 56,
  };

  virtual QByteArray serialize(const QString& fn, const QVariant& p1 = QVariant(), const QVariant& p2 = QVariant(), const QVariant& p3 = QVariant(),
                               const QVariant& p4 = QVariant(), const QVariant& p5 = QVariant(), const QVariant& p6 = QVariant(),
                               const QVariant& p7 = QVariant(), const QVariant& p8 = QVariant()) const;
  virtual DeserializedData deserialize(QByteArray& data);
  virtual bool canDeserialize(const QByteArray& buffer) const;
};

#endif
