#include "mpdsession.h"
#include "mpdserver.h"
#include "../maplesession.h"
#include "../mapledb.h"
#ifdef MD_ENABLE_JUKEBOX
#include "../jukebox/jukeboxplayer.h"
#endif

static const QHash<QString, MediaItem::ItemType> tagsToType{
  { "album", MediaItem::Album },
  { "albumsort", MediaItem::Album },
  { "artist", MediaItem::Artist },
  { "artistsort", MediaItem::Artist },
  { "folder", MediaItem::Folder },
  { "playlist", MediaItem::Playlist },
};

QHash<MapleSession*, QExplicitlySharedDataPointer<MpdSessionData>> MpdSessionData::groupedSessions;

static QPair<int, int> parseRange(const QString& range, int len)
{
  int start = 0, end = -1;
  if (range.contains(':')) {
    QStringList parts = range.split(':');
    start = parts[0].toInt();
    end = parts[1].isEmpty() ? -1 : parts[1].toInt();
  } else if (!range.isEmpty()) {
    start = range.toInt();
    end = start;
  }
  if (start < 0) {
    start = 0;
  }
  if (end < 0) {
    end = len;
  } else if (end < start) {
    end = start;
  }
  if (end >= len) {
    end = len;
  }
  return QPair<int, int>(start, end);
}

static int getIdFromName(const QString& name)
{
  return name.section('[', -1).section(']', 0, 0).toInt();
}

MpdSessionData::MpdSessionData(MapleSession* session)
: QObject(nullptr), baseSession(session)
{
  groupedSessions[session] = this;
}

MpdSessionData::~MpdSessionData()
{
  baseSession->deleteLater();
}

void MpdSessionData::setQueuePos(int newPos)
{
  baseSession->seekQueue(newPos);
  emit updateCurrentSong();
  broadcastEvent("player");
}

MpdSession::MpdSession(quint64 id, int userId, const QString& sessionName, MpdServer* parent)
: QObject(parent), binaryLimit(-1), server(parent), id(id), useList(false), listHalt(false)
{
  MapleSession* baseSession = MapleSession::findSession(MapleSession::MPD, userId, sessionName);
  if (!baseSession) {
    qFatal("Impossible session in MpdSession");
  }
  if (!baseSession->dbSessionId) {
    baseSession->setCookie(sessionName);
  }
  d = MpdSessionData::groupedSessions.value(baseSession);
  if (!d) {
    MpdSessionData::groupedSessions[baseSession] = d = QExplicitlySharedDataPointer<MpdSessionData>(new MpdSessionData(baseSession));
  }
  d->sessions << this;

  QObject::connect(mDB, SIGNAL(scanStarted()), this, SLOT(scanStarted()));
  QObject::connect(mDB, SIGNAL(scanFinished(bool)), this, SLOT(scanFinished(bool)));
  QObject::connect(baseSession, SIGNAL(started()), this, SLOT(updateStatus()));
  QObject::connect(baseSession, SIGNAL(paused()), this, SLOT(updateStatus()));
  QObject::connect(baseSession, SIGNAL(resumed()), this, SLOT(updateStatus()));
  QObject::connect(baseSession, SIGNAL(stopped()), this, SLOT(updateStatus()));
  QObject::connect(&timeout, SIGNAL(timeout()), this, SLOT(idleTimeout()));
  timeout.setSingleShot(true);
  timeout.setInterval(60000);
  timeout.start();
}

void MpdSession::outputs()
{
  for (MapleSession* target : MapleSession::targetSessions(d->baseSession->userId)) {
    if (target->isJukeboxOnly) {
      continue;
    }
    partial({{ "outputid", QString::number(target->dbSessionId) }});
    partial({
      { "outputname", target->sessionName },
      { "plugin", target->type == MapleSession::MPD ? "httpd" : "web" },
      { "outputenabled", d->activeTargets.contains(target) ? "1" : "0" },
    });
  }
#ifdef MD_ENABLE_JUKEBOX
  partial({{ "outputid", "0" }});
  partial({
    { "outputname", "jukebox" },
    { "plugin", "jukebox" },
    { "outputenabled", isOutputEnabled(0) ? "1" : "0" },
  });
#endif
  ok();
}

void MpdSession::disableoutput(const QString& outputId)
{
  qint64 output = outputId.toLong();
  bool success = setOutputEnabled(output, false);
  if (!success) {
    ack(MpdProtocol::PermissionError, "disableoutput", "Cannot disable output");
  } else {
    ok();
  }
}

void MpdSession::enableoutput(const QString& outputId)
{
  qint64 output = outputId.toLong();
  bool success = setOutputEnabled(output, true);
  if (!success) {
    ack(MpdProtocol::PermissionError, "enableoutput", "Cannot enable output");
  } else {
    ok();
  }
}

void MpdSession::toggleoutput(const QString& outputId)
{
  qint64 output = outputId.toLong();
  bool success = setOutputEnabled(output, !isOutputEnabled(output));
  if (!success) {
    ack(MpdProtocol::PermissionError, "toggleoutput", "Cannot toggle output");
  } else {
    ok();
  }
}

bool MpdSession::isOutputEnabled(qint64 outputId)
{
#ifdef MD_ENABLE_JUKEBOX
  if (outputId == 0) {
    JukeboxPlayer* jukebox = JukeboxPlayer::instance();
    return (jukebox && jukebox->isClaimed(d->baseSession));
  }
#endif
  for (MapleSession* target : d->activeTargets) {
    if (target->dbSessionId == outputId) {
      return true;
    }
  }
  return false;
}

bool MpdSession::setOutputEnabled(qint64 outputId, bool enable)
{
  if (isOutputEnabled(outputId) == enable) {
    return true;
  }
#ifdef MD_ENABLE_JUKEBOX
  if (outputId == 0) {
    JukeboxPlayer* jukebox = JukeboxPlayer::instance();
    if (enable) {
      if (!jukebox) {
        jukebox = new JukeboxPlayer(d->baseSession);
      }
      return jukebox->claim(d->baseSession);
    } else if (jukebox) {
      return jukebox->claim(nullptr);
    }
  }
#endif
  // TODO
  return false;
}

void MpdSession::binarylimit(const QString& limit)
{
  int limitValue = limit.toInt();
  if (limitValue > 0) {
    binaryLimit = limitValue;
  } else {
    binaryLimit = -1;
  }
  ok();
}

void MpdSession::idleTimeout()
{
  if (idling.isEmpty()) {
    qDebug() << id << "idle timeout";
    server->destroyClient(id);
  } else {
    timeout.start();
  }
}

void MpdSession::updateStatus()
{
  d->broadcastEvent("player");
}

void MpdSession::partial(const QVariantHash& values, const QByteArray& binary, int offset)
{
  timeout.start();
  if (useList) {
    if (!listHalt) {
      auto chunk = MpdServer::binaryChunk(values, binary, offset, binaryLimit);
      CommandResponse cr;
      cr.partial = true;
      cr.error = MpdProtocol::NoError;
      cr.values = chunk.first;
      cr.binary = chunk.second;
      cmdList << cr;
    }
    return;
  }
  server->partial(id, values, binary, offset);
}

void MpdSession::ok(const QVariantHash& values, const QByteArray& binary, int offset)
{
  timeout.start();
  if (useList) {
    if (!listHalt) {
      auto chunk = MpdServer::binaryChunk(values, binary, offset, binaryLimit);
      CommandResponse cr;
      cr.partial = false;
      cr.error = MpdProtocol::NoError;
      cr.values = chunk.first;
      cr.binary = chunk.second;
      cmdList << cr;
    }
    return;
  }
  server->ok(id, values, binary);
}

void MpdSession::ack(MpdProtocol::Ack error, const QString& command, const QString& message)
{
  timeout.start();
  if (useList) {
    if (!listHalt) {
      CommandResponse cr;
      cr.partial = false;
      cr.error = error;
      cr.values = {{ "command", command }, { "message", message }};
      cmdList << cr;
      listHalt = true;
    }
    return;
  }
  server->ack(id, error, 0, command, message);
}

void MpdSession::fillStatus(QVariantHash& status)
{
  const MapleSession* s = d->baseSession;
  const auto& queue = s->queue;
  int len = queue.size();
  int pos = s->queuePos;
  bool single = d->baseSession->singleMode;
  bool repeat = d->baseSession->repeatMode;
  if (single) {
    status["single"] = "1";
  }
  if (repeat) {
    status["repeat"] = "1";
  }
  status["playlistlength"] = len;
  if (len < 1 || pos >= len || s->state == MapleSession::Idle) {
    status["state"] = "stop";
  } else {
    status["state"] = s->state == MapleSession::Paused ? "pause" : "play";
    status["songid"] = s->nowPlaying.id;
    if (pos >= 0) {
      status["song"] = pos;
    }
    status["duration"] = s->duration;
    status["elapsed"] = s->elapsed;
    status["time"] = QStringLiteral("%1:%2").arg(int(s->elapsed)).arg(int(s->duration));
    if (repeat && single) {
      status["nextsongid"] = status["songid"];
      status["nextsong"] = pos;
    } else if (!single && queue.size() > pos + 1) {
      status["nextsongid"] = queue[pos + 1];
      status["nextsong"] = pos + 1;
    } else if (repeat) {
      status["nextsongid"] = queue[0];
      status["nextsong"] = 0;
    }
  }
}

void MpdSession::idle(const QStringList& subsystems)
{
  idling.clear();
  if (subsystems.isEmpty()) {
    idling << "all";
  } else {
    for (const QString& s : subsystems) {
      idling << s;
    }
  }
  checkIdle();
}

void MpdSession::noidle()
{
  if (!checkIdle()) {
    ok();
  }
  idling.clear();
}

void MpdSession::currentsong()
{
  const MapleSession* s = d->baseSession;
  if (s->nowPlaying.id) {
    playlistid(QString::number(s->nowPlaying.id));
    return;
  }
  ok();
}

void MpdSession::playlist()
{
  int pos = 0;
  for (int id : d->baseSession->queue) {
    partial({{ QStringLiteral("%1:file").arg(pos), QStringLiteral("/stream/%1/").arg(id) }});
    ++pos;
  }
  ok();
}

void MpdSession::playlistinfo(const QString& range)
{
  const MapleSession* s = d->baseSession;
  QPair<int, int> span = parseRange(range, s->queue.size());
  for (int i = span.first; i < span.second; i++) {
    outputTrackInfo(s->queue[i], i);
  }
  ok();
}

void MpdSession::playlistfind(const QString& type, const QStringList& filters)
{
  // XXX
  if (type != "filename" || filters.isEmpty()) {
    ok();
    return;
  }
  int trackId = filters.first().section('/', -1).toInt();
  int pos = d->baseSession->queue.indexOf(trackId);
  if (pos >= 0) {
    outputTrackInfo(trackId, pos);
  }
  ok();
}

void MpdSession::playlistid(const QString& songId)
{
  const MapleSession* s = d->baseSession;
  if (!songId.isEmpty()) {
    int songIdNum = songId.toInt();
    int pos = s->queue.indexOf(songIdNum);
    if (pos >= 0) {
      outputTrackInfo(songIdNum, pos);
    }
    ok();
    return;
  }
  int i = 0;
  for (int id : s->queue) {
    outputTrackInfo(id, i);
    i++;
  }
  ok();
}

void MpdSession::outputTrackInfo(int id, int pos)
{
  MediaItem item(MediaItem::Track, id);
  item.loadMetadata();
  partial({{ "file", item.fullPath }});
  QVariantHash info{{ "Pos", pos }, { "Id", id }, { "Title", item.metadata.value("TITLE", item.name) }};
  if (item.metadata.contains("ARTIST")) {
    info["Artist"] = item.metadata["ARTIST"];
  }
  if (item.metadata.contains("ALBUM")) {
    info["Album"] = item.metadata["ALBUM"];
  }
  if (item.metadata.contains("$duration")) {
    info["duration"] = item.metadata["$duration"];
  }
  const MapleSession* s = d->baseSession;
  if (pos == s->queuePos) {
    info["elapsed"] = s->elapsed;
    info["time"] = int(s->elapsed);
  }
  partial(info);
}

QList<MediaItem> MpdSession::filter(const QString& type, const QString& filter, bool getMeta) const
{
  if (type.startsWith("(")) {
    // TODO: filter expression
    return QList<MediaItem>();
  }
  // TODO: search
  auto itemType = tagsToType.value(type, MediaItem::Invalid);
  if (itemType == MediaItem::Folder && (filter.isEmpty() || filter == "/")) {
    return mDB->rootFolders();
  }
  int id = getIdFromName(filter);
  if (!id || itemType == MediaItem::Invalid) {
    return QList<MediaItem>();
  }
  MediaItem container(itemType, id);
  return container.children(nullptr, getMeta);
}

void MpdSession::listplaylist(const QString& name)
{
  find("playlist", { name });
}

void MpdSession::listplaylistinfo(const QString& name)
{
  find("playlist", { name });
}

void MpdSession::listplaylists()
{
  QList<MediaItem> items = MediaItem::getAll(MediaItem::Playlist, false);
  MediaItem::sort(items, QString());
  for (const MediaItem& item : items) {
    partial({{ "playlist", QStringLiteral("%1 [%2]").arg(item.name).arg(item.id) }});
    partial({{
      "Last-Modified",
      (item.metadata.contains("$lastUpdated") ? QDateTime::fromSecsSinceEpoch(item.metadata["$lastUpdated"].toLongLong()) : item.created).toString(Qt::ISODate)
    }});
  }
  ok();
}

void MpdSession::lsinfo(const QStringList& path)
{
  if (path.isEmpty()) {
    find("folder", { "/" });
  } else if (path[0].contains("://")) {
    ack(MpdProtocol::UnknownError, "lsinfo", "Unsupported URI scheme");
  } else {
    find("folder", { path.first() });
  }
}

void MpdSession::listall(const QString& path)
{
  QList<MediaItem> items = filter("playlist", path, true);
  for (const MediaItem& item : items) {
    for (const MediaItem& child : item.children(nullptr, true)) {
      if (child.type == MediaItem::Track) {
        partial({{ "file", QStringLiteral("/stream/%1/").arg(child.id) }});
      } else if (child.type == MediaItem::Folder) {
        partial({{ "directory", QStringLiteral("%1 [%2]").arg(child.name).arg(child.id) }});
      } else if (child.type == MediaItem::Playlist) {
        partial({{ "playlist", QStringLiteral("%1 [%2]").arg(child.name).arg(child.id) }});
      }
    }
  }
  ok();
}

void MpdSession::list(const QString& type, const QStringList& filters)
{
  Q_UNUSED(filters);
  QString key;
  MediaItem::ItemType itemType;
  QStringList result;
  if (type == "album" || type == "albumsort") {
    key = "Album";
    itemType = MediaItem::Album;
  } else if (type == "artist" || type == "artistsort") {
    key = "Artist";
    itemType = MediaItem::Artist;
  } else if (type == "playlist") {
    key = "playlist";
    itemType = MediaItem::Playlist;
  } else {
    // TODO: search
    ok();
    return;
  }
  QList<MediaItem> items = MediaItem::getAll(itemType, false);
  MediaItem::sort(items, QString());
  for (const MediaItem& item : items) {
    result << QStringLiteral("%1 [%2]").arg(item.name).arg(item.id);
  }
  if (result.isEmpty()) {
    ok();
  } else {
    ok({{ key, result }});
  }
}

void MpdSession::find(const QString& type, const QStringList& filters)
{
  QList<MediaItem> items = filter(type, filters.isEmpty() ? QString() : filters.first(), true);
  qDebug() << "find()" << type << filters << items.size();
  for (const MediaItem& child : items) {
    QVariantHash h{
      { "Last-Modified", (child.metadata.contains("$lastUpdated") ? QDateTime::fromSecsSinceEpoch(child.metadata["$lastUpdated"].toLongLong()) : child.created).toString(Qt::ISODate) },
      { "Time", int(child.metadata.value("$duration").toDouble()) },
      { "duration", child.metadata.value("$duration") },
      { "Album", child.metadata.value("ALBUM") },
      { "Artist", child.metadata.value("ARTIST") },
      { "AlbumArtist", child.metadata.value("ALBUMARTIST") },
      { "Title", child.metadata.value("TITLE", child.name) },
    };
    for (const QString& key : h.keys()) {
      if (h[key].toString().isEmpty()) {
        h.remove(key);
      }
    }
    if (child.type == MediaItem::Track) {
      partial({{ "file", QStringLiteral("/stream/%1/").arg(child.id) }});
    } else if (child.type == MediaItem::Folder) {
      partial({{ "directory", QStringLiteral("%1 [%2]").arg(child.name).arg(child.id) }});
    } else if (child.type == MediaItem::Playlist) {
      partial({{ "playlist", QStringLiteral("%1 [%2]").arg(child.name).arg(child.id) }});
    }
    partial(h);
  }
  ok();
}

void MpdSession::count(const QString& type, const QStringList& filters)
{
  QList<MediaItem> items = filter(type, filters.isEmpty() ? QString() : filters.first(), false);
  double duration = 0;
  for (const MediaItem& child : items) {
    duration += child.metadata.value("$duration").toDouble();
  }
  ok({{ "songs", items.size() }, { "playtime", duration }});
}

void MpdSession::add(const QString& path)
{
  int trackId = path.section("/", -2, -2).toInt();
  if (path.startsWith("/stream/") && trackId) {
    MediaItem item(MediaItem::Track, trackId);
    if (item.id > 0) {
      d->broadcastEvent("playlist");
      d->baseSession->enqueue(trackId);
      ok();
      return;
    }
  }
  ack(MpdProtocol::DoesNotExistError, "add", "not found");
}

void MpdSession::addid(const QString& path, const QString& pos)
{
  Q_UNUSED(pos);
  int trackId = path.section("/", -2, -2).toInt();
  if (path.startsWith("/stream/") && trackId) {
    MediaItem item(MediaItem::Track, trackId);
    if (item.id > 0) {
      d->broadcastEvent("playlist");
      d->baseSession->enqueue(trackId);
      ok({{ "Id", trackId }});
      return;
    }
  }
  ack(MpdProtocol::DoesNotExistError, "addid", "not found");
}

void MpdSession::delete_(const QString& range)
{
  MapleSession* s = d->baseSession;
  QPair<int, int> span = parseRange(range, s->queue.size());
  s->queue.erase(s->queue.begin() + span.first, s->queue.begin() + span.second);
  d->broadcastEvent("playlist");
  ok();
}

void MpdSession::deleteid(const QString& songId)
{
  bool found = d->baseSession->queue.removeOne(songId.toInt());
  if (found) {
    d->broadcastEvent("playlist");
    ok();
  } else {
    ack(MpdProtocol::DoesNotExistError, "deleteid", "not found");
  }
}

void MpdSession::load(const QString& playlist, const QString& range, const QString& pos)
{
  Q_UNUSED(range);
  Q_UNUSED(pos);
  int playlistId = getIdFromName(playlist);
  MediaItem item(MediaItem::Playlist, playlistId);
  if (item.id < 1) {
    ack(MpdProtocol::DoesNotExistError, "load", "not found");
    return;
  }
  // TODO: range/pos
  for (const MediaItem& child : item.children()) {
    d->baseSession->enqueue(child.id);
  }
  d->broadcastEvent("playlist");
  ok();
}

void MpdSession::clear()
{
  // TODO: queue mutator functions
  d->baseSession->queue.clear();
  d->broadcastEvent("playlist");
  ok();
}

void MpdSession::playid(const QString& _trackId)
{
  int trackId = _trackId.toInt();
  int pos = d->baseSession->queue.indexOf(trackId);
  if (pos < 0) {
    ack(MpdProtocol::DoesNotExistError, "playid", "not found");
  } else {
    d->setQueuePos(pos);
    play();
  }
}

void MpdSession::play()
{
  d->baseSession->play();
  ok();
  emit d->playerPlay();
}

void MpdSession::pause(const QString& newState)
{
  auto oldState = d->baseSession->state;
  if (newState.isEmpty()) {
    d->baseSession->togglePause();
  } else if (newState.toInt()) {
    d->baseSession->pause();
  } else if (oldState == MapleSession::Paused) {
    // Calling play() restarts the track, so don't call it unconditionally
    d->baseSession->play();
  }
  if (d->baseSession->state != oldState) {
    if (d->baseSession->state == MapleSession::Paused) {
      emit d->playerPause();
    } else {
      emit d->playerResume();
    }
  }
  ok();
}

void MpdSession::stop()
{
  bool wasPlaying = d->baseSession->state != MapleSession::Idle;
  d->baseSession->stop();
  ok();
  if (wasPlaying) {
    emit d->playerStop();
  }
}

void MpdSession::next()
{
  d->baseSession->playNext();
  if (d->baseSession->nowPlaying.id > 0) {
    ok();
  } else {
    ack(MpdProtocol::PlayerSyncError, "next", "not playing");
  }
}

void MpdSession::previous()
{
  MapleSession* s = d->baseSession;
  if (s->queuePos < 1) {
    ack(MpdProtocol::PlayerSyncError, "previous", "not playing");
    return;
  }
  s->seekQueue(s->queuePos - 1);
  s->play();
  if (d->baseSession->nowPlaying.id > 0) {
    ok();
  } else {
    ack(MpdProtocol::PlayerSyncError, "previous", "not playing");
  }
}

void MpdSession::getvol()
{
  double scaled = d->baseSession->volume * 100;
  ok({{ "volume", QString::number(int(scaled > 99.5 ? 100 : scaled)) }});
}

void MpdSession::setvol(const QString& volume)
{
  bool scaleOK = false;
  double scaled = volume.toDouble(&scaleOK) * 0.01;
  if (scaleOK && scaled >= 0.0 && scaled <= 1.0) {
    d->baseSession->setVolume(scaled);
    ok();
  } else {
    ack(MpdProtocol::ArgumentError, "setvol", "Integer expected or out of range");
  }
}

void MpdSession::volume(const QString& delta)
{
  bool scaleOK = false;
  double scaled = delta.toDouble(&scaleOK) * 0.01 + d->baseSession->volume;
  if (scaleOK && scaled >= 0.0 && scaled <= 1.0) {
    d->baseSession->setVolume(scaled);
    ok();
  } else {
    ack(MpdProtocol::ArgumentError, "volume", "Integer expected or out of range");
  }
}

void MpdSession::single(const QString& state)
{
  if (state == "0") {
    d->baseSession->singleMode = false;
    ok();
  } else if (state == "1") {
    d->baseSession->singleMode = true;
    ok();
  } else {
    ack(MpdProtocol::ArgumentError, "single", "Unsupported state");
  }
#ifdef MD_ENABLE_JUKEBOX
  JukeboxPlayer* jukebox = JukeboxPlayer::instance();
  jukebox->setLooping(d->baseSession->singleMode && d->baseSession->repeatMode);
#endif
}

void MpdSession::repeat(const QString& state)
{
  if (state == "0") {
    d->baseSession->repeatMode = false;
    ok();
  } else if (state == "1") {
    d->baseSession->repeatMode = true;
    ok();
  } else {
    ack(MpdProtocol::ArgumentError, "repeat", "Unsupported state");
  }
#ifdef MD_ENABLE_JUKEBOX
  JukeboxPlayer* jukebox = JukeboxPlayer::instance();
  jukebox->setLooping(d->baseSession->singleMode && d->baseSession->repeatMode);
#endif
}

void MpdSession::command_list_begin()
{
  useList = true;
  useListOK = false;
  listHalt = false;
}

void MpdSession::command_list_ok_begin()
{
  useList = true;
  useListOK = true;
  listHalt = false;
}

void MpdSession::command_list_end()
{
  useList = false;
  listHalt = false;
  int i = 0;
  bool ok = true;
  for (const CommandResponse& cr : cmdList) {
    if (cr.error == MpdProtocol::NoError) {
      if (useListOK && !cr.partial) {
        server->list_ok(id, cr.values, cr.binary);
      } else if (!cr.values.isEmpty()) {
        server->partial(id, cr.values, cr.binary);
      }
    } else {
      server->ack(id, cr.error, i, cr.values["command"].toString(), cr.values["message"].toString());
      ok = false;
      break;
    }
    ++i;
  }
  if (ok) {
    server->ok(id);
  }
  cmdList.clear();
}

void MpdSession::scanStarted()
{
  events << "update";
  checkIdle();
}

void MpdSession::scanFinished(bool updated)
{
  events << "update";
  if (updated) {
    events << "database";
  }
  checkIdle();
}

bool MpdSession::checkIdle()
{
  if (idling.isEmpty() || events.isEmpty()) {
    return false;
  }
  if (idling.contains("all") || events.intersects(idling)) {
    ok(QVariantHash{{ "changed", QVariant::fromValue<QStringList>(QStringList(events.values())) }});
    idling.clear();
    events.clear();
    return true;
  }
  return false;
}

void MpdSessionData::broadcastEvent(const QString& event)
{
  for (int i = sessions.size() - 1; i >= 0; --i) {
    QPointer<MpdSession> s = sessions[i];
    if (s.isNull()) {
      sessions.removeAt(i);
    } else {
      s->events << event;
      s->checkIdle();
    }
  }
}

int MpdSession::queueAt(int pos) const
{
  if (pos < 0 || pos >= d->baseSession->queue.size()) {
    return -1;
  }
  return d->baseSession->queue[pos];
}

QString MpdSession::sessionName() const
{
  return d->baseSession->sessionName;
}
