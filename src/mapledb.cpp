#include "mapledb.h"
#include "mapleapp.h"
#include "dirscanner.h"
#include "tagsm3u.h"
#include "database/mdbxenv.h"
#include "database/mdbxtable.h"
#include "metadata/metadataprovider.h"
#include <QDir>
#include <QFileInfo>
#include <QMimeDatabase>
#include <QtDebug>
#include <QThread>
#include <QDateTime>

MapleDB* MapleDB::instance()
{
  return mApp->database();
}

MapleDB::Filter::Filter(const QString& tag, const QString& value)
: tag(tag), min(value), max(value)
{
  // initializers only
}

MapleDB::Filter::Filter(const QString& tag, const QString& min, const QString& max)
: tag(tag), min(min), max(max)
{
  // initializers only
}

MapleDB::MapleDB(QObject* parent)
: QObject(parent), activeThread(nullptr)
{
  QObject::connect(&watcher, SIGNAL(directoryChanged(QString)), this, SLOT(throttledScan(QString)));
  QObject::connect(&watcher, SIGNAL(fileChanged(QString)), this, SLOT(throttledScan(QString)));

  throttle.setInterval(5000);
  throttle.setSingleShot(true);
  QObject::connect(&throttle, SIGNAL(timeout()), this, SLOT(runThrottledScan()));

  MdbxTxn txn(mEnv->startRead());
  MdbxTable tblPaths("paths", &txn);
  for (qint64 pathId : tblPaths.search("parent", -1)) {
    MdbxRecord recPath(tblPaths.get(pathId));
    addPath(recPath.value<QString>("path"));
  }
}

void MapleDB::addRootPath(const QString& path)
{
  QString absolute = QDir(path).absolutePath();
  MdbxTxn txn(mEnv->startWrite());
  MdbxTable tblPaths("paths", &txn);
  MdbxRecord row(tblPaths.get(absolute));
  if (!row.isValid()) {
    row = tblPaths.create({
      { "path", absolute },
      { "parent", -1 },
      { "modified", 0 },
    });
    qDebug() << "addRootPath" << path << row.id();
    txn.commit();
  }
  addPath(path);
}

void MapleDB::addPath(const QString& path)
{
  QString absolute = QDir(path).absolutePath();
  QString canonical = QDir(path).canonicalPath();
  {
    QMutexLocker lock(&mutex);
    queuedPaths << absolute;
    if (!watchedPaths.contains(canonical)) {
      watchedPaths << canonical;
      if (!watcher.addPath(canonical)) {
        qWarning() << "Error: Out of watchers trying to add" << path;
      }
    }
  }
}

bool MapleDB::start()
{
  if (watchedPaths.isEmpty()) {
    return false;
  }
  runThrottledScan(true);
  return true;
}

void MapleDB::throttledScan(const QString& path)
{
  {
    QMutexLocker lock(&mutex);
    queuedPaths << path;
  }
  runThrottledScan();
}

void MapleDB::runThrottledScan(bool recursive)
{
  QSet<QString> paths;
  QMutexLocker lock(&mutex);
  if (queuedPaths.isEmpty()) {
    // If there's nothing to do, then just stop now.
    return;
  }
  if (throttle.isActive()) {
    // Don't run too frequently.
    return;
  }
  throttle.start();
  if (activeThread) {
    // If there's already a thread running, abort.
    // The throttle will trigger a re-run when the current run is done.
    return;
  }
  paths.swap(queuedPaths);
  progress = 0;
  qDebug() << "scanStarted";
  activeThread = QThread::create([this, paths, recursive]{
    emit scanStarted();
    emit scanProgress(0);

    QList<qint64> toScan;
    QSet<qint64> seen;
    QHash<qint64, QString> newPlaylists;

    // Collect the root paths from the database.
    {
      MdbxTxn txn(mEnv->startRead());
      MdbxTable tblPaths("paths", &txn);
      toScan = tblPaths.search("parent", -1);
      MdbxTable tblTracks("tracks", &txn);
    }
    for (qint64 dirId : toScan) {
      seen << dirId;
    }
    int numItems = 0, itemsDone = 0;

    // Collect the list of folders and number of files to be scanned.
    // This isn't necessary for correctness but it only costs a fraction
    // of a second and makes the progress reporting more useful.
    for (int i = 0; i < toScan.size(); i++) {
      DirScanner scan(toScan[i], false, true);
      numItems += scan.modifiedCount;
      for (qint64 dirId : scan.dirs) {
        if (!seen.contains(dirId)) {
          toScan << dirId;
          seen << dirId;
        }
      }
    }
    if (!numItems) {
      // If no modified files were detected, go ahead and stop here.
      qDebug() << "scanFinished - no action required";
      emit scanFinished(false);
      return;
    }

    // Scan for new/modified tracks and modified playlists.
    // Save new playlists for later because it's slow.
    for (int i = 0; i < toScan.size(); i++) {
      DirScanner scan(toScan[i]);
      for (const QString& path : scan.modifiedFiles) {
        QFileInfo info(path);
        QString mt = mimeType(info);
        bool isPlaylist = mt.contains("mpegurl");
        bool maybeTrack = !isPlaylist && (mt.startsWith("audio/") || mt.startsWith("video/"));
        QHash<QString, QString> metadata;

        MdbxTxn txn(mEnv->startWrite());
        MdbxTable tblPaths("paths", &txn);
        MdbxRecord row(tblPaths.get(path));
        if (!row.isValid()) {
          qWarning() << "Data integrity warning: no record found for" << path;
          ++itemsDone;
          continue;
        }
        QList<qint64> trackIds = row.links("tracks");
        bool isNew = true;
        if (trackIds.size()) {
          isNew = false;
          for (qint64 trackId : trackIds) {
            MediaItem track(MediaItem::Track, trackId);
            track.loadMetadata(&txn, true);
            track.saveMetadata(&txn);
          }
        } else {
          QList<qint64> collectionIds = row.links("collections");
          if (collectionIds.size()) {
            isNew = false;
            for (qint64 collectionId : collectionIds) {
              MediaItem collection(MediaItem::AnyCollection, collectionId);
              collection.loadMetadata(&txn, true);
              collection.saveMetadata(&txn);
            }
          }
        }
        if (isNew) {
          if (isPlaylist) {
            newPlaylists[row.id()] = path;
          } else if (maybeTrack) {
            MediaItem newItem(MediaItem::Track);
            newItem.pathId = row.id();
            newItem.fullPath = path;
            newItem.loadMetadata(&txn, true);
            newItem.saveMetadata(&txn);
          }
        }
        if (isNew && isPlaylist) {
          row.setValue("modified", 0);
        } else {
          ++itemsDone;
          row.setValue("modified", info.lastModified().toSecsSinceEpoch());
        }
        row.save();
        txn.commit();
        if (!isNew || !isPlaylist) {
          progress = (1.0 * itemsDone) / numItems;
          emit scanProgress(progress);
        }
      }
      qDebug() << "Progress:" << itemsDone << "/" << numItems << scan.path;
    }

    // Scan new playlists as a separate pass because it can be slow.
    for (qint64 playlistPathId : newPlaylists.keys()) {
      MdbxTxn txn(mEnv->startWrite());
      // TODO: should this recursively create items? Maybe but will need to collect metadata
      MediaItem newItem(MediaItem::Playlist);
      newItem.pathId = playlistPathId;
      newItem.fullPath = newPlaylists[playlistPathId];
      newItem.loadMetadata(&txn, true);
      newItem.saveMetadata(&txn);
      txn.commit();
      ++itemsDone;
      qDebug() << "Progress:" << itemsDone << "/" << numItems;
    }

    updateAlbums();
    updateArtists();
    qDebug() << "albums finished";

    progress = 1;
    emit scanFinished(true);
    qDebug() << "scan finished";
  });
  QObject::connect(activeThread, SIGNAL(finished()), activeThread, SLOT(deleteLater()));
  activeThread->start();
}

void MapleDB::updateAlbums()
{
  QHash<QPair<QString, QString>, qint64> albumIdsByArtist;
  QHash<QPair<QString, qint64>, qint64> albumIdsByFolder;
  QSet<qint64> albumsToSort;
  QList<qint64> trackIds;
  {
    MdbxTxn txn(mEnv->startRead());
    MdbxTable tblTracks("tracks", &txn);
    trackIds = tblTracks.search("no_album", 2);
  }
  for (qint64 trackId : trackIds) {
    MdbxTxn txn(mEnv->startWrite());
    MdbxTable tblTracks("tracks", &txn);
    MdbxTable tblPaths("paths", &txn);
    MdbxTable tblCollections("collections", &txn);
    MdbxRecord recTrack(tblTracks.get(trackId));
    qint64 folderId = 0;
    for (qint64 pathId : recTrack.links("paths")) {
      MdbxRecord recPath(tblPaths.get(pathId));
      if (recPath.isValid()) {
        folderId = recPath.value<qint64>("parent");
      }
    }
    QString albumName = recTrack.value<QString>("tags", "ALBUM");
    QString albumArtistName = recTrack.value<QString>("tags", "ALBUMARTIST");
    if (albumArtistName.isEmpty()) {
      albumArtistName = recTrack.value<QString>("tags", "ARTIST");
    }
    QPair<QString, QString> albumBlankKey(albumName, QString());
    QPair<QString, QString> albumArtistKey(albumName, albumArtistName);
    QPair<QString, qint64> albumFolderKey(albumName, folderId);
    qint64 idByAlbumArtist = albumIdsByArtist.value(albumArtistKey, 0);
    qint64 idByName = albumIdsByArtist.value(albumBlankKey, 0);
    qint64 idByFolder = albumIdsByFolder.value(albumFolderKey, 0);
    if (!idByAlbumArtist && !idByName && !idByFolder) {
      for (qint64 albumId : tblCollections.search("name", albumName)) {
        MdbxRecord recAlbum(tblCollections.get(albumId));
        if (recAlbum.value<int>("type") != MediaItem::Album) {
          // An artist or playlist and an album have the same name.
          continue;
        }
        QString artistFromAlbum = recAlbum.value<QString>("tags", "ARTIST");
        if (!albumArtistName.isEmpty() && artistFromAlbum == albumArtistName) {
          albumIdsByArtist[albumArtistKey] = albumIdsByArtist[albumBlankKey] = idByAlbumArtist = albumId;
        } else if (folderId && recAlbum.value<qint64>("folderId") == folderId) {
          albumIdsByFolder[albumFolderKey] = idByFolder = albumId;
        } else {
          albumIdsByArtist[albumBlankKey] = idByName = albumId;
        }
      }
    }
    // Prioritize by specificity
    qint64 albumId = idByAlbumArtist ? idByAlbumArtist :
      idByFolder ? idByFolder :
      idByName;
    if (!albumId) {
      // No match found; create a new one
      MdbxRecord recAlbum(tblCollections.create({
        { "type", int(MediaItem::Album) },
        { "tags", QVariantHash{
          { "ARTIST", albumArtistName },
        }},
        { "folderId", folderId },
        { "name", albumName },
      }));
      albumId = recAlbum.id();
      albumIdsByArtist[albumArtistKey] = albumIdsByArtist[albumBlankKey] = albumId;
      albumIdsByFolder[albumFolderKey] = albumId;
    }
    recTrack.setValue("tags", "_albumId", albumId);
    recTrack.remove("no_album");
    recTrack.link("collections", albumId);
    recTrack.save();
    // TODO: disconnect artist if different?
    txn.commit();
    albumsToSort << albumId;
  }
  for (qint64 albumId : albumsToSort) {
    MdbxTxn txn(mEnv->startWrite());
    MediaItem album(MediaItem::Album, albumId);
    QList<MediaItem> tracks = album.children(&txn, true);
    MediaItem::sort(tracks, "TRACK");
    MdbxTable tblCollections("collections", &txn);
    MdbxRecord recCollection(tblCollections.get(albumId));
    QList<qint64> childOrder;
    for (const MediaItem& item : tracks) {
      childOrder << item.id;
    }
    recCollection.setValue("items", childOrder);
    recCollection.save();
    txn.commit();
  }
}

void MapleDB::updateArtists()
{
  QHash<QString, QList<qint64>> tracksByArtist;
  QHash<QString, QSet<qint64>> albumsByArtist;
  {
    MdbxTxn txn(mEnv->startRead());
    MdbxTable tblTracks("tracks", &txn);
    for (qint64 trackId : tblTracks.search("no_artist", 2)) {
      MdbxRecord recTrack(tblTracks.get(trackId));
      int albumId = recTrack.value<int>("tags", "_albumId");
      QString artist = recTrack.value<QString>("tags", "ARTIST");
      if (!artist.isEmpty()) {
        tracksByArtist[artist] << trackId;
        if (albumId) {
          albumsByArtist[artist] << albumId;
        }
      }
      QString albumArtist = recTrack.value<QString>("tags", "ALBUMARTIST");
      if (!albumArtist.isEmpty()) {
        tracksByArtist[albumArtist] << trackId;
        if (albumId) {
          albumsByArtist[albumArtist] << albumId;
        }
      }
    }
  }
  MdbxTxn txn(mEnv->startWrite());
  MdbxTable tblCollections("collections", &txn);
  MdbxTable tblTracks("tracks", &txn);
  for (const QString& artist : tracksByArtist.keys()) {
    MdbxRecord recCollection;
    for (qint64 collectionId : tblCollections.search("name", artist)) {
      recCollection = tblCollections.get(collectionId);
      if (recCollection.value<int>("type") == MediaItem::Artist) {
        break;
      }
    }
    if (!recCollection.isValid() || recCollection.value<int>("type") != MediaItem::Artist) {
      recCollection = tblCollections.create({
        { "name", artist },
        { "type", int(MediaItem::Artist) },
      });
    }
    QVariantList items = recCollection.value<QVariantList>("items");
    for (qint64 trackId : tracksByArtist[artist]) {
      if (!items.contains(trackId)) {
        items << trackId;
        recCollection.link("tracks", trackId);
      }
      MdbxRecord recTrack(tblTracks.get(trackId));
      recTrack.remove("no_artist");
      recTrack.setValue("_artistId", recCollection.id());
      recTrack.save();
    }
    recCollection.setValue("items", items);
    // TODO: this schema doesn't work, switch to a list
    for (qint64 albumId : albumsByArtist[artist]) {
      MdbxRecord recAlbum(tblCollections.get(albumId));
      if (!recAlbum.value<int>("parent")) {
        recAlbum.setValue("parent", recCollection.id());
        recAlbum.save();
      }
    }
    recCollection.save();
  }
  txn.commit();
}

QString MapleDB::mimeType(const QFileInfo& info) const
{
  return MetadataProvider::mimeType(info);
}

double MapleDB::currentScanProgress() const
{
  if (!activeThread) {
    return -1;
  }
  return progress;
}

QList<MediaItem> MapleDB::rootFolders() const
{
  return MediaItem::getAll(MediaItem::Folder, true);
}

QList<MediaItem> MapleDB::searchTracks(const QList<Filter>& filters) const
{
  MdbxTxn txn(mEnv->startRead());
  MdbxTable tblTracks("tracks", &txn);
  QSet<qint64> ids;
  bool first = true;
  for (const Filter& filter : filters) {
    QSet<qint64> matches;
    if (filter.min == filter.max) {
      matches = QSet<qint64>::fromList(tblTracks.search("tags", filter.tag, filter.min));
    } else {
      matches = tblTracks.searchRange("tags", filter.tag, filter.min, filter.max);
    }
    if (first) {
      ids = matches;
      first = false;
    } else {
      ids.intersect(matches);
    }
    if (ids.isEmpty()) {
      return QList<MediaItem>();
    }
  }
  return MediaItem::getMultiple(MediaItem::Track, ids.toList(), false, &txn);
}
