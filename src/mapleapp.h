#ifndef MD_MAPLEAPP_H
#define MD_MAPLEAPP_H

#include <QxtCommandOptions>
#include <QHash>
#ifdef Q_OS_MAC
#include <QGuiApplication>
#define BASE_APPLICATION QGuiApplication
#else
#include <QCoreApplication>
#define BASE_APPLICATION QCoreApplication
#endif
class MapleDB;
class MdbxEnv;
class HttpServer;
class MpdServer;

class MapleApp : public BASE_APPLICATION
{
Q_OBJECT
public:
  static QString findBinary(const QStringList& names);
  static QString hashPassword(const QString& password, const QString& salt = QString());
  static bool checkPassword(const QString& password, const QString& hash);
  static int authenticateUser(const QString& username, const QString& password);

  MapleApp(int argc, char** argv);
  ~MapleApp();

  inline const QxtCommandOptions* options() const { return &opt; }
  inline MapleDB* database() const { return db; }

  QString transcoder(const QString& mimeType) const;
  const QHash<QString, QString>& transcoders() const;

  int exec();

private:
  void fatal(const QString& message, int status = 1);
  void showHelp();
  void bootstrap();

  QHash<QString, QString> transcoderBins;

  QxtCommandOptions opt;
  HttpServer* server;
  MpdServer* mpd;
  MapleDB* db;
  MdbxEnv* mdbx;
  bool shouldShowHelp;
  int exitStatus;
};

#define mApp static_cast<MapleApp*>(QCoreApplication::instance())

#endif
