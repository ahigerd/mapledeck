#include "jukeboxplayer.h"
#include "riffparser.h"
#include "../streamtranscode.h"
#include "../maplesession.h"
#include <QAudioDeviceInfo>
#include <QAudioOutput>
#include <QtDebug>

static QPointer<JukeboxPlayer> JukeboxPlayer_instance = nullptr;

JukeboxPlayer* JukeboxPlayer::instance()
{
  return JukeboxPlayer_instance.data();
}

JukeboxPlayer::JukeboxPlayer(QObject* parent)
: QObject(parent), currentSession(nullptr), output(nullptr), player(nullptr), looping(false)
{
  JukeboxPlayer_instance = this;
  updateTimer.setSingleShot(true);
  updateTimer.setInterval(16);
  QObject::connect(&updateTimer, SIGNAL(timeout()), this, SLOT(fillBuffer()));
}

bool JukeboxPlayer::claim(MapleSession* session)
{
  if (currentSession == session) {
    return true;
  } else if (!currentSession || !output || output->state() == QAudio::IdleState) {
    if (currentSession) {
      QObject::disconnect(currentSession, nullptr, this, nullptr);
    }
    currentSession = session;
    if (session) {
      QObject::connect(session, SIGNAL(started()), this, SLOT(play()));
      QObject::connect(session, SIGNAL(paused()), this, SLOT(pause()));
      QObject::connect(session, SIGNAL(resumed()), this, SLOT(resume()));
      QObject::connect(session, SIGNAL(stopped()), this, SLOT(stop()));
      QObject::connect(session, SIGNAL(volumeChanged(double)), this, SLOT(setVolume(double)));
    }
    return true;
  } else {
    return false;
  }
}

bool JukeboxPlayer::isClaimed(MapleSession* session) const
{
  return session == currentSession;
}

void JukeboxPlayer::play()
{
  skipTime = 0;
  skipBytes = 0;
  if (!currentSession || currentSession->nowPlaying.id <= 0) {
    stop();
    return;
  }
  if (output) {
    output->deleteLater();
    output = nullptr;
  }
  buffer.clear();
  if (transcode) {
    transcode->deleteLater();
    transcode = nullptr;
  }
  currentSession->nowPlaying.loadMetadata();
  if (QMediaPlayer::hasSupport(currentSession->nowPlaying.metadata.value("$mimetype")) >= QMultimedia::MaybeSupported) {
    if (!player) {
      player = new QMediaPlayer(this, QMediaPlayer::StreamPlayback);
      player->setAudioRole(QAudio::MusicRole);
      player->setPlaylist(&playlist);
      QObject::connect(player, SIGNAL(error(QMediaPlayer::Error)), this, SLOT(startTranscode()));
      QObject::connect(player, SIGNAL(stateChanged(QMediaPlayer::State)), this, SLOT(playerStateChanged(QMediaPlayer::State)));
      QObject::connect(player, SIGNAL(positionChanged(qint64)), this, SLOT(positionChanged(qint64)));
    }
    playlist.clear();
    playlist.addMedia(QUrl::fromLocalFile(currentSession->nowPlaying.fullPath));
    playlist.setPlaybackMode(QMediaPlaylist::CurrentItemOnce);
    player->play();
  } else {
    startTranscode();
  }
}

void JukeboxPlayer::startTranscode()
{
  if (player) {
    qDebug() << "Player error, using transcode";
    player->deleteLater();
    player = nullptr;
  }
  transcode = StreamTranscode::transcode(currentSession->nowPlaying, StreamTranscode::Riff, this);
  doneTranscoding = false;
  QObject::connect(transcode, SIGNAL(transcodeStarted()), this, SLOT(transcodeStarted()));
  QObject::connect(transcode, SIGNAL(transcodeFailed()), this, SLOT(songFinished()));
  QObject::connect(transcode, SIGNAL(transcodeFinished()), this, SLOT(transcodeFinished()));
  if (output) {
    playStart = output->processedUSecs() * -0.000001;
  } else {
    playStart = 0;
  }
  totalBytes = 0;
}

void JukeboxPlayer::pause()
{
  updateTimer.stop();
  if (player) {
    player->pause();
  } else if (output) {
    output->suspend();
  }
}

void JukeboxPlayer::resume()
{
  if (player && player->state() == QMediaPlayer::PausedState) {
    player->play();
  } else if (output && output->state() == QAudio::SuspendedState) {
    output->resume();
    updateTimer.start();
  } else {
    play();
  }
}

void JukeboxPlayer::stop()
{
  updateTimer.stop();
  if (player && player->state() != QMediaPlayer::StoppedState) {
    player->stop();
  }
  if (output && output->state() != QAudio::IdleState) {
    output->stop();
  }
  buffer.clear();
}

void JukeboxPlayer::seek(double timestamp)
{
  // TODO: playStart = timestamp
  if (player) {
    player->setPosition(timestamp * 1000);
  } else {
    skipTime = timestamp;
    skipBytes = 0;
  }
}

void JukeboxPlayer::songFinished()
{
  if (transcode) {
    transcode->deleteLater();
  }
  if (currentSession) {
    currentSession->playNext();
  } else {
    stop();
  }
}

double JukeboxPlayer::volume() const
{
  if (player) {
    return player->volume() * 0.01;
  }
  if (output) {
    return output->volume();
  }
  return 0;
}

double JukeboxPlayer::elapsed() const
{
  return currentSession->elapsed;
}

void JukeboxPlayer::setVolume(double volume)
{
  if (player) {
    player->setVolume(volume * 100);
  }
  if (output) {
    output->setVolume(volume);
  }
}

void JukeboxPlayer::transcodeStarted()
{
  QObject::connect(transcode->output(), SIGNAL(readyRead()), this, SLOT(transcodeReadyRead()));
  transcodeReadyRead();
}

void JukeboxPlayer::transcodeReadyRead()
{
  buffer += transcode->output()->readAll();
  RiffParser riff(buffer);
  if (!riff.isFinished) {
    return;
  }
  if (riff.isInvalid) {
    songFinished();
    return;
  }
  QObject::disconnect(transcode->output(), SIGNAL(readyRead()), this, SLOT(transcodeReadyRead()));
  QAudioFormat format;
  format.setCodec("audio/pcm");
  format.setChannelCount(riff.channels);
  format.setSampleSize(riff.sampleBits);
  format.setSampleRate(riff.sampleRate);
  format.setByteOrder(QAudioFormat::LittleEndian);
  format.setSampleType(riff.sampleBits == 8 ? QAudioFormat::UnSignedInt : QAudioFormat::SignedInt);
  if (looping) {
    setLooping(true);
  }
  int bytesPerSample = riff.channels * riff.sampleBits / 8;
  int bytesPerSecond = riff.sampleRate * bytesPerSample;
  if (currentTrackLooping) {
    if (loopEnd > 1000) {
      // loop points are in samples
      loopStartBytes = loopStart * bytesPerSample;
      loopEndBytes = loopEnd * bytesPerSample;
      loopStart /= riff.sampleRate;
      loopEnd /= riff.sampleRate;
    } else {
      loopStartBytes = loopStart * bytesPerSecond;
      loopEndBytes = loopEnd * bytesPerSecond;
    }
  }
  buffer.remove(0, riff.dataStart);
  if (skipTime > 0 || skipBytes > 0) {
    if (skipBytes <= 0) {
      skipBytes = skipTime * bytesPerSecond;
    }
    skipTime = 0;
  }
  if (!output || output->format() != format) {
    if (stream) {
      stream->deleteLater();
    }
    if (output) {
      output->deleteLater();
    }
    // TODO: select output device
    output = new QAudioOutput(format, this);
    playStart = 0;
    setVolume(currentSession->volume);
    output->setBufferSize(format.sampleRate() * format.channelCount() * format.sampleSize() * 33 / 8000);
    stream = nullptr;
    QObject::connect(output, SIGNAL(stateChanged(QAudio::State)), this, SLOT(stateChanged(QAudio::State)));
  }
  if (!stream) {
    stream = output->start();
    if (!stream) {
      qDebug() << "==== Error initializing stream" << format;
      // TODO: signal error
      songFinished();
      return;
    }
  }
  copyBuffer();
}

void JukeboxPlayer::transcodeFinished()
{
  doneTranscoding = true;
}

void JukeboxPlayer::fillBuffer()
{
  if (!output || !transcode) {
    return;
  }
  int needed = output->bytesFree();
  if (needed > 0 && transcode->output()) {
    buffer = transcode->output()->read(needed);
    copyBuffer();
  } else {
    updateTimer.start();
  }
  currentSession->elapsed = playStart + output->processedUSecs() * 0.000001;
}

void JukeboxPlayer::copyBuffer()
{
  int bytes = buffer.size();
  int endBytes = totalBytes + bytes;
  bool doLoop = false;
  if (totalBytes < skipBytes) {
    buffer.clear();
  } else if (endBytes >= skipBytes && skipBytes > totalBytes) {
    buffer.remove(0, skipBytes - totalBytes);
  } else if (currentTrackLooping && totalBytes < loopEndBytes && endBytes >= loopEndBytes) {
    buffer.remove(bytes - (endBytes - loopEndBytes), bytes);
    doLoop = true;
  }
  totalBytes = endBytes;
  bytes = buffer.size();
  if (bytes) {
    stream->write(buffer);
  }
  if (doLoop) {
    buffer.clear();
    QObject::disconnect(transcode, nullptr, this, nullptr);
    transcode->deleteLater();
    transcode = nullptr;
    skipBytes = loopStartBytes;
    playStart += (loopEnd - loopStart);
    startTranscode();
  } else if (bytes) {
    updateTimer.start();
  } else {
    QTimer::singleShot(0, this, SLOT(fillBuffer()));
  }
}

void JukeboxPlayer::stateChanged(QAudio::State state)
{
  if (state == QAudio::IdleState && doneTranscoding && !(transcode && transcode->output() && transcode->output()->bytesAvailable())) {
    songFinished();
  }
}

void JukeboxPlayer::playerStateChanged(QMediaPlayer::State state)
{
  if (state == QMediaPlayer::PlayingState) {
    if (output) {
      if (stream) {
        stream->deleteLater();
      }
      if (transcode) {
        transcode->deleteLater();
      }
      output->deleteLater();
      output = nullptr;
    }
  } else if (state == QMediaPlayer::StoppedState) {
    if (!playlist.isEmpty()) {
      songFinished();
    }
  }
}

void JukeboxPlayer::positionChanged(qint64 msecs)
{
  currentSession->elapsed = msecs * 0.001;
}

bool JukeboxPlayer::isLooping() const
{
  return looping;
}

void JukeboxPlayer::setLooping(bool loop)
{
  looping = loop;
  currentTrackLooping = false;
  if (looping && currentSession && currentSession->nowPlaying.id > 0) {
    const auto& metadata = currentSession->nowPlaying.metadata;
    if (metadata.contains("LOOPSTART")) {
      loopStart = metadata.value("LOOPSTART").toDouble();
      if (metadata.contains("LOOPEND")) {
        loopEnd = metadata.value("LOOPEND").toDouble();
      } else if (metadata.contains("LOOPLENGTH")) {
        loopEnd = loopStart + metadata.value("LOOPLENGTH").toDouble();
      } else {
        loopEnd = 0;
      }
      if (loopEnd > loopStart) {
        currentTrackLooping = true;
      }
    }
  }
}
