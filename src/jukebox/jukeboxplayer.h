#ifndef MD_JUKEBOXPLAYER_H
#define MD_JUKEBOXPLAYER_H

#include <QObject>
#include <QPointer>
#include <QAudio>
#include <QTimer>
#include <QMediaPlayer>
#include <QMediaPlaylist>
class QAudioOutput;
class QIODevice;
class MapleSession;
class StreamTranscode;

class JukeboxPlayer : public QObject
{
Q_OBJECT
public:
  static JukeboxPlayer* instance();

  JukeboxPlayer(QObject* parent = nullptr);

  bool claim(MapleSession* session);
  bool isClaimed(MapleSession* session) const;
  double volume() const;
  double elapsed() const;
  bool isLooping() const;

public slots:
  void play();
  void pause();
  void resume();
  void stop();
  void seek(double timestamp);
  void setVolume(double volume);
  void setLooping(bool loop);

private slots:
  void startTranscode();
  void transcodeStarted();
  void transcodeReadyRead();
  void transcodeFinished();
  void songFinished();
  void fillBuffer();
  void stateChanged(QAudio::State state);
  void playerStateChanged(QMediaPlayer::State state);
  void positionChanged(qint64 msecs);

private:
  void copyBuffer();

  MapleSession* currentSession;
  QAudioOutput* output;
  QMediaPlayer* player;
  QMediaPlaylist playlist;
  QPointer<QIODevice> stream;
  QPointer<StreamTranscode> transcode;
  QByteArray buffer;
  QByteArray loopBuffer;
  QTimer updateTimer;
  double playStart, skipTime;
  double loopStart, loopEnd;
  int skipBytes, loopStartBytes, loopEndBytes, totalBytes;
  bool doneTranscoding;
  bool looping;
  bool currentTrackLooping;
  bool extendLoopBuffer;
};

#endif
