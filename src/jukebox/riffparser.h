#ifndef MD_RIFFPARSER_H
#define MD_RIFFPARSER_H

#include <QByteArray>

struct RiffParser {
public:
  RiffParser(const QByteArray& buffer);

  bool isInvalid : 1;
  bool isFinished : 1;
  int dataStart;
  quint16 channels;
  quint32 sampleRate;
  quint16 sampleBits;
};

#endif
