#include "riffparser.h"
#include <QtDebug>

#define INVALID_IF(p) isInvalid = p; if (isInvalid) { qDebug() << "====" << #p; isFinished = true; return; }

RiffParser::RiffParser(const QByteArray& buffer)
{
  static const QByteArray subchunk1Size("\x10\0\0\0", 4);
  static const QByteArray pcm("\x1\0", 2);
  int length = buffer.length();
  INVALID_IF(length >= 4 && buffer.left(4) != "RIFF");
  INVALID_IF(length >= 12 && buffer.mid(8, 4) != "WAVE");
  INVALID_IF(length >= 16 && buffer.mid(12, 4) != "fmt ");
  INVALID_IF(length >= 20 && buffer.mid(16, 4) != subchunk1Size);
  INVALID_IF(length >= 22 && buffer.mid(20, 2) != pcm);
  dataStart = 0;
  int chunkStart = 36;
  while (chunkStart < length + 8 && dataStart == 0) {
    if (buffer.mid(chunkStart, 4) == "data") {
      dataStart = chunkStart + 8;
      isInvalid = false;
    } else {
      QByteArray chunkLenBytes = buffer.mid(chunkStart + 4, 4);
      INVALID_IF(chunkLenBytes.length() != 4);
      int chunkLen = (quint8)chunkLenBytes[0] | ((quint8)chunkLenBytes[1] << 8) | ((quint8)chunkLenBytes[2] << 16) | ((quint8)chunkLenBytes[3] << 24);
      chunkStart += chunkLen + 8;
    }
  }
  isFinished = dataStart != 0;
  if (isFinished) {
    channels = (quint8)buffer[22] | ((quint8)buffer[23] << 8);
    sampleRate = (quint8)buffer[24] | ((quint8)buffer[25] << 8) | ((quint8)buffer[26] << 16) | ((quint8)buffer[27] << 24);
    sampleBits = (quint8)buffer[34] | ((quint8)buffer[35] << 8);
  }
}
