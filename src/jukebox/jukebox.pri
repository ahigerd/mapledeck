contains(QT_MODULES,multimedia) {
  QT += multimedia
  macx {
    QT += gui
  }

  HEADERS += src/jukebox/jukeboxplayer.h   src/jukebox/riffparser.h
  SOURCES += src/jukebox/jukeboxplayer.cpp src/jukebox/riffparser.cpp
  DEFINES += MD_ENABLE_JUKEBOX=1
}
