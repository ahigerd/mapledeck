#include "codaprovider.h"
#include <QFileInfo>

CodaProvider::CodaProvider() : MetadataProvider() {}

QString CodaProvider::mimeTypeImpl(const QFileInfo& info)
{
  QString ext = info.suffix();
  QString base = info.absolutePath() + "/" + info.completeBaseName();
  if (ext == "1") {
    QString other1 = base + ".2dx";
    QString other2 = base + ".s3p";
    if (QFileInfo(other1).isReadable() || QFileInfo(other2).isReadable()) {
      return "audio/x-iidx";
    }
  } else if (ext == "ifs") {
    QString other = base + ".ifs";
    if (other.contains("_bgm.ifs")) {
      other = other.replace("_bgm.ifs", "_seq.ifs");
    } else {
      return QString();
    }
    if (QFileInfo(other).isReadable()) {
      return "audio/x-gitadora";
    }
  }
  return QString();
}

bool CodaProvider::canReadTags(const QFileInfo&, const QString&)
{
  return false;
}

bool CodaProvider::readTagsImpl(const QFileInfo&, QHash<QString, QString>&)
{
  return false;
}

static CodaProvider registrar;
