#include "id666provider.h"
#include <QFile>
#include <QFileInfo>
#include <QtEndian>
#include <QtDebug>

static QHash<char, const char*> tagTypes = {
  { 0x01, "TITLE" },
  { 0x02, "ALBUM" },
  { 0x03, "ARTIST" },
  { 0x04, "DUMPER" },
  { 0x05, "DATE" },
  { 0x07, "COMMENT" },
  { 0x10, "OST_TITLE" },
  { 0x11, "DISC" },
  { 0x12, "TRACK" },
  { 0x13, "PUBLISHER" },
  { 0x14, "YEAR" },
};

ID666Provider::ID666Provider() : MetadataProvider() {}

QString ID666Provider::mimeTypeImpl(const QFileInfo& info)
{
  if (info.size() < 0x010200) {
    return QString();
  }
  QFile spc(info.absoluteFilePath());
  if (spc.open(QIODevice::ReadOnly) && spc.read(11) == "SNES-SPC700") {
    return "audio/x-spc";
  }
  return QString();
}

bool ID666Provider::canReadTags(const QFileInfo& info, const QString&)
{
  if (info.size() < 0x010200) {
    return false;
  }
  QFile spc(info.absoluteFilePath());
  if (!spc.open(QIODevice::ReadOnly)) {
    return false;
  }
  QByteArray data = spc.read(36);
  return quint8(data[35]) == quint8(0x1a) && data.startsWith("SNES-SPC700");
}

static QString parseBinDate(const QByteArray& data) {
  quint16 year = qFromLittleEndian<quint16>(data.constData());
  quint8 month = data[2];
  quint8 day = data[3];
  if (!year) {
    return QString();
  }
  return QString("%1/%2/%3").arg(int(month), 2, 10, QChar('0')).arg(int(day), 2, 10, QChar('0')).arg(int(year), 4, 10, QChar('0'));
}

inline void setTag(QHash<QString, QString>& target, const QString& key, QString value)
{
  value = value.trimmed();
  if (value.isEmpty()) {
    return;
  }
  target[key] = value;
}

bool ID666Provider::readTagsImpl(const QFileInfo& info, QHash<QString, QString>& target)
{
  QFile spc(info.absoluteFilePath());
  if (!spc.open(QIODevice::ReadOnly)) {
    return false;
  }
  spc.seek(0x2e);
  setTag(target, "TITLE", QString::fromUtf8(spc.read(32)));
  setTag(target, "ALBUM", QString::fromUtf8(spc.read(32)));
  setTag(target, "DUMPER", QString::fromUtf8(spc.read(16)));
  setTag(target, "COMMENT", QString::fromUtf8(spc.read(32)));
  QByteArray data = spc.read(11);
  bool noDate = data == QByteArray(11, '\0');
  bool textFormat = noDate || (data[2] == '/' && data[5] == '/' && data[0] >= '0' && data[1] <= '1');
  double duration = 0, fade = 0;
  if (textFormat) {
    setTag(target, "DATE", QString::fromLatin1(data));
    duration = spc.read(3).toInt();
    fade =  spc.read(5).toInt() / 1000.0;
  } else {
    setTag(target, "DATE", parseBinDate(data));
    data = spc.read(3);
    data += '\0';
    duration = qFromLittleEndian<quint32>(data.constData());
    data = spc.read(4);
    fade = qFromLittleEndian<quint32>(data.constData()) / 1000.0;
  }
  if (fade) {
    setTag(target, "FADE", QString::number(fade));
    duration += fade;
  }
  setTag(target, "$duration", QString::number(duration));
  setTag(target, "ARTIST", QString::fromUtf8(spc.read(32)));

  if (!spc.seek(0x010200) || spc.read(4) != "xid6") {
    return true;
  }
  spc.skip(4);
  while (!spc.atEnd()) {
    data = spc.read(4);
    if (data.length() < 4) {
      break;
    }
    const char* tagName = tagTypes.value(data[0], nullptr);
    char dataType = data[1];
    quint16 len = qFromLittleEndian<quint16>(data.constData() + 2);
    QString value;
    if (dataType == 0) {
      value = QString::number(len);
    } else {
      data = spc.read(len);
      if (data.length() != len) {
        break;
      }
      quint16 padding = (len & 0x3) ? 4 - (len & 0x3) : 0;
      spc.skip(padding);
      if (dataType == 4) {
        quint64 intValue = 0;
        for (int i = 0; i < len; i++) {
          intValue = (intValue << 8) | data[i];
        }
        value = QString::number(intValue);
      } else if (dataType == 1) {
        value = QString::fromUtf8(data);
      } else {
        continue;
      }
      if (tagName) {
        setTag(target, tagName, value);
      }
    }
  }
  return true;
}

static ID666Provider registrar;
