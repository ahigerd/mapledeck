#include "taglibprovider.h"
#include <QtDebug>

#ifdef PC_HAVE_TAGLIB
#include <taglib.h>
#include <tag.h>
#include <fileref.h>
#include <tpropertymap.h>
#include <asftag.h>
#include <asffile.h>
#include <mp4tag.h>
#include <mp4file.h>
#include <id3v2tag.h>
#include <mpegfile.h>
#include <xiphcomment.h>
#include <oggfile.h>
#include <vorbisfile.h>
#include <opusfile.h>
#include <speexfile.h>
#include <oggflacfile.h>
#include <flacfile.h>
#include <QFileInfo>
#include <QFile>

TagLibProvider::TagLibProvider() : MetadataProvider(1) {}

static TagLib::FileRef openFileRef(const QFileInfo& info, const QString& mimeType, bool fast)
{
  auto props = fast ? TagLib::AudioProperties::Fast : TagLib::AudioProperties::Accurate;
  TagLib::File* file = nullptr;
  if (mimeType.startsWith("audio/ogg")) {
    if (mimeType.contains("vorbis")) {
      file = new TagLib::Ogg::Vorbis::File(qPrintable(info.absoluteFilePath()), props);
    } else if (mimeType.contains("opus")) {
      file = new TagLib::Ogg::Opus::File(qPrintable(info.absoluteFilePath()), props);
    } else if (mimeType.contains("flac")) {
      file = new TagLib::Ogg::FLAC::File(qPrintable(info.absoluteFilePath()), props);
    } else if (mimeType.contains("speex")) {
      file = new TagLib::Ogg::Speex::File(qPrintable(info.absoluteFilePath()), props);
    }
  } else if (mimeType.startsWith("audio/mp4")) {
    file = new TagLib::MP4::File(qPrintable(info.absoluteFilePath()), props);
  } else if (mimeType.startsWith("audio/mpeg")) {
    file = new TagLib::MPEG::File(qPrintable(info.absoluteFilePath()), props);
  } else if (mimeType.contains("flac")) {
    file = new TagLib::FLAC::File(qPrintable(info.absoluteFilePath()), props);
  }
  TagLib::FileRef tagFile;
  if (file) {
    return TagLib::FileRef(file);
  } else {
    return TagLib::FileRef(qPrintable(info.absoluteFilePath()), !fast, props);
  }
}

bool TagLibProvider::canReadTags(const QFileInfo& info, const QString& mimeType)
{
  TagLib::FileRef tagFile = openFileRef(info, mimeType, true);
  return !tagFile.isNull();
}

template<typename T>
static bool readTagsFrom(const T& map, QHash<QString, QString>& target)
{
  bool readSome = false;
  for (const auto& iter : map) {
    QString key = QString::fromUtf8(iter.first.toCString(true)).trimmed().toUpper();
    int idx = 0;
    for (const auto& iter2 : iter.second) {
      ++idx;
      QString value = QString::fromUtf8(iter2.toCString(true));
      if (idx > 1) {
        target[key + "_" + QString::number(idx)] = value;
      } else {
        target[key] = value;
      }
      readSome = true;
    }
  }
  return readSome;
}

bool TagLibProvider::readTagsImpl(const QFileInfo& info, QHash<QString, QString>& target)
{
  bool hasLength = target.contains("$duration");
  bool readSome = false;
  TagLib::FileRef tagFile = openFileRef(info, target.value("$mimetype"), false);
  if (!hasLength && tagFile.audioProperties()) {
    target["$duration"] = QString::number(tagFile.audioProperties()->lengthInMilliseconds() * .001);
    readSome = true;
  }
  if (tagFile.tag() && !tagFile.tag()->isEmpty()) {
    readSome = readTagsFrom(tagFile.tag()->properties(), target) || readSome;
    TagLib::Ogg::XiphComment* xiph = dynamic_cast<TagLib::Ogg::XiphComment*>(tagFile.tag());
    if (xiph) {
      readSome = readTagsFrom(xiph->fieldListMap(), target) || readSome;
    }
  }
  return readSome;
}

bool TagLibProvider::canReadImages(const QFileInfo& info)
{
  TagLib::FileRef tagFile(qPrintable(info.absoluteFilePath()), false);
  TagLib::File* file = tagFile.file();
  return dynamic_cast<TagLib::MPEG::File*>(file) ||
    dynamic_cast<TagLib::MP4::File*>(file) ||
    dynamic_cast<TagLib::ASF::File*>(file) ||
    dynamic_cast<TagLib::Ogg::File*>(file);
}

static void dedupeAdd(QHash<QString, QByteArray>& result, const QString& base, const QByteArray& data)
{
  int dupe = 0;
  QString key = base;
  while (result.contains(key)) {
    key = base + QString::number(++dupe);
  }
  result[key] = data;
}

template <typename Picture>
static void dedupeAdd(QHash<QString, QByteArray>& result, const Picture& picture, const TagLib::ByteVector& data)
{
  static QHash<typename Picture::Type, QString> keys{
    { Picture::Other, "Other" },
    { Picture::FileIcon, "FileIcon" },
    { Picture::OtherFileIcon, "OtherFileIcon" },
    { Picture::FrontCover, "FrontCover" },
    { Picture::BackCover, "BackCover" },
    { Picture::LeafletPage, "LeafletPage" },
    { Picture::Media, "Media" },
    { Picture::LeadArtist, "LeadArtist" },
    { Picture::Artist, "Artist" },
    { Picture::Conductor, "Conductor" },
    { Picture::Band, "Band" },
    { Picture::Composer, "Composer" },
    { Picture::Lyricist, "Lyricist" },
    { Picture::RecordingLocation, "RecordingLocation" },
    { Picture::DuringRecording, "DuringRecording" },
    { Picture::DuringPerformance, "DuringPerformance" },
    { Picture::MovieScreenCapture, "MovieScreenCapture" },
    { Picture::ColouredFish, "ColouredFish" },
    { Picture::Illustration, "Illustration" },
    { Picture::BandLogo, "BandLogo" },
    { Picture::PublisherLogo, "PublisherLogo" },
  };
  QString key = picture.description().toCString(true);
  if (key.isEmpty()) {
    key = keys[picture.type()];
  }
  dedupeAdd(result, key, QByteArray(data.data(), data.size()));
}

bool TagLibProvider::readImagesImpl(const QFileInfo& info, QHash<QString, QByteArray>& result, bool checkOnly)
{
  TagLib::FileRef tagFile(qPrintable(info.absoluteFilePath()), false);
  if (tagFile.isNull()) {
    return false;
  }
  TagLib::File* file = tagFile.file();
  auto id3 = dynamic_cast<TagLib::MPEG::File*>(tagFile.file());
  if (id3) {
    using namespace TagLib::ID3v2;
    auto tag = id3->ID3v2Tag();
    if (tag) {
      for (Frame* frame : tag->frameListMap()["APIC"]) {
        if (checkOnly) {
          return true;
        }
        auto picture = static_cast<AttachedPictureFrame*>(frame);
        dedupeAdd(result, *picture, picture->picture());
      }
    }
    return !result.isEmpty();
  }
  auto mp4 = dynamic_cast<TagLib::MP4::File*>(file);
  if (mp4) {
    using namespace TagLib::MP4;
    static QHash<CoverArt::Format, QString> keys{
      { CoverArt::JPEG, "JPEG" },
      { CoverArt::PNG, "PNG" },
      { CoverArt::BMP, "BMP" },
      { CoverArt::GIF, "GIF" },
      { CoverArt::Unknown, "Unknown" },
    };
    auto tag = static_cast<Tag*>(mp4->tag());
    if (tag) {
      CoverArtList covers = tag->itemListMap()["covr"].toCoverArtList();
      for (const auto& cover : covers) {
        if (checkOnly) {
          return true;
        }
        dedupeAdd(result, keys[cover.format()], QByteArray(cover.data().data(), cover.data().size()));
      }
    }
    return !result.isEmpty();
  }
  auto asf = dynamic_cast<TagLib::ASF::File*>(file);
  if (asf) {
    using namespace TagLib::ASF;
    auto tag = static_cast<Tag*>(asf->tag());
    if (tag) {
      for (const Attribute& attr : tag->attributeListMap()["WM/Picture"]) {
        if (checkOnly) {
          return true;
        }
        Picture picture = attr.toPicture();
        if (picture.isValid()) {
          dedupeAdd(result, picture, picture.picture());
        }
      }
    }
    return !result.isEmpty();
  }
  auto ogg = dynamic_cast<TagLib::Ogg::File*>(file);
  if (ogg) {
    using namespace TagLib::Ogg;
    auto tag = static_cast<XiphComment*>(ogg->tag());
    if (tag) {
      auto covers = tag->pictureList();
      for (const auto& cover : covers) {
        if (checkOnly) {
          return true;
        }
        dedupeAdd(result, *cover, cover->data());
      }
    }
    return !result.isEmpty();
  }
  return false;
}

static TagLibProvider registrar;
#endif
