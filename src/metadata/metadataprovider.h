#ifndef MD_METADATAPROVIDER_H
#define MD_METADATAPROVIDER_H

#include <QHash>
class QFileInfo;

class MetadataProvider {
public:
  static bool readTags(const QFileInfo& info, QHash<QString, QString>& target);
  static QString mimeType(const QFileInfo& info);
  static QHash<QString, QByteArray> readImages(const QFileInfo& info);
  static bool hasImages(const QFileInfo& info);

  virtual ~MetadataProvider();

  const int priority;
  const bool mayOverride;

  virtual QString mimeTypeImpl(const QFileInfo& info);
  virtual bool canReadTags(const QFileInfo& info, const QString& mimeType) = 0;
  virtual bool readTagsImpl(const QFileInfo& info, QHash<QString, QString>& target) = 0;
  virtual bool canReadImages(const QFileInfo& info);
  virtual bool readImagesImpl(const QFileInfo& info, QHash<QString, QByteArray>& result, bool checkOnly);

protected:
  MetadataProvider(int priority = 0, bool mayOverride = false);
};

#endif
