HEADERS += src/metadata/metadataprovider.h   src/metadata/m3uprovider.h   src/metadata/taglibprovider.h
SOURCES += src/metadata/metadataprovider.cpp src/metadata/m3uprovider.cpp src/metadata/taglibprovider.cpp

HEADERS += src/metadata/xsfprovider.h   src/metadata/mimeprovider.h   src/metadata/id666provider.h
SOURCES += src/metadata/xsfprovider.cpp src/metadata/mimeprovider.cpp src/metadata/id666provider.cpp

HEADERS += src/metadata/vgmstreamprovider.h   src/metadata/codaprovider.h   src/metadata/uadeprovider.h
SOURCES += src/metadata/vgmstreamprovider.cpp src/metadata/codaprovider.cpp src/metadata/uadeprovider.cpp

HEADERS += src/metadata/workercache.h
SOURCES += src/metadata/workercache.cpp
