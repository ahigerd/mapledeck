#include "vgmstreamprovider.h"
#include "../mapleapp.h"
#include <QFile>
#include <QFileInfo>
#include <QProcess>
#include <QMimeDatabase>

QStringList VgmstreamProvider::commandNames() { return { "vgmstream_cli", "vgmstream-cli", "vgmstream" }; }

VgmstreamProvider::VgmstreamProvider() : WorkerCache(false, 101) {}

QStringList VgmstreamProvider::scanCmd(const QDir& path)
{
  QStringList cmd{ "-m" };
  for (const QString& file : path.entryList(QDir::Files | QDir::Readable | QDir::NoDotAndDotDot)) {
    cmd << path.absoluteFilePath(file);
  }
  return cmd;
}

QStringList VgmstreamProvider::metaCmd(const QFileInfo& info)
{
  return QStringList() << "-m" << info.absoluteFilePath();
}

QString VgmstreamProvider::scanLine(const QString& line)
{
  if (line.startsWith("metadata for ")) {
    return line.section(" for ", 1);
  }
  return QString();
}

static const QHash<QString, QString> keyNames = {
  { "sample rate", "SAMPLERATE" },
  { "stream index", "SUBSONG" },
  { "stream name", "TITLE" },
  { "input channels", "" },
  { "output channels", "" },
  { "stream total samples", "" },
};

static QString getDuration(QString value)
{
  if (value.contains(" samples")) {
    return value.section(" samples", 0, 0);
  }
  value = value.section('(', -1).replace(" seconds)", "");
  int minutes = value.section(':', 0, 0).toInt();
  double seconds = value.section(':', 1, 1).toDouble();
  return QString::number(minutes * 60 + seconds);
}

bool VgmstreamProvider::parseMetadata(QHash<QString, QString>& target, const QStringList& lines)
{
  bool ok = false;
  for (const QString& line : lines) {
    if (line.startsWith("metadata for ")) {
      ok = true;
    } else {
      QString key = line.section(":", 0, 0).simplified();
      QString value = line.section(":", 1).simplified();
      if (key == "play duration") {
        target["$duration"] = getDuration(value);
      } else if (key == "loop start") {
        target["LOOPSTART"] = getDuration(value);
      } else if (key == "loop end") {
        target["LOOPEND"] = getDuration(value);
      } else if (key == "encoding") {
        QString codec = value.toLower();
        int parenPos;
        while ((parenPos = codec.indexOf('(')) >= 0) {
          int closePos = codec.indexOf(')', parenPos);
          codec.remove(parenPos, closePos - parenPos + 1);
        }
        target["$mimetype"] = "audio/x-vgmstream;codecs=" + codec.simplified().replace(" ", "-");
      } else if (key == "stream count") {
        target["SUBSONG_MIN"] = "0";
        target["SUBSONG_MAX"] = QString::number(value.toInt() - 1);
      } else if (keyNames.contains(key)) {
        key = keyNames.value(key);
        if (!key.isEmpty()) {
          target[key] = value;
        }
      } else if (!key.isEmpty()) {
        target[key.replace(" ", "").toUpper()] = value;
      }
    }
  }
  return ok;
}

static VgmstreamProvider registrar;
