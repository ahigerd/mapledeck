#ifndef MD_CODAPROVIDER_H
#define MD_CODAPROVIDER_H

#include "metadataprovider.h"

class CodaProvider : public MetadataProvider {
public:
  CodaProvider();

  QString mimeTypeImpl(const QFileInfo& info);
  bool canReadTags(const QFileInfo& info, const QString& mimeType);
  bool readTagsImpl(const QFileInfo& info, QHash<QString, QString>& target);
};

#endif
