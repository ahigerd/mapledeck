#include "m3uprovider.h"
#include <QFileInfo>
#include <QFile>
#include <QMutex>

M3UProvider::M3UProvider() : MetadataProvider(999, true) {}

bool M3UProvider::canReadTags(const QFileInfo& info, const QString&)
{
  return QFileInfo(TagsM3U::relativeTo(info.absoluteFilePath())).isReadable();
}

bool M3UProvider::readTagsImpl(const QFileInfo& info, QHash<QString, QString>& target)
{
  static QMutex mutex;
  {
    QMutexLocker lock(&mutex);
    QString path = TagsM3U::relativeTo(info.absoluteFilePath());
    if (path != cacheFilename) {
      QFile m3uFile(path);
      cacheValid = m3uFile.open(QIODevice::ReadOnly | QIODevice::Text);
      if (cacheValid) {
        cache = TagsM3U(&m3uFile);
      }
    }
    if (!cacheValid) {
      return false;
    }
  }
  TagMap tags = cache.allTags(info.fileName());
  for (const QString& key : tags.keys()) {
    if (key == "LENGTH") {
      target["$duration"] = tags[key];
    } else {
      target[key.toUpper()] = tags[key];
    }
  }
  return !tags.isEmpty();
}

static M3UProvider registrar;
