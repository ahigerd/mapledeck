#ifndef MD_M3UPROVIDER_H
#define MD_M3UPROVIDER_H

#include "metadataprovider.h"
#include "../tagsm3u.h"

class M3UProvider : public MetadataProvider {
public:
  M3UProvider();

  bool canReadTags(const QFileInfo& info, const QString& mimeType);
  bool readTagsImpl(const QFileInfo& info, QHash<QString, QString>& target);

private:
  mutable QString cacheFilename;
  mutable bool cacheValid;
  mutable TagsM3U cache;
};

#endif
