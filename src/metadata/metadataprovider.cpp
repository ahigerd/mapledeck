#include "metadataprovider.h"
#include <QList>
#include <QFileInfo>
#include <algorithm>
#include <typeinfo>
#include <QtDebug>

static const QHash<QString, QString> tagSubstitution{
  { "TRACKNUMBER", "TRACK" },
  { "LOOP_START", "LOOPSTART" },
  { "LOOP_END", "LOOPEND" },
  { "LOOP_LENGTH", "LOOPLENGTH" },
};

static QString extractYear(QString yearStr)
{
  QStringList parts;
  if (yearStr.contains('-')) {
    parts = yearStr.split('-');
  } else {
    parts = yearStr.split('/');
  }
  int numParts = parts.size();
  if (numParts > 1) {
    int longestLen = 0;
    for (int i = 0; i < numParts; i++) {
      if (parts[i].size() > longestLen) {
        longestLen = parts[i].size();
        yearStr = parts[i];
      }
    }
  }
  return yearStr;
}

static QList<MetadataProvider*> allProviders;

#include <typeinfo>
bool MetadataProvider::readTags(const QFileInfo& info, QHash<QString, QString>& target)
{
  if (!info.exists() || !info.isReadable()) {
    return false;
  }
  bool readSome = false;
  QString mimeType = target.value("$mimetype");
  for (MetadataProvider* p : allProviders) {
    if ((readSome && !p->mayOverride) || !p->canReadTags(info, mimeType)) {
      continue;
    }
    readSome = p->readTagsImpl(info, target) || readSome;
  }
  if (readSome) {
    for (const QString& key : target) {
      QString sub = tagSubstitution.value(key);
      if (!sub.isEmpty()) {
        target[sub] = target.take(key);
      }
    }
    if (target.contains("YEAR") && !target.contains("DATE")) {
      QString yearStr = target.value("YEAR");
      bool ok = false;
      yearStr.toInt(&ok);
      if (!ok) {
        target["DATE"] = yearStr;
        target["YEAR"] = extractYear(yearStr);
      }
    } else if (target.contains("DATE") && !target.contains("YEAR")) {
      target["YEAR"] = extractYear(target.value("DATE"));
    }
  }
  qDebug() << target;
  return readSome;
}

QString MetadataProvider::mimeType(const QFileInfo& info)
{
  if (!info.exists() || !info.isReadable()) {
    return QString();
  }

  QString result;
  for (MetadataProvider* p : allProviders) {
    result = p->mimeTypeImpl(info);
    qDebug() << info << typeid(*p).name() << result;;
    if (!result.isEmpty()) {
      return result;
    }
  }
  return "application/octet-stream";
}

bool MetadataProvider::hasImages(const QFileInfo& info)
{
  if (!info.exists() || !info.isReadable()) {
    return false;
  }
  QHash<QString, QByteArray> result;
  for (MetadataProvider* p : allProviders) {
    if (p->canReadImages(info)) {
      bool done = p->readImagesImpl(info, result, true);
      if (done) {
        return true;
      }
    }
  }
  return false;
}

QHash<QString, QByteArray> MetadataProvider::readImages(const QFileInfo& info)
{
  QHash<QString, QByteArray> result;
  if (!info.exists() || !info.isReadable()) {
    return result;
  }
  for (MetadataProvider* p : allProviders) {
    if (p->canReadImages(info)) {
      bool done = p->readImagesImpl(info, result, false);
      if (done) {
        break;
      }
    }
  }
  return result;
}

bool sortByPriority(const MetadataProvider* lhs, const MetadataProvider* rhs)
{
  return lhs->priority < rhs->priority;
}

MetadataProvider::MetadataProvider(int priority, bool mayOverride)
: priority(priority), mayOverride(mayOverride)
{
  allProviders << this;
  std::sort(allProviders.begin(), allProviders.end(), sortByPriority);
}

MetadataProvider::~MetadataProvider()
{
  allProviders.removeAll(this);
}

QString MetadataProvider::mimeTypeImpl(const QFileInfo&)
{
  return QString();
}

bool MetadataProvider::canReadImages(const QFileInfo&)
{
  return false;
}

bool MetadataProvider::readImagesImpl(const QFileInfo&, QHash<QString, QByteArray>&, bool)
{
  return false;
}
