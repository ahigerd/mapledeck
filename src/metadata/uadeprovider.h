#ifndef MD_UADEPROVIDER_H
#define MD_UADEPROVIDER_H

#include "workercache.h"

class UADEProvider : public WorkerCache<UADEProvider> {
public:
  static QStringList commandNames();

  UADEProvider();

protected:
  virtual QStringList scanCmd(const QDir& path);
  virtual QStringList metaCmd(const QFileInfo& info);
  virtual QString scanLine(const QString& line);
  virtual bool parseMetadata(QHash<QString, QString>& target, const QStringList& lines);
};

#endif
