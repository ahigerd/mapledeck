#include "xsfprovider.h"
#include <QFile>
#include <QFileInfo>
#include <QtEndian>

static const QHash<char, const char*> xsfTypes = {
  { 0x01, "psf" },
  { 0x02, "psf2" },
  { 0x11, "ssf" },
  { 0x12, "dsf" },
  { 0x13, "mdsf" },
  { 0x21, "usf" },
  { 0x22, "gsf" },
  { 0x23, "snsf" },
  { 0x24, "2sf" },
  { 0x25, "ncsf" },
  { 0x26, "dsesf" },
  { 0x27, "mpsf" },
  { 0x41, "qsf" },
};

XSFProvider::XSFProvider() : MetadataProvider() {}

static char getXsfVersion(const QFileInfo& info, QFile* xsf)
{
  xsf->setFileName(info.absoluteFilePath());
  if (!xsf->open(QIODevice::ReadOnly)) {
    return 0x00;
  }

  QByteArray data = xsf->read(3);
  if (data != "PSF") {
    return 0x00;
  }
  data = xsf->read(1);
  if (data.isEmpty()) {
    return 0x00;
  }
  return data[0];
}

static QString mimeTypeForVersion(char version)
{
  if (!version) {
    return QString();
  }
  QString format = xsfTypes.value(version);
  if (format.isEmpty()) {
    return QStringLiteral("audio/x-psf;magic=0x%1").arg(int(version), 0, 16);
  }
  return QStringLiteral("audio/x-psf;format=%1").arg(format);
}

QString XSFProvider::mimeTypeImpl(const QFileInfo& info)
{
  QFile xsf;
  char version = getXsfVersion(info, &xsf);
  if (version && info.fileName().endsWith("lib")) {
    // Don't return an "audio/" type because libs shouldn't be marked as playable
    return "application/x-psf;format=psflib";
  }
  return mimeTypeForVersion(version);
}

bool XSFProvider::canReadTags(const QFileInfo& info, const QString&)
{
  QFile xsf;
  return getXsfVersion(info, &xsf) != 0;
}

bool XSFProvider::readTagsImpl(const QFileInfo& info, QHash<QString, QString>& target)
{
  QFile xsf;
  char version = getXsfVersion(info, &xsf);
  if (!version) {
    return false;
  }

  target["$mimetype"] = mimeTypeForVersion(version);

  QByteArray data = xsf.read(4);
  quint32 reservedSize = qFromLittleEndian<quint32>(data.constData());
  data = xsf.read(4);
  quint32 compressedSize = qFromLittleEndian<quint32>(data.constData());
  xsf.skip(reservedSize);
  xsf.skip(compressedSize);

  while (!xsf.atEnd() && xsf.peek(5) != "[TAG]") {
    xsf.skip(1);
  }
  xsf.skip(5);

  if (!xsf.atEnd()) {
    QList<QByteArray> tagData = xsf.readAll().split('\n');
    for (const QByteArray& lineData : tagData) {
      QString line = QString::fromUtf8(lineData);
      if (!line.contains('=')) {
        continue;
      }
      QString key = line.section('=', 0, 0).toUpper();
      QString value = line.section('=', 1);
      if (key == "LENGTH") {
        int minutes = value.section(':', 0, 0).toInt();
        double seconds = value.section(':', 1, 1).toDouble();
        target["$duration"] = QString::number(minutes * 60 + seconds);
      }
      target[key] = value.trimmed();
    }
  }

  if (target.contains("GAME") && !target.contains("ALBUM")) {
    target["ALBUM"] = target["GAME"];
  }

  return true;
}

static XSFProvider registrar;
