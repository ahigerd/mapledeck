#ifndef MD_TAGLIBPROVIDER_H
#define MD_TAGLIBPROVIDER_H

#include "metadataprovider.h"

#ifdef PC_HAVE_TAGLIB
class TagLibProvider : public MetadataProvider {
public:
  TagLibProvider();

  bool canReadTags(const QFileInfo& info, const QString& mimeType);
  bool readTagsImpl(const QFileInfo& info, QHash<QString, QString>& target);
  bool canReadImages(const QFileInfo& info);
  bool readImagesImpl(const QFileInfo& info, QHash<QString, QByteArray>& result, bool checkOnly);
};
#endif

#endif
