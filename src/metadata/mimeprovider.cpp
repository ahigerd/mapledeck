#include "mimeprovider.h"
#include <QFileInfo>
#include <QMimeDatabase>
#include <QHash>
#include <QtDebug>

MimeProvider::MimeProvider() : MetadataProvider(100) {}

QString MimeProvider::mimeTypeImpl(const QFileInfo& info)
{
  QMimeDatabase mime;
  QMimeType mimeType = mime.mimeTypeForFile(info, QMimeDatabase::MatchContent);
  QString type = mimeType.name();
  if (mimeType.isDefault() || !(type.startsWith("audio/") || type.startsWith("video/"))) {
    mimeType = mime.mimeTypeForFile(info);
    if (mimeType.isDefault()) {
      return QString();
    }
    type = mimeType.name();
    if (type.startsWith("application/x-")) {
      // A nonstandardized MIME type in the application/ category has a decent
      // chance of being a false positive, so let the other providers take a guess.
      return QString();
    }
  }
  if (type.endsWith("+ogg")) {
    type = QString("%1/ogg;codecs=%2").arg(type.section('/', 0, 0)).arg(type.section('/', 1).replace("+ogg", "").replace("x-", ""));
  }
  return type;
}

bool MimeProvider::canReadTags(const QFileInfo&, const QString&)
{
  return false;
}

bool MimeProvider::readTagsImpl(const QFileInfo&, QHash<QString, QString>&)
{
  return false;
}

static MimeProvider registrar;
