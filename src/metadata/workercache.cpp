#include "workercache.h"
#include "../mapleapp.h"
#include "../mapledb.h"
#include <QMutex>
#include <QProcess>
#include <QtDebug>

static QMutex mutex(QMutex::Recursive);

WorkerCacheImpl::WorkerCacheImpl(bool recursive, int priority, bool mayOverride)
: MetadataProvider(priority, mayOverride), connected(false), recursive(recursive)
{
  // initializers only
}

void WorkerCacheImpl::rescan()
{
  QMutexLocker lock(&mutex);
  scanned.clear();
  cachePath.clear();
}

// True if it's worth trying
// False if it's known invalid
bool WorkerCacheImpl::checkCache(const QFileInfo& info)
{
  if (!connected) {
    bin = MapleApp::findBinary(commandNamesImpl());
    QObject::connect(mDB, &MapleDB::scanStarted, [this] { rescan(); });
    connected = true;
  }
  if (bin.isEmpty() || !info.isReadable()) {
    return false;
  }
  QMutexLocker lock(&mutex);
  // If it's in the cache, allow it to continue for further evaluation.
  if (found.contains(info.absoluteFilePath())) {
    return true;
  }
  // If the directory is in the cache, but the file isn't, we know it's not playable.
  return !isScanned(info.absolutePath());
}

bool WorkerCacheImpl::isScanned(QDir d)
{
  QMutexLocker lock(&mutex);
  if (!recursive) {
    return scanned.contains(d.absolutePath());
  }
  while (!d.isRoot()) {
    if (scanned.contains(d.absolutePath())) {
      return true;
    }
    d.cdUp();
  }
  return false;
}

void WorkerCacheImpl::scan(const QString& path)
{
  QMutexLocker lock(&mutex);
  if (isScanned(path)) {
    return;
  }
  scanned.insert(path);
  QProcess worker;
  worker.start(bin, scanCmd(path));
  if (!worker.waitForStarted() || !worker.waitForReadyRead()) {
    if (worker.exitStatus() != QProcess::NormalExit || worker.exitCode() != 0) {
      qWarning() << qPrintable(QString("Error launching %1 to scan %2").arg(bin).arg(path));
    }
    return;
  }
  QByteArray buffer;
  while (!worker.atEnd()) {
    buffer += worker.readAll();
    int pos;
    while ((pos = buffer.indexOf('\n')) >= 0) {
      QString line = QString::fromUtf8(buffer.left(pos));
      buffer = buffer.mid(pos + 1);
      if (!line.isEmpty()) {
        QString f = scanLine(line);
        if (!f.isEmpty()) {
          found.insert(f);
        }
      }
    }
    worker.waitForReadyRead();
  }
  worker.waitForFinished();
}

QString WorkerCacheImpl::mimeTypeImpl(const QFileInfo& info)
{
  if (!checkCache(info)) {
    return QString();
  }
  QHash<QString, QString> meta;
  readTagsImpl(info, meta);
  return meta.value("$mimetype");
}

bool WorkerCacheImpl::canReadTags(const QFileInfo& info, const QString&)
{
  return !mimeTypeImpl(info).isEmpty();
}

bool WorkerCacheImpl::readTagsImpl(const QFileInfo& info, QHash<QString, QString>& target)
{
  if (bin.isEmpty()) {
    return false;
  }
  QMutexLocker lock(&mutex);
  QString path = info.absoluteFilePath();
  // Since it's likely that this function might get called up to three times
  // in quick succession, cache the results.
  if (path == cachePath) {
    for (const QString& key : tagCache.keys()) {
      target[key] = tagCache.value(key);
    }
    return tagCache.contains("$mimetype");
  }
  scan(info.absolutePath());
  if (!found.contains(path)) {
    return false;
  }
  tagCache.clear();
  cachePath = path;

  QProcess worker;
  worker.start(bin, metaCmd(info));
  if (!worker.waitForStarted() || !worker.waitForReadyRead()) {
    if (worker.exitStatus() != QProcess::NormalExit || worker.exitCode() != 0) {
      qWarning() << qPrintable(QString("Error launching %1 to read %2").arg(bin).arg(info.absoluteFilePath()));
    }
    return false;
  }
  if (!worker.waitForFinished() || worker.exitStatus() == QProcess::CrashExit || worker.exitCode() != 0) {
    return false;
  }
  QStringList lines = QString::fromUtf8(worker.readAll()).split('\n');
  QHash<QString, QString> meta;
  bool ok = parseMetadata(meta, lines);
  tagCache = meta;
  bool finished = false;
  if (ok) {
    for (const QString& key : meta.keys()) {
      target[key] = meta.value(key);
      if (!key.startsWith('$')) {
        finished = true;
      }
    }
  }
  return finished;
}
