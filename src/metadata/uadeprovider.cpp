#include "uadeprovider.h"
#include "../mapledb.h"
#include "../mapleapp.h"
#include <QFileInfo>
#include <QtDebug>

QStringList UADEProvider::commandNames() { return { "uade123", "uade" }; }

UADEProvider::UADEProvider() : WorkerCache(true, 50) {}

QStringList UADEProvider::scanCmd(const QDir& path)
{
  return { "--scan", "-r", path.absolutePath() };
}

QStringList UADEProvider::metaCmd(const QFileInfo& info)
{
  return { "-g", info.absoluteFilePath() };
}

QString UADEProvider::scanLine(const QString& line)
{
  return line;
}

bool UADEProvider::parseMetadata(QHash<QString, QString>& target, const QStringList& lines)
{
  bool found = false;
  for (QString line : lines) {
    line = line.trimmed();
    QString key = line.section(':', 0, 0);
    QString value = line.section(':', 1).simplified();
    if (value.isEmpty()) {
      continue;
    } else if (key == "playername") {
      target["PLAYER"] = value;
      target["$mimetype"] = "audio/x-delitracker;player=" + value.section(' ', 0, 0).toLower();
      found = true;
    } else if (key == "modulename") {
      target["TITLE"] = value;
    } else if (key == "formatname") {
      target["FORMAT"] = value;
    } else if (key == "subsongs") {
      QStringList parts = value.split(' ');
      if (parts.size() != 6) {
        // unknown metadata format
        continue;
      }
      if (parts[3] == parts[5]) {
        // only one subsong
        continue;
      }
      target["SUBSONG"] = parts[1];
      target["SUBSONG_MIN"] = parts[3];
      target["SUBSONG_MAX"] = parts[5];
    }
  }
  return found;
}

static UADEProvider registrar;
