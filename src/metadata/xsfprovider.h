#ifndef MD_XSFPROVIDER_H
#define MD_XSFPROVIDER_H

#include "metadataprovider.h"

class XSFProvider : public MetadataProvider {
public:
  XSFProvider();

  QString mimeTypeImpl(const QFileInfo& info);
  bool canReadTags(const QFileInfo& info, const QString& mimeType);
  bool readTagsImpl(const QFileInfo& info, QHash<QString, QString>& target);
};

#endif
