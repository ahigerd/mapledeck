#ifndef MD_WORKERCACHE_H
#define MD_WORKERCACHE_H

#include "metadataprovider.h"
#include <QList>
#include <QSet>
#include <QPointer>
#include <QDir>

class WorkerCacheImpl : public MetadataProvider {
public:
  QString mimeTypeImpl(const QFileInfo& info);
  bool canReadTags(const QFileInfo& info, const QString& mimeType);
  bool readTagsImpl(const QFileInfo& info, QHash<QString, QString>& target);

protected:
  WorkerCacheImpl(bool recursive, int priority = 0, bool mayOverride = false);

  virtual QStringList commandNamesImpl() = 0;
  virtual QStringList scanCmd(const QDir& path) = 0;
  virtual QStringList metaCmd(const QFileInfo& info) = 0;
  virtual QString scanLine(const QString& line) = 0;
  virtual bool parseMetadata(QHash<QString, QString>& target, const QStringList& lines) = 0;

  void rescan();
  bool checkCache(const QFileInfo& path);
  bool isScanned(QDir d);
  void scan(const QString& path);

  QSet<QString> scanned, found;
  QHash<QString, QString> tagCache;
  QString cachePath, bin;
  bool connected, recursive;
};

template <class T>
class WorkerCache : public WorkerCacheImpl
{
protected:
  WorkerCache(bool recursive, int priority = 0, bool mayOverride = false)
  : WorkerCacheImpl(recursive, priority, mayOverride) {}

  virtual QStringList commandNamesImpl() { return T::commandNames(); }
};

#endif
