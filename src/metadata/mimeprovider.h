#ifndef MD_MIMEPROVIDER_H
#define MD_MIMEPROVIDER_H

#include "metadataprovider.h"

class MimeProvider : public MetadataProvider {
public:
  MimeProvider();

  QString mimeTypeImpl(const QFileInfo& info);
  bool canReadTags(const QFileInfo& info, const QString& mimeType);
  bool readTagsImpl(const QFileInfo& info, QHash<QString, QString>& target);
};

#endif
