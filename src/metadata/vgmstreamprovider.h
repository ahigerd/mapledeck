#ifndef MD_VGMSTREAMPROVIDER_H
#define MD_VGMSTREAMPROVIDER_H

#include "workercache.h"

class VgmstreamProvider : public WorkerCache<VgmstreamProvider> {
public:
  static QStringList commandNames();

  VgmstreamProvider();

protected:
  virtual QStringList scanCmd(const QDir& path);
  virtual QStringList metaCmd(const QFileInfo& info);
  virtual QString scanLine(const QString& line);
  virtual bool parseMetadata(QHash<QString, QString>& target, const QStringList& lines);
};

#endif
