#ifndef MD_ID666PROVIDER_H
#define MD_ID666PROVIDER_H

#include "metadataprovider.h"

class ID666Provider : public MetadataProvider {
public:
  ID666Provider();

  QString mimeTypeImpl(const QFileInfo& info);
  bool canReadTags(const QFileInfo& info, const QString& mimeType);
  bool readTagsImpl(const QFileInfo& info, QHash<QString, QString>& target);
};

#endif
