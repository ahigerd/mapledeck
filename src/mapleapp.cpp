#include "mapleapp.h"
#include "mapledb.h"
#include "web/httpserver.h"
#include "mpd/mpdserver.h"
#include "metadata/vgmstreamprovider.h"
#include "metadata/uadeprovider.h"
#include "database/mdbxenv.h"
#include "database/mdbxtable.h"
#include <QCryptographicHash>
#include <QRandomGenerator>
#include <QStandardPaths>
#include <QTextStream>
#include <QFileInfo>
#include <QSettings>
#include <QDir>

static QString execName;

struct Transcoder {
  const char* mimeType;
  const char* args;
  QStringList apps;
  const char* subsong;
};
static QList<Transcoder> transcoderApps = {
  { "audio/x-vgmstream", "-P {}", VgmstreamProvider::commandNames(), "-s {}" },
  { "audio/x-delitracker", "-c -1 {}", UADEProvider::commandNames(), "-s {}" },
  { "audio/x-psf;format=2sf", "{} -", { "2sf2wav" }, nullptr },
  { "audio/x-psf;format=ncsf", "{} -", { "ncsf2wav" }, nullptr },
  { "audio/x-psf;format=ssf", "{} -o -", { "ssf2wav" }, nullptr },
  { "audio/x-psf;format=dsf", "{} -o -", { "ssf2wav" }, nullptr },
  { "audio/x-iidx", "-o - {}", { "bemani2wav" }, nullptr },
  { "audio/x-gitadora", "-o - {}", { "gitadora2wav" }, nullptr },
};

QString MapleApp::findBinary(const QStringList& names)
{
  QHash<QString, QString> cache;
  if (cache.contains(names.first())) {
    return cache.value(names.first());
  }
  QDir appDir(QCoreApplication::applicationDirPath());
  for (QString app : names) {
#ifdef Q_OS_WIN
    app += ".exe";
#endif
    if (QFileInfo(appDir.absoluteFilePath(app)).isExecutable()) {
      return cache[names.first()] = appDir.absoluteFilePath(app);
    }
  }
  for (QString app : names) {
#ifdef Q_OS_WIN
    app += ".exe";
#endif
    QString path = QStandardPaths::findExecutable(app);
    if (!path.isEmpty()) {
      return cache[names.first()] = path;
    }
  }
  return cache[names.first()] = QString();
}

QString MapleApp::hashPassword(const QString& password, const QString& salt)
{
  QByteArray saltBA = salt.toUtf8();
  if (salt.isEmpty()) {
    quint64 s = QRandomGenerator::global()->generate64();
    for (int i = 0; i < 8; i++) {
      saltBA += char(s & 0xFF);
      s >>= 8;
    }
  } else {
    if (!saltBA.endsWith('=')) {
      saltBA += '=';
    }
    saltBA = QByteArray::fromBase64(saltBA);
  }
  QCryptographicHash h(QCryptographicHash::Sha3_512);
  QByteArray hash = password.toUtf8();
  for (int i = 0; i < 5000; i++) {
    h.reset();
    h.addData(saltBA);
    h.addData(hash);
    hash = h.result();
  }
  return QString::fromUtf8(saltBA.toBase64() + hash.toBase64());
}

bool MapleApp::checkPassword(const QString& password, const QString& hash)
{
  QString salt = hash.section('=', 0, 0);
  QString check = hashPassword(password, salt);
  return check == hash;
}

int MapleApp::authenticateUser(const QString& username, const QString& password)
{
  // For security, track whether or not there's been an error, but still run every step.
  // We can't do this perfectly because of optimizations in the underlying libraries, but
  // we can try to minimize the difference in elapsed time.
  bool err = username.isEmpty() || password.isEmpty();

  MdbxTxn txn(mEnv->startRead());
  MdbxTable tblUsers("users", &txn);
  MdbxRecord recUser(tblUsers.get(err ? QString() : username));
  QString incomingPassword;
  qint64 userId = recUser.id();
  if (recUser.isValid()) {
    incomingPassword = recUser.value<QString>("password");
  } else {
    incomingPassword = QString("%1=%2==").arg(QString(11, QChar('0'))).arg(QString(86, QChar('0')));
  }

  err = (userId < 1) || err;
  err = !MapleApp::checkPassword(password, incomingPassword) || err;
  if (err) {
    return -1;
  }
  return userId;
}

void MapleApp::fatal(const QString& message, int status)
{
  qWarning("%s: %s, aborting.", qPrintable(execName), qPrintable(message));
  if (!exitStatus) {
    exitStatus = status;
  }
  exit(status);
}

MapleApp::MapleApp(int argc, char** argv)
: BASE_APPLICATION(argc, argv), db(nullptr), mdbx(nullptr), exitStatus(0)
{
  execName = (arguments().mid(0, 1) << applicationFilePath()).first();

  opt.add("help", "show this help text");
  opt.alias("help", "h");

  opt.parse(arguments());
  shouldShowHelp = opt.count("help") || opt.showUnrecognizedWarning();
  if (shouldShowHelp) {
    return;
  }

  QSettings::setDefaultFormat(QSettings::IniFormat);
  QSettings settings;
  QString dbPath = settings.value("data/database").toString();
  // If the settings don't contain a path, try some standard locations.
  if (dbPath.isEmpty()) {
    // First see if one already exists beside the application binary.
    QString appDirPath = QDir(applicationDirPath()).absoluteFilePath("mapledeck.db");
    if (QFileInfo(appDirPath).isReadable()) {
      dbPath = appDirPath;
    } else {
      // If one doesn't already exist, check the standard writable data path.
      QString stdPath = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
      if (!stdPath.isEmpty()) {
        QString stdPathFile = QDir(stdPath).absoluteFilePath("mapledeck.db");
        if (QFileInfo(stdPathFile).isReadable() || QFileInfo(stdPath).isWritable()) {
          // If a file in the standard path already exists, or if it can be created, use it.
          dbPath = stdPathFile;
        }
      }
      if (dbPath.isEmpty()) {
        // If the standard path doesn't work, try beside the application binary.
        if (QFileInfo(applicationDirPath()).isWritable()) {
          dbPath = appDirPath;
        }
      }
    }
    if (dbPath.isEmpty()) {
      fatal("Unable to create database");
      return;
    }
  }
  if (QFileInfo(dbPath).exists() && !QFileInfo(dbPath).isWritable()) {
    fatal("Database is read-only");
    return;
  }
  mdbx = new MdbxEnv(dbPath);
  settings.setValue("data/database", dbPath);
  settings.sync();

  bootstrap();

  for (const auto& app : transcoderApps) {
    QString bin = MapleApp::findBinary(app.apps);
    if (bin.isEmpty()) {
      continue;
    }
    transcoderBins[app.mimeType] = QString("%1 %2").arg(bin).arg(app.args);
  }

  db = new MapleDB(this);
  for (const QString& path : opt.positional()) {
    db->addRootPath(path);
  }

  server = new HttpServer(this);
  mpd = new MpdServer(this);
}

MapleApp::~MapleApp()
{
  if (mdbx) {
    delete mdbx;
  }
}

int MapleApp::exec()
{
  if (shouldShowHelp) {
    showHelp();
    return opt.count("help") ? 0 : 1;
  }
  if (!server->start()) {
    fatal("Unable to start HTTP server");
  } else if (!mpd->start()) {
    fatal("Unable to start MPD server");
  } else if (!mDB->start()) {
    fatal("No media paths configured");
  }
  int execExitStatus = 0;
  if (!exitStatus) {
    execExitStatus = QCoreApplication::exec();
  }
  return execExitStatus ? execExitStatus : exitStatus;
}

void MapleApp::showHelp()
{
  QTextStream qout(stdout);
  QString appName = QDir(applicationFilePath()).dirName();
  qout << "Usage: " << appName << " [options] paths..." << endl;
  opt.showUsage();
}

void MapleApp::bootstrap()
{
  QSettings settings;

  MdbxTxn txn(mEnv->startWrite());
  MdbxTable tblUsers("users", &txn);
  if (tblUsers.rowCount() < 1) {
    // No existing users found, create the bootstrap user.
    QString username = settings.value("bootstrap/username").toString();
    QString password = settings.value("bootstrap/password").toString();
    if (username.isEmpty() || password.isEmpty()) {
      qWarning() << "Mapledeck First-Time Setup:";
      qWarning() << "Edit the file" << settings.fileName() << "to include the following:";
      qWarning() << "";
      qWarning() << "[bootstrap]\nusername=<username>\npassword=<password>";
      qWarning() << "";
      qWarning() << "An administrator will be created with this username and password.";
      qWarning() << "This information will be removed from the settings file after creation.";
      qWarning() << "";
      fatal("No bootstrap user");
      return;
    }
    try {
      tblUsers.create({
        { "username", username },
        { "password", hashPassword(password) },
      });
      txn.commit();
    } catch (mdbx::exception& e) {
      fatal(QStringLiteral("Unable to create bootstrap user: %1").arg(e.what()));
      return;
    }

    settings.remove("bootstrap");
  }
}

QString MapleApp::transcoder(const QString& mimeType) const
{
  QString transcodeCmd = transcoderBins.value(mimeType);
  if (transcodeCmd.isEmpty() && mimeType.contains(";")) {
    transcodeCmd = transcoderBins.value(mimeType.section(';', 0, 0));
  }
  return transcodeCmd;
}

const QHash<QString, QString>& MapleApp::transcoders() const {
  return transcoderBins;
}
