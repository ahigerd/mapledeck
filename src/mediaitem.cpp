#include "mediaitem.h"
#include "mapledb.h"
#include "tagsm3u.h"
#include "metadata/metadataprovider.h"
#include "database/mdbxtable.h"
#include <QDir>
#include <QFileInfo>
#include <QDateTime>
#include <QRandomGenerator>
#include <QtDebug>
#include <functional>

#define ENSURE_TXN(rw) MdbxTxn mTxn; if (!txn) { mTxn = mEnv->start ## rw(); txn = &mTxn; }

namespace {
#define CompareFnT(name, T) bool(name)(const T&, const T&)
#define CompareFn(name) CompareFnT(name,CompareType)

  struct AlbumSortKey {
    AlbumSortKey(const MediaItem& item) {
      disc = item.metadata.value("DISC", "0").toInt();
      track = item.metadata.value("TRACK", "0").toInt();
      filename = QDir(item.fullPath).dirName();
      fileNumber = filename.toInt();
    }
    AlbumSortKey() = default;
    AlbumSortKey(const AlbumSortKey&) = default;
    AlbumSortKey(AlbumSortKey&&) = default;
    AlbumSortKey& operator=(const AlbumSortKey& other) = default;
    AlbumSortKey& operator=(AlbumSortKey&& other) = default;

    inline bool operator==(const AlbumSortKey& other) const {
      return disc == other.disc && track == other.track && filename == other.filename;
    }

    inline bool operator<(const AlbumSortKey& other) const {
      if (!!disc == !other.disc) return !!disc;
      if (!!track == !other.track) return !!track;
      if (disc == other.disc) {
        if (track == other.track) {
          if (fileNumber == other.fileNumber) {
            return filename < other.filename;
          }
          return fileNumber < other.fileNumber;
        }
        return track < other.track;
      }
      return disc < other.disc;
    }

    inline bool operator>(const AlbumSortKey& other) const {
      if (!!disc == !other.disc) return !!disc;
      if (!!track == !other.track) return !!track;
      if (disc == other.disc) {
        if (track == other.track) {
          if (fileNumber == other.fileNumber) {
            return filename > other.filename;
          }
          return fileNumber > other.fileNumber;
        }
        return track > other.track;
      }
      return disc > other.disc;
    }

    int disc;
    int track;
    int fileNumber;
    QString filename;
  };

  template <typename T> inline bool isEmpty(const T& v) { return v; }
  inline bool isEmpty(const QString& v) { return v.isEmpty(); }
  inline bool isEmpty(const QDateTime& v) { return v.isNull(); }
  inline bool isEmpty(const AlbumSortKey&) { return false; }

  template <typename T>
  struct Box : public T {
    Box(const T& other) : T(other) {}
    inline operator bool() const { return !isEmpty(*this); }
    inline operator const T&() const { return *this; }
    inline bool operator<(const T& other) const { return *this < other; }
    inline bool operator>(const T& other) const { return *this > other; }
  };

  template <typename T> static inline bool less(const T& lhs, const T& rhs) { return lhs < rhs; }
  template <typename T> static inline bool greater(const T& lhs, const T& rhs) { return lhs > rhs; }
  static inline bool less(const QString& lhs, const QString& rhs) { return lhs.compare(rhs, Qt::CaseInsensitive) < 0; }
  static inline bool greater(const QString& lhs, const QString& rhs) { return lhs.compare(rhs, Qt::CaseInsensitive) > 0; }

  template <typename CompareType, CompareFn(Compare)=less>
  struct CompareBlankLast {
    inline static bool with(const CompareType& lhs, const CompareType& rhs) {
      return !isEmpty(lhs) && (isEmpty(rhs) || Compare(lhs, rhs));
    }
  };

  template <typename Type> struct Convert { static inline Type value(const QString& v) { return QVariant(v).value<Type>(); } };
#define CONVERT(type, expr) template<> struct Convert<type> { static inline type value(const QString& v) { return expr; } }
  CONVERT(QString, v);
  CONVERT(int, v.toInt());
  CONVERT(double, v.toDouble());
  CONVERT(QDateTime, QDateTime::fromSecsSinceEpoch(v.toLongLong()));

  template <typename Type> struct Extract { inline static Type value(const MediaItem& item, const char* key) { return Convert<Type>::value(item.metadata.value(key)); } };
#define EXTRACT(Name, type, expr) struct Name : public Box<type> { Name(const type& other) : Box(other) {} }; \
  template<> struct Extract<Name> { inline static Name value(const MediaItem& item, const char* key) { (void)key; return expr; } }; \
  template<> struct CompareBlankLast<Name, less> : CompareBlankLast<type, less> {}; \
  template<> struct CompareBlankLast<Name, greater> : CompareBlankLast<type, greater> {};
  EXTRACT(Title, QString, item.metadata.value(key, item.name));
  EXTRACT(Updated, QDateTime, item.lastUpdated.isValid() ? item.lastUpdated : item.created);
  EXTRACT(Created, QDateTime, item.created);
  EXTRACT(AlbumSort, AlbumSortKey, AlbumSortKey(item));

  template <typename CompareType, CompareFn(Compare)=less>
  struct Comparator {
    Comparator(const char* key) : key(key) {}
    inline bool operator()(const MediaItem& lhs, const MediaItem& rhs) {
      return CompareBlankLast<CompareType, Compare>::with(Extract<CompareType>::value(lhs, key), Extract<CompareType>::value(rhs, key));
    }
    const char* key;
  };
}

template <typename CompareType>
static inline void sortWorker(QList<MediaItem>& items, const char* key, Qt::SortOrder order)
{
  if (order == Qt::AscendingOrder) {
    std::stable_sort(items.begin(), items.end(), Comparator<CompareType, less>(key));
  } else {
    std::stable_sort(items.begin(), items.end(), Comparator<CompareType, greater>(key));
  }
}

void MediaItem::sort(QList<MediaItem>& items, const QString& key, Qt::SortOrder order)
{
  if (key == "created") {
    sortWorker<Created>(items, "created", order);
  } else if (key == "lastUpdated") {
    sortWorker<Updated>(items, "$lastUpdated", order);
  } else if (key.isEmpty() || key == "TITLE") {
    sortWorker<Title>(items, "TITLE", order);
  } else if (key == "random") {
    QRandomGenerator* rng = QRandomGenerator::global();
    int ct = items.size();
    for (int i = ct - 1; i >= 0; --i) {
      int j = rng->bounded(0, ct);
      std::swap(items[i], items[j]);
    }
  } else if (key == "TRACK") {
    sortWorker<AlbumSort>(items, qPrintable(key), order);
  } else {
    sortWorker<QString>(items, qPrintable(key), order);
  }
}

QList<MediaItem> MediaItem::getMultiple(ItemType type, const QList<qint64>& ids, bool getMeta, MdbxTxn* txn)
{
  ENSURE_TXN(Read);
  QList<MediaItem> result;
  for (qint64 id : ids) {
    MediaItem item(type, id);
    if (getMeta) {
      item.loadMetadata(txn);
      if (!item.isValid()) {
        continue;
      }
    }
    result << std::move(item);
  }
  return result;
}

QList<MediaItem> MediaItem::getAll(ItemType type, bool getMeta, MdbxTxn* txn)
{
  ENSURE_TXN(Read);
  if (type == Folder) {
    MdbxTable tblPaths("paths", txn);
    return getMultiple(Folder, tblPaths.search("parent", -1), getMeta, txn);
  } else if (type == Track) {
    MdbxTable tblTracks("tracks", txn);
    QList<MediaItem> result;
    for (const MdbxRecord& rec : tblTracks) {
      result << MediaItem(Track, rec, getMeta);
    }
    return result;
  } else if (type == AnyCollection) {
    MdbxTable tblCollections("collections", txn);
    QList<MediaItem> result;
    for (const MdbxRecord& rec : tblCollections) {
      result << MediaItem(ItemType(rec.value("type").toInt()), rec, getMeta);
    }
    return result;
  } else if (type == Search) {
    // invalid
    return QList<MediaItem>();
  } else {
    MdbxTable tblCollections("collections", txn);
    return getMultiple(type, tblCollections.search("type", int(type)), getMeta, txn);
  }
}

QByteArray MediaItem::getCoverArt(const QString& sourceString, MdbxTxn* txn)
{
  QStringList source = sourceString.split(":", QString::SkipEmptyParts);
  if (source.isEmpty() || source.first() == "-") {
    return QByteArray();
  }
  QByteArray image;
  int sourceId = source.first().toInt();
  if (sourceId) {
    MediaItem sourceItem(MediaItem::Track, sourceId);
    sourceItem.loadMetadata(txn, true);
    auto images = MetadataProvider::readImages(sourceItem.fullPath);
    if (!images.isEmpty()) {
      if (source.size() > 1) {
        image = images.value(source[1]);
      }
      if (image.isEmpty()) {
        image = images.value(images.keys().first());
      }
    }
  } else {
    // TODO: files by scheme
  }
  return image;
}

MediaItem::MediaItem(MediaItem::ItemType type, qint64 id)
: type(type), id(id), pathId(-1), parentId(-1), loadedShallow(false), loadedDeep(false)
{
  // initializers only
}

MediaItem::MediaItem(MediaItem::ItemType type, const MdbxRecord& rec, bool getMeta)
: type(type), id(rec.id()), pathId(-1), parentId(-1), loadedShallow(false), loadedDeep(false)
{
  if (!rec.isValid()) {
    qWarning() << "!!!" << type << "failed";
    type = Invalid;
    id = -1;
    return;
  }
  loadMetadata(rec.transaction(), rec, getMeta);
}

bool MediaItem::isValid() const
{
  return type != Invalid && id > 0;
}

QList<MediaItem> MediaItem::children(MdbxTxn* txn, bool getMeta) const
{
  ENSURE_TXN(Read);
  if (type == Folder) {
    MdbxTable tblPaths("paths", txn);
    MdbxRecord recPath(tblPaths.get(id));
    QList<qint64> trackIds, collectionIds, folderIds;
    for (qint64 childPathId : tblPaths.search("parent", id)) {
      QList<qint64> search = tblPaths.searchLinks("tracks", childPathId);
      if (search.size()) {
        trackIds += search;
        continue;
      }
      search = tblPaths.searchLinks("collections", childPathId);
      if (search.size()) {
        collectionIds += search;
        continue;
      }
      if (tblPaths.search("parent", childPathId).size()) {
        folderIds << childPathId;
      }
    }
    return getMultiple(Track, trackIds, getMeta, txn) +
      getMultiple(AnyCollection, collectionIds, getMeta, txn) +
      getMultiple(Folder, folderIds, getMeta, txn);
  } else if (type == Album || type == Playlist || type == Artist) {
    QList<qint64> childIds = childOrder;
    if (getMeta && childIds.isEmpty()) {
      MdbxTable tblCollections("collections", txn);
      childIds = tblCollections.searchLinks("tracks", id);
    }
    QList<MediaItem> result = getMultiple(Track, childIds, getMeta, txn);
    if (type == Artist) {
      MdbxTable tblCollections("collections", txn);
      QList<qint64> albumIds = tblCollections.search("parent", id);
      result += getMultiple(Album, albumIds, getMeta, txn);
    }
    return result;
  }
  return QList<MediaItem>();
}

QList<MediaItem> MediaItem::containers(bool getMeta) const
{
  QList<MediaItem> result;
  Q_UNUSED(getMeta);
  // TODO
  return result;
}

QList<MediaItem> MediaItem::containers(MediaItem::ItemType type, bool getMeta) const
{
  QList<qint64> ids;
  // TODO
  return getMultiple(type, ids, getMeta);
}

MediaItem MediaItem::album(bool getMeta) const
{
  MediaItem result;
  if (type != MediaItem::Track) {
    return result;
  }
  int albumId = metadata.value("$albumId", "0").toInt();
  if (!albumId) {
    QString albumName = metadata.value("ALBUM");
    if (albumName.isEmpty()) {
      return result;
    }
    QList<MediaItem> albums = containers(MediaItem::Album, false);
    int ct = albums.size();
    if (ct > 0) {
      albumId = albums[0].id;
      if (ct > 1) {
        for (int i = 1; i < ct; i++) {
          if (albums[i].name == albumName) {
            albumId = albums[i].id;
            break;
          }
        }
      }
    }
  }
  result = MediaItem(MediaItem::Album, albumId);
  if (result.type == MediaItem::Album && getMeta) {
    result.loadMetadata();
  }
  return result;
}

MediaItem MediaItem::artist(bool getMeta) const
{
  MediaItem result;
  if (type != MediaItem::Track && type != MediaItem::Album) {
    return result;
  }
  int artistId = metadata.value("$artistId", "0").toInt();
  if (!artistId) {
    QString artistName = metadata.value("ARTIST");
    if (artistName.isEmpty()) {
      return result;
    }
    QList<MediaItem> artists = containers(MediaItem::Artist, false);
    int ct = artists.size();
    if (ct > 0) {
      artistId = artists[0].id;
      if (ct > 1) {
        for (int i = 1; i < ct; i++) {
          if (artists[i].name == artistName) {
            artistId = artists[i].id;
            break;
          }
        }
      }
    }
  }
  result = MediaItem(MediaItem::Artist, artistId);
  if (result.type == MediaItem::Artist && getMeta) {
    result.loadMetadata();
  }
  return result;
}

QByteArray MediaItem::coverArt(MdbxTxn* txn) const
{
  if (type != MediaItem::Track && type != MediaItem::Album) {
    // TODO: decide how to handle artist artwork
    return QByteArray();
  }
  return getCoverArt(coverArtSource(txn));
}

QString MediaItem::coverArtSource(MdbxTxn* txn, bool failFast) const
{
  if (id < 1) {
    return QString();
  }
  if (failFast) {
    return metadata.value("_coverArt", QStringLiteral("%1/%2").arg(int(type)).arg(id));
  }
  QString source = metadata.value("_coverArt");
  if (source.isEmpty()) {
    if (!fullPath.isEmpty()) {
      auto images = MetadataProvider::readImages(fullPath);
      if (!images.isEmpty()) {
        // TODO: pick the most appropriate if multiple images
        return QStringLiteral("%1:%2").arg(id).arg(images.keys().first());
      }
    } else {
      // TODO: artist art
      QList<MediaItem> tracks = children(txn, true);
      for (const MediaItem& item : tracks) {
        source = item.coverArtSource(txn);
        if (!source.isEmpty()) {
          break;
        }
      }
    }
  }
  return source;
}

bool MediaItem::isMetadataDirty() const
{
  if (fullPath.isEmpty()) {
    // The metadata of something that doesn't correspond to a file can't be out of date.
    return false;
  }
  if (type == Track) {
    if (metadata.value("$duration").isEmpty() || metadata.value("$mimetype").isEmpty()) {
      return true;
    }
  }
  if (!lastUpdated.isValid()) {
    return true;
  }
  QFileInfo info(fullPath);
  return info.lastModified() > lastUpdated;
}

void MediaItem::loadMetadata(MdbxTxn* txn, const MdbxRecord& rec, bool deep)
{
  if (deep ? loadedDeep : loadedShallow) {
    return;
  }
  if (rec.isValid()) {
    if (type == AnyCollection) {
      type = MediaItem::ItemType(rec.value<int>("type"));
    }
    QVariantHash tags = rec.value<QVariantHash>("tags");
    for (const QString& key : tags.keys()) {
      metadata[key] = tags.value(key).value<QString>();
    }
    name = rec.value<QString>("name");
    for (const char* key : (const char*[]){ "mimetype", "duration" }) {
      QString value = rec.value<QString>(key);
      if (!value.isEmpty()) {
        metadata[QStringLiteral("$%1").arg(key)] = value;
      }
    }
    childOrder.clear();
    for (const QVariant& v : rec.value<QVariantList>("items")) {
      childOrder << v.toLongLong();
    }
  }
  if (deep && !loadedDeep) {
    MdbxTable tblPaths("paths", txn);
    if (type == Folder) {
      pathId = id;
    } else if (pathId < 1) {
      QList<qint64> pathIds = rec.links("paths");
      if (pathIds.size()) {
        pathId = pathIds.first();
      }
    }
    if (pathId > 0) {
      MdbxRecord recPath(tblPaths.get(pathId));
      if (recPath.isValid()) {
        fullPath = recPath.value<QString>("path");
        parentId = recPath.value<qint64>("parent");
        lastUpdated = recPath.value<QDateTime>("modified");
      }
    }
    bool isDirty = id < 1 || isMetadataDirty();
    if (isDirty) {
      if (type != Folder && metadata.value("$mimetype").isEmpty() && !fullPath.isEmpty()) {
        metadata["$mimetype"] = MetadataProvider::mimeType(fullPath);
      }
      if (type == Playlist) {
        // TODO: playlists not saved in the filesystem
        QFile f(fullPath);
        if (f.open(QIODevice::ReadOnly | QIODevice::Text)) {
          QDir m3uDir(fullPath);
          m3uDir.cdUp();
          TagsM3U m3u(&f);
          int numTracks = m3u.numTracks();
          childOrder.clear();
          for (int i = 0; i < numTracks; i++) {
            QString itemPath = m3uDir.absoluteFilePath(m3u.trackName(i));
            MdbxRecord recPath(tblPaths.get(itemPath));
            if (recPath.isValid()) {
              QList<qint64> trackIds = recPath.links("tracks");
              if (trackIds.size()) {
                childOrder << trackIds.first();
              }
            }
          }
        }
      }
      if (type == Track) {
        for (const auto& key : metadata.keys()) {
          if (!key.startsWith("$")) {
            metadata.remove(key);
          }
        }
        MetadataProvider::readTags(fullPath, metadata);
      }
      QString cover = coverArtSource(txn);
      if (!cover.isEmpty()) {
        metadata["_coverArt"] = cover;
      }
      // TODO: collection duration
      lastUpdated = QDateTime::currentDateTimeUtc();
    }
    loadedDeep = true;
  }
  if (type != Folder && name.isEmpty()) {
    name = metadata.value("TITLE");
  }
  if (name.isEmpty()) {
    name = QDir(fullPath).dirName();
    if (name.contains('.')) {
      if (name == "!playlist.m3u" || name == "!tags.m3u") {
        name = QFileInfo(fullPath).dir().dirName();
      } else {
        name = QFileInfo(fullPath).completeBaseName();
      }
    }
  }
  loadedShallow = true;
}

void MediaItem::loadMetadata(MdbxTxn* txn, bool deep)
{
  if (type == Invalid) {
    return;
  }
  ENSURE_TXN(Read);
  if (type == Folder) {
    MdbxTable tblPaths("paths", txn);
    loadMetadata(txn, tblPaths.get(id), deep);
  } else if (type == Track) {
    MdbxTable tblTracks("tracks", txn);
    loadMetadata(txn, tblTracks.get(id), deep);
  } else {
    MdbxTable tblCollections("collections", txn);
    loadMetadata(txn, tblCollections.get(id), deep);
  }
}

void MediaItem::saveMetadata(MdbxTxn* txn)
{
  if (type == Invalid) {
    return;
  }
  ENSURE_TXN(Write);
  if (pathId > 0) {
    MdbxTable tblPaths("paths", txn);
    MdbxRecord recPath(tblPaths.get(pathId));
    recPath.setValue("modified", lastUpdated);
    recPath.save();
  }
  if (type == Folder) {
    return;
  }
  QVariantHash toStoreMeta;
  MdbxTable tblTagTypes("tagtypes", txn);
  for (const auto& key : metadata.keys()) {
    if (!key.startsWith("$")) {
      toStoreMeta[key] = metadata[key];
      if (!key.startsWith("_")) {
        if (!tblTagTypes.get(key).isValid()) {
          tblTagTypes.create({ { "tag", key } }).save();
        }
      }
    }
  }
  std::unique_ptr<MdbxTable> tbl;
  MdbxRecord rec;
  if (type == Track) {
    tbl.reset(new MdbxTable("tracks", txn));
  } else {
    tbl.reset(new MdbxTable("collections", txn));
  }
  if (id > 0) {
    rec = tbl->get(id);
  } else {
    rec = tbl->create();
    if (pathId > 0) {
      rec.link("paths", pathId);
    }
    id = rec.id();
  }
  rec.setValue("mimetype", metadata.value("$mimetype"));
  rec.setValue("tags", toStoreMeta);
  if (metadata.contains("$duration")) {
    rec.setValue("duration", metadata.value("$duration"));
  }
  if (childOrder.size()) {
    rec.setValue("items", childOrder);
  }
  if (type != Track) {
    rec.setValue("type", int(type));
  } else {
    if (metadata.value("ALBUM").isEmpty()) {
      rec.setValue("no_album", 1);
    } else if (metadata.value("_albumId").isEmpty()) {
      rec.setValue("no_album", 2);
    } else {
      rec.remove("no_album");
    }
    if (metadata.value("ARTIST").isEmpty()) {
      rec.setValue("no_artist", 1);
    } else if (metadata.value("_artistId").isEmpty()) {
      rec.setValue("no_artist", 2);
    } else {
      rec.remove("no_artist");
    }
  }
  rec.save();
}

qint32 MediaItem::taggedId() const
{
  if (type == Track) {
    return id;
  } else if (type == Folder) {
    return 0x20000000 | (id & 0x1FFFFFFF);
  } else if (type != Invalid) {
    return 0x40000000 | (id & 0x1FFFFFFF);
  } else {
    return 0;
  }
}

MediaItem MediaItem::fromTaggedId(qint32 taggedId)
{
  qint64 tag = taggedId & 0x60000000;
  qint64 id = taggedId & 0x1FFFFFFF;
  if (tag == 0x20000000) {
    return MediaItem(Folder, id);
  } else if (tag == 0x40000000) {
    return MediaItem(AnyCollection, id);
  } else {
    return MediaItem(Track, id);
  }
}
