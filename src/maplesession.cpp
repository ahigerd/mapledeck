#include "maplesession.h"
#include "database/mdbxtable.h"
#ifdef MD_ENABLE_JUKEBOX
#include "jukebox/jukeboxplayer.h"
#endif

static QList<MapleSession*> MapleSession_allSessions;

QList<MapleSession*> MapleSession::allSessions()
{
  return MapleSession_allSessions;
}

QList<MapleSession*> MapleSession::allSessions(int userId)
{
  QList<MapleSession*> result;
  for (MapleSession* session : MapleSession_allSessions) {
    if (session->userId == userId) {
      result << session;
    }
  }
  return result;
}

QList<MapleSession*> MapleSession::targetSessions(int userId)
{
  QList<MapleSession*> result;
  for (MapleSession* session : MapleSession_allSessions) {
    // TODO: distinguish between target and non-target sessions
    if (session->userId == userId && (session->type == Web || session->type == MPD)) {
      result << session;
    }
  }
  return result;
}

MapleSession* MapleSession::findSession(MapleSession::SessionType type, int userId, const QString& sessionName)
{
  for (MapleSession* session : allSessions(userId)) {
    if (session->type == type && session->sessionName == sessionName) {
      return session;
    }
  }
  MapleSession* session = new MapleSession(type, userId, sessionName);
  if (session->isValid()) {
    return session;
  }
  delete session;
  return nullptr;
}

MapleSession* MapleSession::findSession(MapleSession::SessionType type, const QString& cookie)
{
  for (MapleSession* session : MapleSession_allSessions) {
    if (session->type == type && session->sessionCookie == cookie) {
      return session;
    }
  }

  MapleSession* session = nullptr;

  qint64 userId = 0;
  QString username;
  {
    MdbxTxn txn(mEnv->startRead());
    MdbxTable tblSessions("sessions", &txn);
    MdbxRecord rec(tblSessions.get(QStringLiteral("%1\t%2").arg(int(type)).arg(cookie)));
    qint64 timeout = rec.value<qint64>("timeout");
    if (!timeout || timeout < QDateTime::currentDateTimeUtc().toSecsSinceEpoch()) {
      // TODO: delete expired sessions
      return nullptr;
    }

    QList<qint64> userIds = rec.links("users");
    MdbxTable tblUsers("users", &txn);
    MdbxRecord userRec(tblUsers.get(userIds.value(0)));
    if (userRec.isValid()) {
      userId = userRec.id();
      username = userRec.value<QString>("username");
    }
  }

  if (userId) {
    session = findSession(type, userId, username);
  }

  if (session && session->sessionCookie.isEmpty()) {
    session->setCookie(cookie);
  }
  return session;
}

MapleSession::MapleSession(MapleSession::SessionType type, int _userId, const QString& _sessionName, QObject* parent)
: QObject(parent), userId(_userId), dbSessionId(0), isAdmin(false), type(type), sessionName(_sessionName), queuePos(-1),
  elapsed(0), duration(0), volume(1.0), state(Idle), repeatMode(false), singleMode(false)
{
  {
    MdbxTxn txn(mEnv->startRead());
    MdbxTable tblUsers("users", &txn);
    MdbxRecord user(tblUsers.get(_userId));

    if (user.isValid()) {
      username = user.value<QString>("username");
      isAdmin = user.value<bool>("isAdmin");
      MapleSession_allSessions << this;

      qint64 now = QDateTime::currentDateTimeUtc().toSecsSinceEpoch();
      MdbxTable tblSessions("sessions", &txn);
      for (qint64 sessionId : user.links("sessions")) {
        MdbxRecord recSession(tblSessions.get(sessionId));
        if (recSession.value<int>("type") == type &&
            recSession.value<qint64>("timeout") > now &&
            recSession.value<QString>("name") == sessionName) {
          dbSessionId = sessionId;
          sessionCookie = recSession.value<QString>("cookie");
          break;
        }
      }
    } else {
      userId = 0;
      sessionName.clear();
    }
  }

  if (dbSessionId) {
    updateTimeout();
  }

  isJukeboxOnly = false;
#ifdef MD_ENABLE_JUKEBOX
  if (userId > 0 && (sessionName.endsWith("+jukebox") || sessionName.endsWith("@jukebox"))) {
    isJukeboxOnly = true;
    JukeboxPlayer* jukebox = JukeboxPlayer::instance();
    if (!jukebox) {
      jukebox = new JukeboxPlayer(this);
    }
    if (jukebox->claim(this)) {
      qDebug() << "Claimed jukebox" << jukebox;
    } else {
      qDebug() << "Unable to claim jukebox";
      // TODO: signal failure
    }
  }
#endif
}

MapleSession::~MapleSession()
{
  MapleSession_allSessions.removeAll(this);
}

bool MapleSession::isValid() const
{
  return userId;
}

void MapleSession::setCookie(const QString& cookie)
{
  QString cookieValue = cookie;
  QStringList parts = cookie.split('\t');
  sessionCookie = parts.back();
  if (parts.size() < 2) {
    cookieValue = QStringLiteral("%1\t%2").arg(int(type)).arg(cookie);
  }
  MdbxTxn txn(mEnv->startWrite());
  MdbxTable tblSessions("sessions", &txn);
  MdbxRecord recSession;
  if (dbSessionId) {
    recSession = tblSessions.get(dbSessionId);
    recSession.setValue("cookie", cookieValue);
    recSession.setValue("timeout", QDateTime::currentDateTimeUtc().toSecsSinceEpoch() + 86400 * 7);
    if (userId) {
      recSession.link("users", userId);
    }
  } else {
    recSession = tblSessions.create({
      { "type", int(type) },
      { "name", sessionName },
      { "cookie", cookieValue },
      { "timeout", QDateTime::currentDateTimeUtc().toSecsSinceEpoch() + 86400 * 7 },
    });
    if (userId) {
      recSession.link("users", userId);
    }
    dbSessionId = recSession.id();
  }
  recSession.save();
  txn.commit();
  sessionCookie = cookie;
}

void MapleSession::updateTimeout()
{
  if (dbSessionId) {
    MdbxTxn txn(mEnv->startWrite());
    MdbxTable tblSessions("sessions", &txn);
    MdbxRecord recSession = tblSessions.get(dbSessionId);
    // A login session times out after a week of inactivity.
    recSession.setValue("timeout", QDateTime::currentDateTimeUtc().toSecsSinceEpoch() + 86400 * 7);
    recSession.save();
    txn.commit();
  }
}

void MapleSession::enqueue(int trackId)
{
  queue << trackId;
  if (queue.size() == 1) {
    setTrack(trackId);
  }
}

void MapleSession::removeQueueAt(int pos)
{
  if (pos >= 0 && pos < queue.size()) {
    queue.removeAt(pos);
  }
}

void MapleSession::clearQueue()
{
  queue.clear();
  queuePos = 0;
}

void MapleSession::setTrack(int trackId)
{
  if (trackId > 0) {
    nowPlaying = MediaItem(MediaItem::Track, trackId);
  } else {
    nowPlaying.id = 0;
    nowPlaying.type = MediaItem::Invalid;
  }
  elapsed = 0;
  if (nowPlaying.id > 0) {
    nowPlaying.loadMetadata();
    duration = nowPlaying.metadata.value("$duration").toDouble();
    if (state == Playing) {
      play();
    }
  } else {
    duration = 0;
    stop();
  }
}

void MapleSession::seekQueue(int pos)
{
  if (pos >= 0 && pos < queue.size()) {
    queuePos = pos;
    setTrack(queue[queuePos]);
  } else {
    setTrack(0);
  }
}

void MapleSession::playNext()
{
  if (repeatMode && singleMode) {
    seekQueue(queuePos);
  } else if (repeatMode || !singleMode) {
    while (queuePos + 1 < queue.size()) {
      if (state == Idle) {
        if (nowPlaying.id < 1) {
          seekQueue(0);
        }
      } else {
        seekQueue(queuePos + 1);
      }
      if (nowPlaying.id > 0) {
        play();
        return;
      }
    }
  }
  seekQueue(-1);
  stop();
}

void MapleSession::play()
{
  if (queuePos < 0 && queue.length() > 0 && queue[0] == nowPlaying.id) {
    queuePos = 0;
  }
  if (nowPlaying.id > 0) {
    if (state == Paused) {
      state = Playing;
      emit resumed();
    } else {
      state = Playing;
      elapsed = 0;
      emit started();
    }
  }
}

void MapleSession::pause()
{
  if (state == Playing) {
    state = Paused;
    emit paused();
  }
}

void MapleSession::togglePause()
{
  if (state == Playing) {
    pause();
  } else if (state == Paused) {
    play();
  }
}

void MapleSession::stop()
{
  state = Idle;
  elapsed = 0;
  emit stopped();
}

void MapleSession::setVolume(double v)
{
  volume = v;
  emit volumeChanged(v);
}
