#include "dirscanner.h"
#include "database/mdbxenv.h"
#include "database/mdbxtable.h"
#include <QDateTime>
#include <QDir>
#include <QFileInfo>
#include <QHash>

namespace {
  struct ChildPath {
    ChildPath() : pathId(0), modified(0) {}
    ChildPath(qint64 pathId, qint64 modified) : pathId(pathId), modified(modified) {}
    ChildPath(const ChildPath& other) = default;
    ChildPath(ChildPath&& other) = default;
    ChildPath& operator=(ChildPath&& other) = default;

    qint64 pathId;
    qint64 modified;
  };
}

DirScanner::DirScanner(qint64 pathId, bool forceRescan, bool countOnly)
: isValid(true), modifiedCount(0)
{
  MdbxTxn txn(mEnv->startWrite());
  MdbxTable tblPaths("paths", &txn);
  QHash<QString, ChildPath> childPaths;
  MdbxRecord rec(tblPaths.get(pathId));
  if (!rec.isValid()) {
    isValid = false;
    return;
  }
  path = rec.value<QString>("path");
  bool dirty = false;

  for (qint64 childId : tblPaths.search("parent", pathId)) {
    MdbxRecord rec(tblPaths.get(childId));
    if (rec.isValid()) {
      childPaths[rec.value<QString>("path")] = ChildPath(rec.id(), rec.value<qint64>("modified"));
    }
  }

  QDir root(path);
  for (const QFileInfo& entry : root.entryInfoList(QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot | QDir::Readable)) {
    QString entryPath = entry.absoluteFilePath();
    ChildPath child = childPaths.take(entryPath);
    bool isDir = entry.isDir();
    bool deep = !countOnly || isDir;
    if (deep && !child.pathId) {
      MdbxRecord row(tblPaths.get(entryPath));
      if (!row.isValid()) {
        row = tblPaths.create({
          { "path", entryPath },
          { "modified", 0 },
          { "parent", pathId },
        });
        dirty = true;
      }
      child.pathId = row.id();
    }

    if (entry.isDir()) {
      dirs << child.pathId;
    } else if (forceRescan || child.modified < entry.lastModified().toSecsSinceEpoch()) {
      ++modifiedCount;
      if (deep) {
        modifiedFiles << entryPath;
      }
    }
  }

  if (!countOnly && !childPaths.isEmpty()) {
    dirty = true;
    MdbxTable tblTracks("tracks", &txn);
    MdbxTable tblCollections("collections", &txn);
    for (const ChildPath& child : childPaths) {
      MdbxRecord row(tblPaths.get(child.pathId));
      for (qint64 trackId : row.links("tracks")) {
        tblTracks.get(trackId).erase();
      }
      for (qint64 collectionId : row.links("collections")) {
        tblTracks.get(collectionId).erase();
      }
      row.erase();
    }
  }

  if (dirty) {
    txn.commit();
  }
}
