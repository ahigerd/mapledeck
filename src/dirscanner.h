#ifndef MD_DIRSCANNER_H
#define MD_DIRSCANNER_H

#include <QList>
#include <QString>

class DirScanner
{
public:
  DirScanner(qint64 pathId, bool forceRescan = false, bool countOnly = false);

  QString path;
  bool isValid;
  int modifiedCount;
  QList<QString> modifiedFiles;
  QList<qint64> dirs;
};

#endif
