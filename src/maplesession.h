#ifndef MD_MAPLESESSION_H
#define MD_MAPLESESSION_H

#include <QObject>
#include <QList>
#include <QString>
#include "mediaitem.h"

class MapleSession : public QObject
{
Q_OBJECT
public:
  enum SessionType {
    Web,
    MPD,
    Subsonic,
    Any = 1024,
  };

  enum State {
    Idle,
    Playing,
    Paused,
  };

  static QList<MapleSession*> allSessions();
  static QList<MapleSession*> allSessions(int userId);
  static QList<MapleSession*> targetSessions(int userId);
  static MapleSession* findSession(SessionType type, int userId, const QString& sessionName);
  static MapleSession* findSession(SessionType type, const QString& cookie);

  MapleSession(SessionType type, int userId, const QString& sessionName, QObject* parent = nullptr);
  ~MapleSession();

  bool isValid() const;
  void setCookie(const QString& cookie);
  void updateTimeout();

  int userId;
  int dbSessionId;
  bool isAdmin;
  bool isJukeboxOnly;
  SessionType type;
  QString username;
  QString sessionName;
  QString sessionCookie;

  QList<qint64> queue;
  int queuePos;

  MediaItem nowPlaying;
  double elapsed, duration, volume;
  State state;
  bool repeatMode, singleMode;

signals:
  void started();
  void paused();
  void resumed();
  void stopped();
  void volumeChanged(double);

public slots:
  void enqueue(int trackId);
  void removeQueueAt(int pos);
  void clearQueue();
  void setTrack(int trackId);
  void seekQueue(int pos);
  void playNext();
  void play();
  void pause();
  void togglePause();
  void stop();
  void setVolume(double);
};

#endif
