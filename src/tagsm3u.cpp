#include "tagsm3u.h"
#include <QDir>
#include <QtDebug>
#include <QFileDevice>

static TagMap m3uKeys = {
  { "ALB", "ALBUM" },
  { "ART", "ARTIST" },
  { "GENRE", "GENRE" },
  { "-X-TARGETDURATION", "LENGTH" },
};

QString TagsM3U::relativeTo(const QString& trackPath)
{
  QDir dir(trackPath);
  dir.cdUp();
  return dir.absoluteFilePath("!tags.m3u");
}

TagsM3U::TagsM3U() : autoTrack(false)
{
  // initializers only
}

TagsM3U::TagsM3U(QIODevice* file) : autoTrack(false)
{
  if (qobject_cast<QFileDevice*>(file)) {
    m3uPath = static_cast<QFileDevice*>(file)->fileName();
  }
  if (!file->isOpen()) {
    file->open(QIODevice::ReadOnly | QIODevice::Text);
  }
  QByteArray data = file->readAll();
  for (const QByteArray& line : data.split('\n')) {
    parseLine(QString::fromUtf8(line));
  }
}

void TagsM3U::parseLine(const QString& _line)
{
  QString line = _line.trimmed();
  if (line.isEmpty()) {
    // Blank lines are ignored
    return;
  }
  // Lines that start with # are directives, everything else is a filename.
  if (line[0] != '#') {
    if (!fileTags.count("TITLE")) {
      fileTags["TITLE"] = line;
    }
    if (autoTrack && !fileTags.count("TRACK")) {
      fileTags["TRACK"] = QString::number(tracks.size() + 1);
    }
    tags[tracks.size()] = fileTags;
    tracks.push_back(line);
    // Copy the current global tag state into the tags for the next file.
    fileTags = globalTags;
    return;
  }
  line = line.mid(1).trimmed();
  QString tag, value;
  bool globalTag = line[0] == '@';
  if (line[0] == '$') {
    // vgmstream-style directives
    if (line == "$AUTOTRACK") {
      autoTrack = true;
    }
    if (line == "$AUTOALBUM" && !m3uPath.isEmpty()) {
      QDir dir(m3uPath);
      dir.cdUp();
      populateTag(true, "ALBUM", dir.dirName());
    }
  } else if (line[0] == '%' || line[0] == '@') {
    // vgmstream-style tags
    int end = line.indexOf(line[0], 1);
    if (end < 0) {
      // A tag can be terminated with another token
      // If it isn't, then it ends at the first whitespace
      end = line.indexOf(' ');
      if (end < 0) {
        // If there's no whitespace at all then it's invalid
        return;
      }
    }
    tag = line.mid(1, end - 1).trimmed();
    value = line.mid(end + 1).trimmed();
  } else if (line.mid(0, 3) == "EXT") {
    // Ext-M3U directive
    int end = line.indexOf(':');
    if (end < 0) {
      // not a data-bearing directive
      return;
    }
    tag = line.mid(3, end - 3).trimmed();
    value = line.mid(end + 1).trimmed();
    if (tag == "INF") {
      tag = "TITLE";
      int lengthEnd = -1;
      bool quoted = false;
      for (int i = 0; i < value.size(); i++) {
        if (value[i] == '"') {
          quoted = !quoted;
        } else if (!quoted && value[i] == ',') {
          lengthEnd = i;
          break;
        }
      }
      if (lengthEnd >= 0) {
        // No delimiter is a violation of spec, but the spec says the length must have a comma.
        // So if there's no delimiter, treat the whole thing as a title.
        QString length = value.mid(0, lengthEnd).trimmed();
        value = value.mid(lengthEnd + 1).trimmed();
        lengthEnd = length.indexOf(' ');
        if (lengthEnd >= 0) {
          QString attr = length.mid(lengthEnd + 1);
          length = length.mid(0, lengthEnd);
          int pos = 0;
          int start = 0;
          QString subtag, subvalue;
          bool quoted = false, needValue = false;
          while (pos < attr.size()) {
            if (quoted) {
              if (attr[pos] == '"') {
                // TODO: not documented if there are escape sequences
                subvalue = attr.mid(start, pos - start);
                populateTag(false, subtag, subvalue);
                quoted = false;
                needValue = false;
                start = pos + 1;
              }
            } else if (needValue) {
              if (attr[pos] == '"') {
                start = pos + 1;
                quoted = true;
              } else if (attr[pos] == ' ') {
                subvalue = attr.mid(start, pos - start);
                populateTag(false, subtag, subvalue);
                needValue = false;
                start = pos + 1;
              }
            } else if (attr[pos] == '=') {
              subtag = attr.mid(start, pos - start);
              needValue = true;
              start = pos + 1;
            }
            ++pos;
          }
          if (needValue) {
            // fell off the end, capture the last value
            subvalue = attr.mid(start, pos - start);
            populateTag(false, subtag, subvalue);
            needValue = false;
          }
        }
        if (!length.isEmpty() && length[0] != '-') {
          populateTag(false, "LENGTH", length);
        }
      }
      ssize_t dashPos = value.indexOf(" - ");
      if (dashPos >= 0) {
        QString artist = value.mid(0, dashPos);
        value = value.mid(dashPos + 3);
        populateTag(false, "ARTIST", artist);
      }
    } else if (tag == "X-VGMSTREAM") {
      for (const QString& attr : value.split(',')) {
        if (!attr.contains('=')) {
          continue;
        }
        QString subtag = attr.section('=', 0, 0);
        QString subvalue = attr.section('=', 1);
        populateTag(false, subtag, subvalue);
      }
    } else {
      auto iter = m3uKeys.find(tag);
      if (iter != m3uKeys.end()) {
        tag = *iter;
        // album, artist, and genre stick until overridden
        // everything else is an extension
        globalTag = tag[0] != '-';
      }
    }
  } else if (line.mid(0, 9) == "PLAYLIST:") {
    // IPTV spec allows giving the playlist a name of its own
    name = line.mid(9).trimmed();
    return;
  } else {
    // Not a recognized directive, probably a comment -- ignore it
    return;
  }
  populateTag(globalTag, tag, value);
}

void TagsM3U::populateTag(bool globalTag, QString tag, QString value)
{
  tag = tag.trimmed().toUpper();
  value = value.trimmed();
  if (tag.isEmpty()) {
    return;
  }
  fileTags[tag] = value;
  if (globalTag) {
    globalTags[tag] = value;
  }
}

void TagsM3U::set(int trackIndex, const QString& tag, const QString& value)
{
  if (trackIndex < 0 || trackIndex >= tracks.size()) {
    qWarning() << "Invalid track index";
    return;
  }
  tags[trackIndex][tag.trimmed().toUpper()] = value;
}

QString TagsM3U::get(int trackIndex, const QString& tag) const
{
  if (trackIndex < 0 || trackIndex >= tracks.size()) {
    return QString();
  }
  auto group = tags.value(trackIndex);
  auto value = group.find(tag.trimmed().toUpper());
  if (value == group.end()) {
    return QString();
  }
  return *value;
}

QString TagsM3U::trackName(int trackIndex) const
{
  if (trackIndex < 0 || trackIndex >= tracks.size()) {
    return QString();
  }
  return tracks.at(trackIndex);
}

int TagsM3U::numTracks() const
{
  return tracks.size();
}

void TagsM3U::addTrack(const QString& trackName)
{
  TagMap newMap;
  newMap["TITLE"] = trackName;
  tags[tracks.size()] = newMap;
  tracks.push_back(trackName);
}

int TagsM3U::findTrack(const QString& trackName) const
{
  QDir dir(trackName);
  QString filename = dir.dirName();
  for (int i = 0; i < tracks.size(); i++) {
    if (tracks.at(i) == filename) {
      return i;
    }
  }
  return -1;
}

TagMap TagsM3U::allTags(int trackIndex) const
{
  if (trackIndex < 0 || trackIndex > tracks.size()) {
    return invalid;
  }
  return tags.value(trackIndex);
}

QString TagsM3U::playlistName() const
{
  return name;
}
