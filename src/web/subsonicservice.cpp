#include "subsonicservice.h"
#include "../maplesession.h"
#include "../mapleapp.h"
#include "../mapledb.h"
#include "../streamtranscode.h"
#include "../metadata/metadataprovider.h"
#include "../database/mdbxtable.h"
#include "../jukebox/jukeboxplayer.h"
#include <QMimeDatabase>
#include <QSettings>
#include <QxtWebEvent>
#include <QJsonDocument>
#include <QDateTime>
#include <QRandomGenerator>
#include <algorithm>
#include <cstdlib>

static QHash<QString, QString> noTranscode{
  { "audio/mpeg", "mp3" },
  { "audio/ogg;codecs=vorbis", "ogg" },
  { "audio/ogg;codecs=opus", "opus" },
};

static QHash<QString, StreamTranscode::OutputFormat> transcodeFormat{
  { "audio/mpeg", StreamTranscode::MP3 },
  { "audio/ogg;codecs=vorbis", StreamTranscode::Vorbis },
  { "audio/ogg;codecs=opus", StreamTranscode::Opus },
};

static QPair<QString, StreamTranscode::OutputFormat> transcodeType(const QString& mimeType)
{
  if (!noTranscode.contains(mimeType)) {
    QSettings settings;
    QString target = settings.value("subsonic/transcode").toString();
    if (transcodeFormat.contains(target)) {
      return qMakePair(target, transcodeFormat[target]);
    } else {
      return qMakePair(QStringLiteral("audio/ogg;codecs=opus"), StreamTranscode::Opus);
    }
  }
  return qMakePair(mimeType, StreamTranscode::Copy);
}

static const QHash<QString, QPair<QString, Qt::SortOrder>> subSortKeys{
  { "", { "TITLE", Qt::AscendingOrder } },
  { "alphabeticalByName", { "TITLE", Qt::AscendingOrder } },
  { "alphabeticalByArtist", { "ARTIST", Qt::AscendingOrder } },
  { "newest", { "created", Qt::DescendingOrder } },
  { "genre", { "GENRE", Qt::AscendingOrder } },
};

static QPair<QString, Qt::SortOrder> subSortKey(const QString& sortBy)
{
  return subSortKeys.value(sortBy, qMakePair(sortBy, Qt::AscendingOrder));
}

static QVariantHash getSongData(const MediaItem& item, int albumId = 0, int autoTrack = 0)
{
  QString mimeType = item.metadata.value("$mimetype");
  QVariantHash song{
    { "id", QString::number(item.taggedId()) },
    { "parent", QString::number(item.parentId) },
    { "isDir", false },
    { "title", item.metadata.value("TITLE", item.name) },
    { "album", item.metadata.value("ALBUM") },
    { "artist", item.metadata.value("ARTIST") },
    { "albumartist", item.metadata.value("ALBUMARTIST") },
    { "created", QDateTime::fromSecsSinceEpoch(item.metadata.value("$lastUpdated").toInt()).toString(Qt::ISODate) },
    { "discNumber", item.metadata.value("DISC") },
    { "contentType", mimeType },
    { "suffix", item.fullPath.contains('.') ? item.fullPath.section('.', -1) : item.fullPath.section('/', -1) },
    { "duration", QString::number(item.metadata.value("$duration").toInt()) },
    { "type", "music" },
    { "year", item.metadata.value("YEAR") },
    { "genre", item.metadata.value("GENRE") },
    { "path", QStringLiteral("stream/%1").arg(item.id) },
    { "playCount", 1 },
  };
  QString cover = item.coverArtSource();
  if (!cover.isEmpty()) {
    song["coverArt"] = cover;
  }
  if (song["year"].toString().isEmpty()) {
    song["year"] = item.metadata.value("DATE").left(4);
  }
  int track = item.metadata.value("TRACK").toInt();
  if (track) {
    song["track"] = track;
  }
  auto format = transcodeType(mimeType);
  if (format.second != StreamTranscode::Copy) {
    song["transcodeContentType"] = format.first;
    song["transcodeSuffix"] = noTranscode[format.first];
  }
  if (albumId > 0) {
    song["albumId"] = QString::number(albumId);
    if (autoTrack && !track) {
      song["track"] = autoTrack;
    }
  } else {
    MediaItem album = item.album();
    if (album.id > 0) {
      song["albumId"] = QString::number(album.taggedId());
    }
  }
  MediaItem artist = item.artist();
  if (artist.id > 0) {
    song["artistId"] = QString::number(artist.taggedId());
  }
  for (const QString& key : song.keys()) {
    if (song[key].isNull() || song[key].toString().isEmpty()) {
      song.remove(key);
    }
  }
  return song;
}

SubsonicService::SubsonicService(QxtAbstractWebSessionManager* parent)
: QxtWebSlotService(parent, parent)
{
  // initializers only
}

void SubsonicService::ping(QxtWebRequestEvent* event)
{
  postResponse(event, QUrlQuery(event->url), QVariantHash());
}

void SubsonicService::getLicense(QxtWebRequestEvent* event)
{
  QUrlQuery query(event->url);
  QVariantHash response{
    { "license", QVariantHash{
      { "valid", "true" },
      { "email", "valid@example.com" },
      { "licenseExpires", "2099-12-31T23:59:59.999999Z" },
    } },
  };
  postResponse(event, query, response);
}

void SubsonicService::getMusicFolders(QxtWebRequestEvent* event)
{
  QUrlQuery query(event->url);
  if (!authenticate(event, query)) {
    return;
  }
  QList<MediaItem> roots = mDB->rootFolders();
  QVariantList folders;
  for (const MediaItem& item : roots) {
    folders << QVariantHash{{ "id", item.taggedId() }, { "name", item.name }};
  }
  QVariantHash response{
    { "musicFolders", QVariantHash{
      { "musicFolder", folders },
    } },
  };
  postResponse(event, query, response);
}

void SubsonicService::getIndexes(QxtWebRequestEvent* event)
{
  QUrlQuery query(event->url);
  int folderId = query.queryItemValue("musicFolderId").toLong();
  bool isOldVersion = !folderId || (query.queryItemValue("v").section('.', 0, 1).toDouble() < 1.7);
  QVariantList children, indexes;

  QList<MediaItem> items;
  if (!folderId) {
    items = mDB->rootFolders();
  } else {
    items = MediaItem(MediaItem::Folder, folderId).children(nullptr, true);
  }
  for (const MediaItem& item : items) {
    if (item.type == MediaItem::Folder) {
      QVariantHash child{
        { "id", QString::number(item.taggedId()) },
        { isOldVersion ? "name" : "title", item.name },
        { "parent", QString::number(item.parentId) },
      };
      if (item.parentId) {
        child["parent"] = QString::number(item.parentId);
      }
      (isOldVersion ? indexes : children) << child;
    } else if (item.type == MediaItem::Track) {
      children << getSongData(item, 0, 0);
    }
  }

  QVariantHash response{{ "indexes", QVariantHash{
    { "lastModified", 0 },
    { "ignoredArticles", "" },
    { "child", children },
    { "index", QVariantList{QVariantHash{
      { "name", "ALL" },
      { "artist", indexes },
    }}},
  }}};
  postResponse(event, query, response);
}

void SubsonicService::getMusicDirectory(QxtWebRequestEvent* event)
{
  getCollection(event, MediaItem::Folder);
}

void SubsonicService::getPlaylists(QxtWebRequestEvent* event)
{
  QVariantList playlists;
  for (const MediaItem& playlist : MediaItem::getAll(MediaItem::Playlist, true)) {
    int songCount = playlist.metadata.value("$songCount").toInt();
    if (!songCount) {
      continue;
    }
    playlists << QVariantHash{
      { "id",  playlist.taggedId() },
      { "name", playlist.name },
      { "songCount", songCount },
      { "duration", playlist.metadata.value("$duration").toDouble() },
      { "created", playlist.created.toString(Qt::ISODate) },
      { "comment", playlist.fullPath.section('/', -2, -2) },
      /*
      { "owner", query.queryItemValue("u") },
      { "public", "true" },
      { "coverArt", "" },
      */
    };
  }
  postResponse(event, QUrlQuery(event->url), QVariantHash{{ "playlists", QVariantHash{{ "playlist", playlists }}}});
}

QVariantList getAlbumListCommon(const QUrlQuery& query, bool oldVersion = false)
{
  QVariantList result;
  QList<MediaItem> albums = MediaItem::getAll(MediaItem::Album, true);
  auto sortBy = subSortKey(query.queryItemValue("type"));
  MediaItem::sort(albums, sortBy.first, sortBy.second);

  for (const MediaItem& album : albums) {
    QVariantHash hash{
      { "id", QString::number(album.taggedId()) },
      { oldVersion ? "title" : "name", album.metadata.value("TITLE", album.name) },
      { "artist", album.metadata["ARTIST"] },
      { "songCount", album.metadata["$songCount"].toInt() },
      { "duration", oldVersion ? album.metadata["$duration"].toInt() : album.metadata["$duration"].toDouble() },
      { "created", album.created.toString(Qt::ISODate) },
    };
    MediaItem artist = album.artist();
    if (artist.type == MediaItem::Artist) {
      hash["artistId"] = artist.taggedId();
    }
    QString coverArt = album.coverArtSource();
    if (!coverArt.isEmpty()) {
      hash["coverArt"] = coverArt;
    }
    result << hash;
  }

  int start = query.queryItemValue("offset").toInt();
  int end = query.queryItemValue("size").toInt();
  if (!end) {
    end = -1;
  }
  return result.mid(start, end);
}

void SubsonicService::getAlbumList2(QxtWebRequestEvent* event)
{
  QUrlQuery query(event->url);
  if (!authenticate(event, query)) {
    return;
  }
  postResponse(event, QUrlQuery(event->url), QVariantHash{{ "albumList2", QVariantHash{{ "album", getAlbumListCommon(query) }}}});
}

void SubsonicService::getAlbumList(QxtWebRequestEvent* event)
{
  QUrlQuery query(event->url);
  if (!authenticate(event, query)) {
    return;
  }
  postResponse(event, QUrlQuery(event->url), QVariantHash{{ "albumList", QVariantHash{{ "album", getAlbumListCommon(query, true) }}}});
}

void SubsonicService::getArtists(QxtWebRequestEvent* event)
{
  QUrlQuery query(event->url);
  if (!authenticate(event, query)) {
    return;
  }
  QList<MediaItem> artists = MediaItem::getAll(MediaItem::Artist);
  MediaItem::sort(artists, "TITLE");
  QVariantList response;

  for (const MediaItem& artist : artists) {
    response << QVariantHash{
      { "id", artist.taggedId() },
      { "name", artist.name },
    };
  }

  postResponse(event, QUrlQuery(event->url), QVariantHash{{ "artists", QVariantHash{
    { "ignoredArticles", "" },
    { "index", QVariantList{ QVariantHash{
      { "name", "ALL" },
      { "artist", response },
    }}},
  }}});
}

void SubsonicService::getArtistInfo(QxtWebRequestEvent* event)
{
  postResponse(event, QUrlQuery(event->url), QVariantHash{{ "artistInfo", QVariantHash() }});
}

void SubsonicService::getArtistInfo2(QxtWebRequestEvent* event)
{
  postResponse(event, QUrlQuery(event->url), QVariantHash{{ "artistInfo2", QVariantHash() }});
}

void SubsonicService::getCollection(QxtWebRequestEvent* event, MediaItem::ItemType type)
{
  QUrlQuery query(event->url);
  if (!authenticate(event, query)) {
    return;
  }
  qint64 id = query.queryItemValue("id").toLong();
  if (!id) {
    postError(event, 70, "Invalid collection ID");
    return;
  }
  MediaItem collection = MediaItem::fromTaggedId(id);
  collection.loadMetadata();
  if (collection.id < 1 || collection.type != type) {
    postError(event, 70, "Invalid collection ID");
    return;
  }
  QVariantList songs, albums, folders;
  QVariantHash response{
    { "id", QString::number(id) },
    { "name", collection.name },
    { "created", collection.created.toString(Qt::ISODate) },
  };
  QString artist = collection.metadata.value("ARTIST");
  if (!artist.isEmpty()) {
    response["artist"] = artist;
  }

  QList<MediaItem> items = collection.children(nullptr, true);
  if (items.isEmpty()) {
    postError(event, 70, "Collection not found");
    return;
  }
  int autoTrack = 0;
  double duration = 0;
  for (const MediaItem& item : items) {
    if (type == MediaItem::Folder && item.type == MediaItem::Folder) {
      folders << QVariantHash{
        { "id", QString::number(item.taggedId()) },
        { "title", item.name },
        { "isDir", true },
        { "parent", QString::number(item.parentId) },
      };
    } else if (item.type == MediaItem::Track) {
      QVariantHash song = getSongData(item, type == MediaItem::Album ? id : 0, autoTrack + 1);
      autoTrack = song["track"].toInt();
      duration += song["duration"].toDouble();
      songs << song;
    } else if (item.type == MediaItem::Album) {
      QVariantHash album{
        { "id", QString::number(item.taggedId()) },
        { "name", item.name },
        { "artist", item.metadata.value("ARTIST") },
        { "songCount", item.metadata["$songCount"].toInt() },
        { "duration", item.metadata["$duration"].toInt() },
      };
      MediaItem artist = item.artist();
      if (artist.type == MediaItem::Artist) {
        album["artistId"] = artist.taggedId();
      }
      albums << album;
    }
  }

  response["duration"] = int(duration);
  if (songs.size()) {
    response["songCount"] = songs.size();
  }
  if (albums.size()) {
    response["albumCount"] = albums.size();
  }
  if (type == MediaItem::Album) {
    response["song"] = songs;
    postResponse(event, query, QVariantHash{{ "album", response }});
  } else if (type == MediaItem::Artist) {
    response["album"] = albums;
    response["song"] = songs;
    postResponse(event, query, QVariantHash{{ "artist", response }});
  } else if (type == MediaItem::Playlist) {
    response["comment"] = collection.fullPath.section('/', -2, -2);
    response["entry"] = songs;
    postResponse(event, query, QVariantHash{{ "playlist", response }});
  } else if (type == MediaItem::Folder) {
    if (collection.parentId) {
      response["parent"] = QString::number(collection.parentId);
    }
    response["child"] = folders + albums + songs;
    postResponse(event, query, QVariantHash{{ "directory", response }});
  }
}

void SubsonicService::getPlaylist(QxtWebRequestEvent* event)
{
  getCollection(event, MediaItem::Playlist);
}

void SubsonicService::getAlbum(QxtWebRequestEvent* event)
{
  getCollection(event, MediaItem::Album);
}

void SubsonicService::getArtist(QxtWebRequestEvent* event)
{
  getCollection(event, MediaItem::Artist);
}

void SubsonicService::getRandomSongs(QxtWebRequestEvent* event)
{
  QUrlQuery query(event->url);
  if (!authenticate(event, query)) {
    return;
  }

  int size = query.hasQueryItem("size") ? query.queryItemValue("size").toInt() : 10;
  QList<MapleDB::Filter> filters;

  if (query.hasQueryItem("genre")) {
    filters << MapleDB::Filter("GENRE", query.queryItemValue("genre"));
  }

  if (query.hasQueryItem("fromYear") || query.hasQueryItem("toYear")) {
    QString min = query.queryItemValue("fromYear");
    QString max = query.queryItemValue("toYear");
    if (max.isEmpty()) {
      max = "9999";
    }
    filters << MapleDB::Filter("YEAR", min, max);
  }

  /*
  if (query.hasQueryItem("musicFolderId")) {
    joinClause += "INNER JOIN recursive_paths rp ON t.path_id=rp.child_id";
    whereClauses << "(t.path_id=? OR rp.parent_id=?)";
    bound << query.queryItemValue("musicFolderId").toInt();
    bound << query.queryItemValue("musicFolderId").toInt();
  }
  */

  MdbxTxn txn(mEnv->startRead());
  MdbxTable tblTracks("tracks", &txn);
  QList<MediaItem> items;
  if (filters.isEmpty()) {
    QList<qint64> ids;
    qint64 maxId = tblTracks.maxId();
    int remaining = size;
    while (remaining > 0) {
      qint64 id = 0;
      for (int retry = 0; retry < 3; retry++) {
        id = std::rand() % maxId;
        if (ids.contains(id)) {
          id = 0;
        } else {
          break;
        }
      }
      if (!id) {
        --remaining;
        continue;
      }
      MdbxRecord recTrack(tblTracks.get(id));
      if (recTrack.isValid()) {
        ids << id;
        --remaining;
      }
    }
    items = MediaItem::getMultiple(MediaItem::Track, ids, true, &txn);
  } else {
    items = mDB->searchTracks(filters);
  }
  MediaItem::sort(items, "random");
  QVariantList songs;
  for (const MediaItem& item : items) {
    songs << getSongData(item, 0, 0);
  }

  postResponse(event, query, QVariantHash{{ "randomSongs", QVariantHash{{ "song", songs }}}});
}

void SubsonicService::getSong(QxtWebRequestEvent* event)
{
  QUrlQuery query(event->url);
  if (!authenticate(event, query)) {
    return;
  }
  qint64 songId = query.queryItemValue("id").toLong();
  if (!songId) {
    postError(event, 70, "Invalid song ID");
    return;
  }
  MediaItem item(MediaItem::Track, songId);
  if (item.type != MediaItem::Track || item.id < 1) {
    postError(event, 70, "Song not found");
    return;
  }
  QVariantHash song = getSongData(item);
  postResponse(event, QUrlQuery(event->url), QVariantHash{{ "song", song }});
}

void SubsonicService::getGenres(QxtWebRequestEvent* event)
{
  postResponse(event, QUrlQuery(event->url), QVariantHash{{ "genres", QVariantList() }});
}

void SubsonicService::getCoverArt(QxtWebRequestEvent* event)
{
  QUrlQuery query(event->url);
  QString coverId = query.queryItemValue("id", QUrl::FullyDecoded);
  bool isTrack = false;
  if (!coverId.contains(":")) {
    // TODO: cover IDs for other types
    QList<QString> parts = coverId.split("/");
    MediaItem::ItemType type = MediaItem::Track;
    qint64 id = parts.back().toLongLong();
    if (parts.size() > 1) {
      type = MediaItem::ItemType(parts[0].toInt());
    }
    MediaItem item(type, id);
    item.loadMetadata(nullptr, true);
    coverId = item.coverArtSource();
    isTrack = item.type == MediaItem::Track;
  }
  QByteArray image = MediaItem::getCoverArt(coverId);
  if (!image.isEmpty()) {
    QxtWebPageEvent* response = new QxtWebPageEvent(event->sessionID, event->requestID, image);
    response->contentType = QMimeDatabase().mimeTypeForData(image).name().toUtf8();
    postEvent(response);
    return;
  }
  if (!query.hasQueryItem("u")) {
    postEvent(new QxtWebRedirectEvent(event->sessionID, event->requestID, isTrack ? "/static/tape.png" : "/static/folder.png"));
    return;
  }
  QFile* f = new QFile(":/static/tape.png");
  f->open(QIODevice::ReadOnly);
  QxtWebPageEvent* r = new QxtWebPageEvent(event->sessionID, event->requestID, f);
  r->contentType = "image/png";
  postEvent(r);
}

void SubsonicService::getScanStatus(QxtWebRequestEvent* event)
{
  double progress = mDB->currentScanProgress();
  postResponse(event, QUrlQuery(event->url), QVariantHash{{ "scanStatus", QVariantHash{{ "scanning", progress >= 0 }}}});
}

void SubsonicService::download(QxtWebRequestEvent* event)
{
  stream(event);
}

void SubsonicService::stream(QxtWebRequestEvent* event)
{
  QUrlQuery query(event->url);
  if (!authenticate(event, query)) {
    return;
  }
  qint64 songId = query.queryItemValue("id").toLong();
  MediaItem item(MediaItem::Track, songId);
  if (item.type != MediaItem::Track) {
    postError(event, 70, "Track not found");
    return;
  }

  StreamTranscode* tc = StreamTranscode::transcode(item, transcodeType(item.metadata.value("$mimetype")).second);

  int sessionID = event->sessionID, requestID = event->requestID;
  QObject::connect(tc, &StreamTranscode::transcodeStarted, [this, sessionID, requestID, tc]{
    QxtWebPageEvent* response = new QxtWebPageEvent(sessionID, requestID, tc->output());
    response->headers.replace("connection", "close");
    response->headers.replace("cache-control", "private,max-age=3600,immutable");
    response->contentType = tc->mimeType().toUtf8();
    postEvent(response);
  });

  QObject::connect(tc, &StreamTranscode::transcodeFailed, [this, sessionID, requestID, tc]{
    tc->deleteLater();
    QxtWebPageEvent* response = new QxtWebErrorEvent(sessionID, requestID, 404, "Transcoding Error");
    postEvent(response);
  });
}

void SubsonicService::jukeboxControl(QxtWebRequestEvent* event)
{
  QUrlQuery query(event->url);
  int userId = authenticate(event, query);
  if (!userId) {
    return;
  }
  MapleSession* session = MapleSession::findSession(MapleSession::Subsonic, userId, QString("%1+jukebox").arg(userId));
  if (!session) {
    session = new MapleSession(MapleSession::Subsonic, userId, QString("%1+jukebox").arg(userId));
  }
  JukeboxPlayer* jukebox = JukeboxPlayer::instance();
  if (!jukebox || !jukebox->claim(session)) {
    postError(event, 50, "unable to claim jukebox");
    return;
  }
  QString action = query.queryItemValue("action");
  QVariantList playlist;
  if (action == "get") {
    QList<MediaItem> songs = MediaItem::getMultiple(MediaItem::Track, session->queue, true);
    for (const auto& item : songs) {
      playlist << getSongData(item, 0, 0);
    }
  } else if (action == "set") {
    session->clearQueue();
    qint64 songId = query.queryItemValue("id").toLong();
    if (session->state == MapleSession::Playing) {
      session->queue << songId;
    } else {
      session->enqueue(songId);
    }
  } else if (action == "start") {
    jukebox->play();
  } else if (action == "stop") {
    jukebox->pause();
  } else if (action == "skip") {
    int index = query.queryItemValue("index").toInt();
    if (index != session->queuePos) {
      session->seekQueue(index);
    }
    if (query.hasQueryItem("offset")) {
      jukebox->seek(query.queryItemValue("offset").toDouble());
    }
  } else if (action == "add") {
    session->enqueue(query.queryItemValue("id").toLong());
  } else if (action == "clear") {
    session->clearQueue();
  } else if (action == "remove") {
    session->removeQueueAt(query.queryItemValue("index").toInt());
  } else if (action == "shuffle") {
    int numSongs = session->queue.size();
    for (int i = numSongs - 1; i >= 1; --i) {
      int other = std::rand() % i;
      if (other == i) continue;
      qint64 t = session->queue[i];
      session->queue[i] = session->queue[other];
      session->queue[other] = t;
    }
  } else if (action == "setGain") {
    jukebox->setVolume(query.queryItemValue("gain").toDouble());
  } else if (action != "status") {
    postError(event, 0, "unknown jukebox command " + action);
    return;
  }
  QVariantHash response{{
    { "currentIndex", session->queuePos },
    { "playing", session->state == MapleSession::Playing },
    { "gain", jukebox->volume() },
  }};
  if (session->state != MapleSession::Idle) {
    response["position"] = int(jukebox->elapsed());
  }
  if (action == "get") {
    response["entry"] = playlist;
    response = QVariantHash{{ "jukeboxPlaylist", response }};
  } else {
    response = QVariantHash{{ "jukeboxStatus", response }};
  }
  postResponse(event, QUrlQuery(event->url), response);
}

int SubsonicService::authenticate(QxtWebRequestEvent* event, const QUrlQuery& query)
{
  if (event->sessionID && event->cookies.contains("mapleS")) {
    MapleSession* session = MapleSession::findSession(MapleSession::Web, event->cookies.values("mapleS").first());
    if (session && session->isValid()) {
      return session->userId;
    }
  }
  QString username = QString(query.queryItemValue("u"));
  QString password = query.queryItemValue("p");
  if (password.startsWith("enc:")) {
    password = QString::fromUtf8(QByteArray::fromHex(password.mid(4).toUtf8()));
  }
  int userId = MapleApp::authenticateUser(username, password);
  if (userId < 1) {
    postError(event, 40, "Unable to log in with this username/password");
    return 0;
  }
  return userId;
}

static inline QString escapeXml(const QVariant& value)
{
  return value.toString().replace('&', "&amp;").replace('"', "&quot;").replace('<', "&lt;").replace("'", "&apos;");
}

static QString toXml(const QString& root, const QVariantHash& hash)
{
  QString xml;
  QString attr;
  for (const QString& key : hash.keys()) {
    QVariant value = hash.value(key);
    if (value.canConvert<QString>()) {
      attr += QStringLiteral(" %1=\"%2\"").arg(key).arg(escapeXml(value));
    } else if (value.canConvert<QVariantList>()) {
      for (const QVariant& v : value.value<QVariantList>()) {
        if (v.canConvert<QString>()) {
          xml += QStringLiteral("<%1>%2</%1>\n").arg(key).arg(escapeXml(v));
        } else {
          xml += toXml(key, v.toHash());
        }
      }
    } else if (value.canConvert<QVariantHash>()) {
      xml += toXml(key, value.toHash());
    } else {
      qWarning("Unable to convert %s to XML", value.typeName());
    }
  }
  if (root.isEmpty()) {
    return xml;
  }
  return QStringLiteral("<%1%2>%3</%1>\n").arg(root).arg(attr).arg(xml);
}


void SubsonicService::postResponse(QxtWebRequestEvent* event, const QUrlQuery& query, QVariantHash response)
{
  QString format = query.queryItemValue("f");
  bool isXml = format.isEmpty() || format == "xml";
  if (!response.contains("status")) {
    response["status"] = "ok";
  }
  if (!response.contains("version")) {
    response["version"] = "1.16.1";
  }
  QByteArray responseData;
  if (isXml) {
    response["xmlns"] = "http://subsonic.org/restapi";
    responseData = (QStringLiteral("<?xml version='1.0' encoding='UTF-8'?>\n") + toXml("subsonic-response", response)).toUtf8();
  } else {
    responseData = QJsonDocument::fromVariant(response).toJson(format == "jsonp" ? QJsonDocument::Indented : QJsonDocument::Compact);
    if (format == "jsonp") {
      responseData = query.queryItemValue("callback").toUtf8() + "({\"subsonic-response\":" + responseData + "})";
    } else {
      responseData = "{\"subsonic-response\":" + responseData + "}";
    }
  }
  qDebug() << responseData;
  QxtWebPageEvent* r = new QxtWebPageEvent(event->sessionID, event->requestID, responseData);
  if (isXml) {
    r->contentType = "text/xml;charset=utf-8";
  } else if (format == "json") {
    r->contentType = "application/json;charset=utf-8";
  } else if (format == "jsonp") {
    r->contentType = "application/javascript;charset=utf-8";
  }
  postEvent(r);
}

void SubsonicService::postError(QxtWebRequestEvent* event, int code, const QString& message)
{
  QVariantHash response{
    { "status", "failed" },
    { "error", QVariantHash{
      { "code", code },
      { "message", message },
    } },
  };
  postResponse(event, QUrlQuery(event->url), response);
}
