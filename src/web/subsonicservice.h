#ifndef MD_SUBSONICSERVICE_H
#define MD_SUBSONICSERVICE_H

#include <QxtWebSlotService>
#include <QUrlQuery>
#include <QVariant>
#include "../mediaitem.h"

class SubsonicService : public QxtWebSlotService
{
Q_OBJECT
public:
  SubsonicService(QxtAbstractWebSessionManager* parent);

public slots:
  void ping(QxtWebRequestEvent* event);
  void getLicense(QxtWebRequestEvent* event);
  void getMusicFolders(QxtWebRequestEvent* event);
  void getIndexes(QxtWebRequestEvent* event);
  void getMusicDirectory(QxtWebRequestEvent* event);
  void getPlaylists(QxtWebRequestEvent* event);
  void getPlaylist(QxtWebRequestEvent* event);
  void getAlbumList(QxtWebRequestEvent* event);
  void getAlbumList2(QxtWebRequestEvent* event);
  void getAlbum(QxtWebRequestEvent* event);
  void getRandomSongs(QxtWebRequestEvent* event);
  void getSong(QxtWebRequestEvent* event);
  void getArtists(QxtWebRequestEvent* event);
  void getArtist(QxtWebRequestEvent* event);
  void getArtistInfo(QxtWebRequestEvent* event);
  void getArtistInfo2(QxtWebRequestEvent* event);
  void getGenres(QxtWebRequestEvent* event);
  void getCoverArt(QxtWebRequestEvent* event);
  void getScanStatus(QxtWebRequestEvent* event);
  void download(QxtWebRequestEvent* event);
  void stream(QxtWebRequestEvent* event);
  void jukeboxControl(QxtWebRequestEvent* event);

private:
  void getCollection(QxtWebRequestEvent* event, MediaItem::ItemType type);
  int authenticate(QxtWebRequestEvent* event, const QUrlQuery& query);
  void postResponse(QxtWebRequestEvent* event, const QUrlQuery& query, QVariantHash response);
  void postError(QxtWebRequestEvent* event, int code, const QString& message);
};

#endif
