#include "statusservice.h"
#include "../mapleapp.h"
#include "../mapledb.h"
#include <QxtWebEvent>
#include <QThread>
#include <QUrlQuery>

StatusService::StatusService(QxtAbstractWebSessionManager* parent)
: QxtAbstractWebService(parent, parent), lastProgress(-1)
{
  MapleDB* db = mApp->database();
  QObject::connect(db, SIGNAL(scanStarted()), this, SLOT(scanStarted()));
  QObject::connect(db, SIGNAL(scanProgress(float)), this, SLOT(scanProgress(float)));
  QObject::connect(db, SIGNAL(scanFinished(bool)), this, SLOT(scanFinished(bool)));
}

void StatusService::pageRequestedEvent(QxtWebRequestEvent* event)
{
  QUrlQuery query(event->url);

  if (query.hasQueryItem("stream")) {
    JsonSeq::Format format = JsonSeq::Format_Ld;
    auto accept = event->headers.values("Accept");
    if (!accept.isEmpty() && accept[0].contains("application/json-seq")) {
      format = JsonSeq::Format_Seq;
    }

    JsonSeq* buffer = new JsonSeq(format, this);
    buffer->open(QIODevice::ReadWrite);
    streams << buffer;

    QxtWebPageEvent* response = new QxtWebPageEvent(event->sessionID, event->requestID, buffer);
    response->chunked = false;
    response->streaming = true;
    if (format == JsonSeq::Format_Seq) {
      response->contentType = "application/json-seq";
    } else {
      response->contentType = "text/plain";
    }
    postEvent(response);

    buffer->push(statusJson(mApp->database()->currentScanProgress()));
  } else {
    QxtWebPageEvent* response = new QxtWebPageEvent(event->sessionID, event->requestID, statusJson(mApp->database()->currentScanProgress()));
    response->contentType = "application/json";
    postEvent(response);
  }
}

void StatusService::broadcast(const QByteArray& json)
{
  for (JsonSeq* buffer : streams) {
    buffer->push(json);
    QMetaObject::invokeMethod(buffer, "readyRead");
  }
}

void StatusService::scanStarted()
{
  broadcast(statusJson(0));
  lastProgress = 0;
}

void StatusService::scanProgress(float progress)
{
  if (progress != lastProgress) {
    lastProgress = progress;
    broadcast(statusJson(progress));
  }
}

void StatusService::scanFinished(bool updated)
{
  broadcast(statusJson(-1));
  if (updated) {
    broadcast("{ \"event\": \"database\" }");
  }
}

QByteArray StatusService::statusJson(float progress) const
{
  if (progress < 0) {
    return "{ \"status\": \"idle\" }";
  }
  return QString("{ \"status\": \"scanning\", \"progress\": %1 }").arg(progress).toUtf8();
}
