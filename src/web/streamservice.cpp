#include "streamservice.h"
#include "httpserver.h"
#include "../maplesession.h"
#include "../streamtranscode.h"
#include "../mediaitem.h"
#include <QxtHtmlTemplate>
#include <QxtWebEvent>
#include <QUrlQuery>
#include <QFileInfo>
#include <algorithm>

StreamService::StreamService(QxtAbstractWebSessionManager* parent)
: QxtAbstractWebService(parent, parent)
{
  // initializers only
}

void StreamService::pageRequestedEvent(QxtWebRequestEvent* event)
{
  int id = event->url.path().section('/', -1).toInt();
  if (!id) {
    postEvent(new QxtWebRedirectEvent(event->sessionID, event->requestID, "/"));
    return;
  }

  MediaItem item(MediaItem::Track, id);
  item.loadMetadata(nullptr, true);
  if (item.fullPath.isEmpty() || !QFileInfo(item.fullPath).isReadable()) {
    postEvent(new QxtWebErrorEvent(event->sessionID, event->requestID, 404, "Not Found"));
    return;
  }

  QUrlQuery query(event->url);
  if (query.hasQueryItem("js")) {
    QxtWebPageEvent* response = new QxtWebPageEvent(event->sessionID, event->requestID, renderPlayer(item).toUtf8());
    if (query.hasQueryItem("queue")) {
      bool ok = false;
      int queuePos = query.queryItemValue("queue").toInt(&ok);
      if (ok) {
        MapleSession* s = MapleSession::findSession(MapleSession::Web, event->cookies.values("mapleS").first());
        if (s && (queuePos == -1 || (s->queue.length() > queuePos && s->queue[queuePos] == id))) {
          s->seekQueue(queuePos);
        }
      }
    }
    response->contentType = "text/html;charset=utf-8";
    postEvent(response);
    return;
  } else if (!query.hasQueryItem("format")) {
    postEvent(new QxtWebRedirectEvent(event->sessionID, event->requestID, QStringLiteral("/browse/track/%1?nowPlaying=%1").arg(id)));
    return;
  }

  QString format = query.queryItemValue("format");
  if (format == "native") {
    QString mimeType = item.metadata.value("$mimetype").simplified().replace(" ", "");
    QFile* file = new QFile(item.fullPath);
    if (!file->open(QIODevice::ReadOnly)) {
      postEvent(new QxtWebErrorEvent(event->sessionID, event->requestID, 404, "Not Found"));
      return;
    }
    int seek = -1;
    if (event->headers.contains("range")) {
      QString range = event->headers.values("range").first();
      if (range.startsWith("bytes=")) {
        bool ok = false;
        seek = range.section('=', 1).section('-', 0, 0).toInt(&ok);
        if (ok) {
          ok = file->seek(seek);
        }
        if (!ok) {
          seek = -1;
        }
      }
    }
    QxtWebPageEvent* response = new QxtWebPageEvent(event->sessionID, event->requestID, file);
    response->contentType = mimeType.toUtf8();
    response->streaming = false;
    response->chunked = true;
    response->headers.replace("accept-ranges", "bytes");
    if (seek >= 0) {
      response->headers.replace("content-range", QString("bytes %1-%2/%3").arg(seek).arg(file->size()).arg(file->size()));
      response->headers.replace("content-length", QString::number(file->size() - seek));
    }
    postEvent(response);
    return;
  }

  StreamTranscode::OutputFormat outputFormat;
  if (format == "opus") {
    outputFormat = StreamTranscode::Opus;
  } else if (format == "vorbis") {
    outputFormat = StreamTranscode::Vorbis;
  } else if (format == "mp3") {
    outputFormat = StreamTranscode::MP3;
  } else {
    postEvent(new QxtWebErrorEvent(event->sessionID, event->requestID, 404, "Not Found"));
    return;
  }

  StreamTranscode* tc = StreamTranscode::transcode(item, outputFormat);

  int sessionID = event->sessionID, requestID = event->requestID;
  QObject::connect(tc, &StreamTranscode::transcodeStarted, [this, sessionID, requestID, format, tc]{
    QxtWebPageEvent* response = new QxtWebPageEvent(sessionID, requestID, tc->output());
    response->headers.replace("connection", "close");
    response->headers.replace("cache-control", "private,max-age=3600,immutable");
    response->contentType = tc->mimeType().toUtf8();
    postEvent(response);
  });

  QObject::connect(tc, &StreamTranscode::transcodeFailed, [this, sessionID, requestID, tc]{
    tc->deleteLater();
    QxtWebPageEvent* response = new QxtWebErrorEvent(sessionID, requestID, 404, "Transcoding Error");
    postEvent(response);
  });
}

QString StreamService::renderPlayer(MediaItem& item)
{
  QxtHtmlTemplate tpl;
  tpl.open(HttpServer::resourcePath("player.html"));

  if (item.id > 0) {
    item.loadMetadata();
    tpl["title"] = item.metadata.value("TITLE", item.name).toHtmlEscaped();
    tpl["artist"] = item.metadata.value("ARTIST").toHtmlEscaped();
    if (tpl["artist"].isEmpty() && !item.metadata.value("ALBUMARTIST").isEmpty()) {
      tpl["artist"] = item.metadata.value("ALBUMARTIST").toHtmlEscaped();
    }
    tpl["album"] = item.metadata.value("ALBUM").toHtmlEscaped();
    tpl["cover"] = item.coverArtSource().toHtmlEscaped();

    double duration = item.metadata.value("$duration").toDouble();
    QString mimeType = item.metadata.value("$mimetype").simplified().replace(" ", "").toHtmlEscaped();
    tpl["mime"] = item.metadata.value("$mimetype").toHtmlEscaped();
    if (tpl["mime"].startsWith("audio/x-wav") || !tpl["mime"].startsWith("audio/")) {
      tpl.remove("mime");
    }
    tpl["id"] = QString::number(item.id);
    tpl["durationDS"] = QString::number(int(duration * 10));
  } else {
    tpl["title"] = "";
    tpl["durationDS"] = "0";
  }

  return tpl.render();
}
