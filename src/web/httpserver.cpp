#include "httpserver.h"
#include "browseservice.h"
#include "statusservice.h"
#include "streamservice.h"
#include "staticservice.h"
#include "loginservice.h"
#include "mpdservice.h"
#include "subsonicservice.h"
#include "eventservice.h"
#include "coverservice.h"
#include "queueservice.h"
#include <QxtWebEvent>
#include <QxtHttpServerConnector>
#include <QxtWebServiceDirectory>
#include <QFile>
#include <QDir>
#include <QFileInfo>
#include <QCoreApplication>
#include <QSettings>

QString HttpServer::resourcePath(const QString& path, bool isStatic)
{
  if (path.contains("..")) {
    return QString();
  }
#ifndef NDEBUG
  QDir dir(QCoreApplication::applicationDirPath());
  if (dir.cd("resource")) {
    QString fullPath = dir.absoluteFilePath(path);
    if (QFileInfo(fullPath).isReadable()) {
      return fullPath;
    }
  }
#endif
  if (isStatic) {
    return ":/static/" + path;
  }
  return ":/" + path;
}

class RootService : public QxtWebServiceDirectory
{
public:
  RootService(QxtAbstractWebSessionManager* sm)
  : QxtWebServiceDirectory(sm, sm)
  {
    // initializers only
  }

  void pageRequestedEvent(QxtWebRequestEvent* event) {
    qDebug() << "[REQ]" << event->url;

    static QSet<QString> noLogin{ "login", "mpd", "rest", "static", "covers" };
    QString target = event->url.path().section("/", 1, 1);
    if (target == "favicon.ico") {
      statics->pageRequestedEvent(event);
    } else if (noLogin.contains(target) || login->validateSession(event)) {
      // validateSession() will redirect on its own if it fails
      if (target == "") {
        index->pageRequestedEvent(event);
      } else {
        QxtWebServiceDirectory::pageRequestedEvent(event);
      }
    }
  }

  QxtAbstractWebService* index;
  LoginService* login;
  StaticService* statics;
};

HttpServer::HttpServer(QObject* parent)
: QxtHttpSessionManager(parent)
{
  setAutoCreateSession(false);
  setSessionCookieName("mapleS");

  connector = new QxtHttpServerConnector(this);
  setConnector(connector);

  RootService* mounts = new RootService(this);
  mounts->addService("browse", browse = new BrowseService(this));
  mounts->addService("status", status = new StatusService(this));
  mounts->addService("stream", stream = new StreamService(this));
  mounts->addService("static", statics = new StaticService(this));
  mounts->addService("mpd", mpd = new MpdService(this));
  mounts->addService("rest", sub = new SubsonicService(this));
  mounts->addService("login", login = new LoginService(this));
  mounts->addService("events", events = new EventService(this));
  mounts->addService("covers", covers = new CoverService(this));
  mounts->addService("queue", queue = new QueueService(this));
  mounts->setDefaultRedirect("browse");
  mounts->index = browse;
  mounts->login = login;
  mounts->statics = statics;

  setStaticContentService(mounts);

  QSettings settings;
  setPort(settings.value("http/port", 42069).toInt());
}
