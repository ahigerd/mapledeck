#include "staticservice.h"
#include "httpserver.h"
#include <QxtWebEvent>
#include <QUrlQuery>
#include <QMimeDatabase>
#include <QFile>

StaticService::StaticService(QxtAbstractWebSessionManager* parent)
: QxtAbstractWebService(parent, parent)
{
  // initializers only
}

void StaticService::pageRequestedEvent(QxtWebRequestEvent* event)
{
  QString path = event->url.path().mid(1);
  if (path == "favicon.ico") {
    path = "tape.png";
  }
  QFile* file = new QFile(HttpServer::resourcePath(path, true));
  if (!file->open(QIODevice::ReadOnly)) {
    delete file;
    postEvent(new QxtWebErrorEvent(event->sessionID, event->requestID, 404, "Not Found"));
    return;
  }
  QString contentType = QMimeDatabase().mimeTypeForFileNameAndData(path, file).name();
  file->reset();
  QxtWebPageEvent* page = new QxtWebPageEvent(event->sessionID, event->requestID, file);
  page->contentType = contentType.toUtf8();
  page->streaming = false;
  if (file->fileName().startsWith(":")) {
    page->headers.replace("cache-control", "private,max-age=3600");
  }
  postEvent(page);
}
