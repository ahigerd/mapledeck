#ifndef MD_MPDSERVICE_H
#define MD_MPDSERVICE_H

#include <QxtAbstractWebService>
#include <QExplicitlySharedDataPointer>
#include <QIODevice>
#include <QPointer>
#include <QTimer>
#include "../mediaitem.h"
#include "../streamtranscode.h"
class MapleSession;
class MpdSessionData;

class MpdService : public QxtAbstractWebService
{
Q_OBJECT
public:
  MpdService(QxtAbstractWebSessionManager* parent);

  void pageRequestedEvent(QxtWebRequestEvent* event);
};

class MpdStream : public QIODevice
{
Q_OBJECT
public:
  MpdStream(QExplicitlySharedDataPointer<MpdSessionData> session, StreamTranscode::OutputFormat format, QObject* parent = nullptr);
  ~MpdStream();

  QString contentType() const;
  QIODevice* output() const;

  bool atEnd() const;
  qint64 bytesAvailable() const;
  void close();

signals:
  void startTrack(int id);

protected:
  qint64 readData(char* data, qint64 maxSize);
  qint64 writeData(const char* data, qint64 maxSize);

private slots:
  void sourceDecoded();
  void startNextTrack();

  void playerPlay();
  void playerPause();
  void playerResume();
  void playerStop();

private:
  QExplicitlySharedDataPointer<MpdSessionData> session;
  QPointer<MapleSession> baseSession;
  QPointer<StreamTranscode> tcIn, tcOut;
  QByteArray buffer1, buffer2;
  QTimer throttle;
  int readPos;
  bool writeBuffer2, paused, seeking;
  int seekBytes;
};

#endif
