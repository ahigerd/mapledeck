#include "loginservice.h"
#include "httpserver.h"
#include "../maplesession.h"
#include "../mapleapp.h"
#include <QxtWebContent>
#include <QxtWebEvent>
#include <QxtHttpSessionManager>
#include <QxtHtmlTemplate>
#include <QDateTime>
#include <QUrlQuery>

LoginService::LoginService(QxtAbstractWebSessionManager* parent)
: QxtAbstractWebService(parent, parent)
{
  // Initializers only
}

void LoginService::formReadyRead(int sessionID, int requestID, QxtWebContent* content)
{
  if (content->bytesNeeded() != 0) {
    // still reading, let it buffer
    return;
  }
  QHash<QString, QString> form = QxtWebContent::parseUrlEncodedQuery(QString::fromUtf8(content->readAll()));
  QString username = form.value("username");
  QString password = form.value("password");
  int userId = MapleApp::authenticateUser(username, password);
  if (userId < 1) {
    postEvent(new QxtWebRedirectEvent(sessionID, requestID, "/login?error=true"));
    return;
  }

  auto sm = static_cast<QxtHttpSessionManager*>(sessionManager());
  int newSessionId = sm->newSession();
  QString sessionKey = sm->sessionKey(newSessionId);
  QString sessionName = "Web-" + sessionKey.mid(1, 8);
  // TODO: customize session name
  MapleSession* session = new MapleSession(MapleSession::Web, userId, sessionName);
  session->setCookie(sessionKey);
  sessions[newSessionId] = session;
  postEvent(new QxtWebRedirectEvent(newSessionId, requestID, "/"));
  return;
}

void LoginService::pageRequestedEvent(QxtWebRequestEvent* event)
{
  if (event->method == "POST") {
    int sessionID = event->sessionID;
    int requestID = event->requestID;
    QxtWebContent* content = event->content;
    QObject::connect(event->content, &QIODevice::readyRead, [this, sessionID, requestID, content]{ formReadyRead(sessionID, requestID, content); });
    if (content->bytesAvailable()) {
      formReadyRead(sessionID, requestID, content);
    }
    return;

  }
  QxtHtmlTemplate tpl, form;
  tpl.open(HttpServer::resourcePath("template.html"));
  form.open(HttpServer::resourcePath("login.html"));

  if (QUrlQuery(event->url).hasQueryItem("error")) {
    form["error"] = "true";
  }

  tpl["page"] = "login";
  tpl["content"] = form.render();
  QxtWebPageEvent* response = new QxtWebPageEvent(event->sessionID, event->requestID, tpl.render().toUtf8());
  response->contentType = "text/html;charset=utf-8";
  postEvent(response);
}

bool LoginService::validateSession(QxtWebRequestEvent* event)
{
  MapleSession* session = sessions.value(event->sessionID, nullptr);
  if (!session) {
    if (event->cookies.contains("mapleS")) {
      // The user has a session cookie, but QxtWeb doesn't know about it.
      // Check the database to see if it's an old session.
      session = MapleSession::findSession(MapleSession::Web, event->cookies.values("mapleS").first());
      if (session) {
        auto sm = static_cast<QxtHttpSessionManager*>(sessionManager());
        int newSessionId = sm->newSession();
        QString newSessionKey = sm->sessionKey(newSessionId);
        session->setCookie(newSessionKey);
        sessions[newSessionId] = session;

        // Redirect back to the same URL to refresh the session token.
        QString url = event->url.path(QUrl::FullyEncoded);
        if (event->url.hasQuery()) {
          url += "?" + event->url.query(QUrl::FullyEncoded);
        }
        postEvent(new QxtWebRedirectEvent(newSessionId, event->requestID, url));
        return false;
      }
    }
    // The user has no session cookie or the session cookie is bogus. Redirect to login to make a new session.
    if (event->cookies.contains("mapleS")) {
      postEvent(new QxtWebRemoveCookieEvent(event->sessionID, "mapleS"));
    }
    postEvent(new QxtWebRedirectEvent(event->sessionID, event->requestID, "/login"));
    return false;
  }
  session->updateTimeout();
  return true;
}
