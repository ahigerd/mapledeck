#ifndef MD_STATICSERVICE_H
#define MD_STATICSERVICE_H

#include <QxtAbstractWebService>

class StaticService : public QxtAbstractWebService
{
Q_OBJECT
public:
  StaticService(QxtAbstractWebSessionManager* parent);

  void pageRequestedEvent(QxtWebRequestEvent* event);
};

#endif

