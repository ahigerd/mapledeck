#ifndef MD_STATUSSERVICE_H
#define MD_STATUSSERVICE_H

#include <QxtAbstractWebService>
#include <QxtPointerList>
#include "../jsonseq.h"

class StatusService : public QxtAbstractWebService
{
Q_OBJECT
public:
  StatusService(QxtAbstractWebSessionManager* parent);

  void pageRequestedEvent(QxtWebRequestEvent* event);

private slots:
  void scanStarted();
  void scanProgress(float progress);
  void scanFinished(bool updated);

private:
  void broadcast(const QByteArray& json);
  QByteArray statusJson(float progress) const;

  QxtPointerList<JsonSeq> streams;
  float lastProgress;
};

#endif
