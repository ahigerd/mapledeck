#include "mpdservice.h"
#include "../maplesession.h"
#include "../mpd/mpdserver.h"
#include "../mpd/mpdsession.h"
#include <QxtWebEvent>
#include <QUrlQuery>
#include <QFileInfo>
#include <QtDebug>
#include <QProcess>
#include <cstring>

MpdService::MpdService(QxtAbstractWebSessionManager* parent)
: QxtAbstractWebService(parent, parent)
{
  // initializers only
}

void MpdService::pageRequestedEvent(QxtWebRequestEvent* event)
{
  QString sessionName = event->url.path().section('/', -1);
  QString format;
  if (sessionName.contains('.')) {
    format = sessionName.section('.', -1);
    sessionName = sessionName.section('.', 0, -2);
  }
  // The MPD mode uses the session name as the cookie as well
  MapleSession* baseSession = MapleSession::findSession(MapleSession::MPD, sessionName);
  QExplicitlySharedDataPointer<MpdSessionData> session = MpdSessionData::groupedSessions.value(baseSession);
  if (!session) {
    postEvent(new QxtWebErrorEvent(event->sessionID, event->requestID, 404, "Not Found"));
    return;
  }

  StreamTranscode::OutputFormat outputFormat = StreamTranscode::MP3;
  if (format == "ogg") {
    outputFormat = StreamTranscode::Vorbis;
  } else if (format == "opus") {
    outputFormat = StreamTranscode::Opus;
  }

  MpdStream* stream = new MpdStream(session, outputFormat, this);
  QxtWebPageEvent* response = new QxtWebPageEvent(event->sessionID, event->requestID, stream->output());
  response->contentType = stream->contentType().toUtf8();
  response->streaming = true;
  response->chunked = false;
  response->headers.replace("connection", "close");
  response->headers.replace("cache-control", "no-store,max-age=0");
  postEvent(response);
}

MpdStream::MpdStream(QExplicitlySharedDataPointer<MpdSessionData> session, StreamTranscode::OutputFormat format, QObject* parent)
: QIODevice(parent), session(session), baseSession(session->baseSession), tcIn(nullptr), readPos(0), writeBuffer2(false)
{
  QObject::connect(&throttle, SIGNAL(timeout()), this, SLOT(sourceDecoded()));
  throttle.setSingleShot(false);
  throttle.setInterval(25);

  QObject::connect(baseSession.data(), SIGNAL(started()), this, SLOT(playerPlay()));
  QObject::connect(baseSession.data(), SIGNAL(paused()), this, SLOT(playerPause()));
  QObject::connect(baseSession.data(), SIGNAL(resumed()), this, SLOT(playerResume()));
  QObject::connect(baseSession.data(), SIGNAL(stopped()), this, SLOT(playerStop()));
  paused = baseSession->state != MapleSession::Playing;

  setOpenMode(QIODevice::ReadOnly);
  tcOut = new StreamTranscode(this, format, this);
  QObject::connect(tcOut, SIGNAL(destroyed()), this, SLOT(deleteLater()));
  QObject::connect(tcOut->output(), SIGNAL(destroyed()), this, SLOT(deleteLater()));

  if (baseSession->state == MapleSession::Playing && baseSession->elapsed > 0) {
    seekBytes = baseSession->elapsed * 44100 * 2 * 2;
  } else {
    seekBytes = 0;
  }
  throttle.start();
}

MpdStream::~MpdStream()
{
}

QString MpdStream::contentType() const
{
  if (tcOut) {
    return tcOut->mimeType();
  }
  return QString();
}

QIODevice* MpdStream::output() const
{
  if (!tcOut) {
    return nullptr;
  }
  return tcOut->output();
}

bool MpdStream::atEnd() const
{
  return !baseSession;
}

qint64 MpdStream::bytesAvailable() const
{
  return (writeBuffer2 ? buffer1.size() : buffer2.size()) - readPos;
}

void MpdStream::close()
{
  if (tcIn) {
    tcIn->deleteLater();
    tcIn = nullptr;
  }
  if (tcOut) {
    tcOut->deleteLater();
    tcOut = nullptr;
  }
  setOpenMode(QIODevice::NotOpen);
}

void MpdStream::startNextTrack()
{
  throttle.stop();
  if (baseSession->state == MapleSession::Playing) {
    baseSession->playNext();
  }
}

void MpdStream::sourceDecoded()
{
  QByteArray& target = writeBuffer2 ? buffer2 : buffer1;
  if (paused) {
    // Trickle out a few samples of silence just to keep the connection from timing out
    target = QByteArray(256, '\0');
    writeBuffer2 = !writeBuffer2;
    emit readyRead();
    return;
  }
  if (!tcIn || !tcIn->output()) {
    startNextTrack();
    return;
  }
  QIODevice* src = tcIn->output();
  qint64 bytes = src->bytesAvailable();
  if (!bytes) {
    return;
  }
  if (seekBytes > 0) {
    if (bytes > seekBytes) {
      bytes = seekBytes;
    }
  }
  if (bytes > 4410) {
    bytes = 4410;
  }
  if (!target.isEmpty()) {
    return;
  }
  if (seekBytes > 0) {
    src->read(bytes);
    seekBytes -= bytes;
  } else {
    target = src->read(bytes);
    baseSession->elapsed += target.size() / 176400.0;
    writeBuffer2 = !writeBuffer2;
    emit readyRead();
  }
  if (src->atEnd()) {
    if (!qobject_cast<QProcess*>(src) || static_cast<QProcess*>(src)->state() == QProcess::NotRunning) {
      startNextTrack();
    }
  }
}

qint64 MpdStream::readData(char* data, qint64 maxSize)
{
  QByteArray& source = writeBuffer2 ? buffer1 : buffer2;
  if (source.isEmpty()) {
    return 0;
  }
  int toWrite = source.size() - readPos;
  if (toWrite > maxSize) {
    toWrite = maxSize;
  }
  std::memcpy(data, source.data() + readPos, toWrite);
  if (source.size() <= toWrite) {
    source.clear();
    readPos = 0;
    writeBuffer2 = !writeBuffer2;
  } else {
    readPos += toWrite;
  }
  return toWrite;
}

qint64 MpdStream::writeData(const char*, qint64)
{
  return 0;
}

void MpdStream::playerPlay()
{
  if (!baseSession) {
    return;
  }
  // Flush the buffers
  buffer1.clear();
  buffer2.clear();
  tcOut->output()->readAll();
  writeBuffer2 = false;
  readPos = 0;
  seekBytes = 0;
  paused = false;
  if (tcIn) {
    tcIn->deleteLater();
  }
  tcIn = StreamTranscode::transcode(baseSession->nowPlaying, StreamTranscode::Wave, this);
  QObject::connect(tcIn, SIGNAL(transcodeFailed()), tcIn, SLOT(deleteLater()));
  //QObject::connect(tcIn, SIGNAL(destroyed()), this, SLOT(startNextTrack()));
  QObject::connect(tcIn, SIGNAL(transcodeStarted()), &throttle, SLOT(start()));
  //QObject::connect(tcIn->output(), SIGNAL(destroyed()), this, SLOT(sourceClosed()));
  //QObject::connect(tcIn->output(), SIGNAL(readyRead()), this, SLOT(sourceDecoded()));
}

void MpdStream::playerPause()
{
  paused = true;
}

void MpdStream::playerResume()
{
  paused = false;
}

void MpdStream::playerStop()
{
  paused = true;
  buffer1.clear();
  buffer2.clear();
  tcOut->output()->readAll();
  tcIn->deleteLater();
  seekBytes = 0;
}
