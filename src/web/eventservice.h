#ifndef MD_EVENTSERVICE_H
#define MD_EVENTSERVICE_H

#include <QxtAbstractWebService>
class QWebSocketServer;

class EventService : public QxtAbstractWebService
{
Q_OBJECT
public:
  EventService(QxtAbstractWebSessionManager* parent);

  void pageRequestedEvent(QxtWebRequestEvent* event);
  void websocketEvent(QxtWebSocketEvent* event);

private:
  QWebSocketServer* wss;
};

#endif
