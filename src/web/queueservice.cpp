#include "queueservice.h"
#include "httpserver.h"
#include "../maplesession.h"
#include "../mediaitem.h"
#include <QxtWebEvent>
#include <QJsonDocument>
#include <QxtHtmlTemplate>

static QList<MediaItem> queueItems(MapleSession* session)
{
  QList<MediaItem> result;
  QHash<int, const MediaItem*> itemsById;
  QList<MediaItem> queuedItems = MediaItem::getMultiple(MediaItem::Track, session->queue, true);
  for (const MediaItem& item : queuedItems) {
    itemsById[item.id] = &item;
  }
  for (int id : session->queue) {
    const MediaItem* item = itemsById[id];
    if (item) {
      result << *item;
    }
  }
  return result;
}

QueueService::QueueService(QxtAbstractWebSessionManager* parent)
: QxtWebSlotService(parent, parent)
{
  // initializers only
}

QString QueueService::render(MapleSession* session)
{
  QxtHtmlTemplate body, line;
  body.open(HttpServer::resourcePath("queue.html"));
  line.open(HttpServer::resourcePath("queueline.html"));
  QString html;
  int i = 0;
  for (const MediaItem& item : queueItems(session)) {
    line["current"] = (session->queuePos == i) ? "1" : "";
    line["id"] = QString::number(item.id);
    line["pos"] = QString::number(i);
    line["name"] = item.metadata.value("TITLE", item.name).toHtmlEscaped();
    html += line.render();
    ++i;
  }
  body["items"] = html;
  return body.render();
}

void QueueService::html(QxtWebRequestEvent* event)
{
  postEvent(new QxtWebPageEvent(event->sessionID, event->requestID, render(session(event)).toUtf8()));
}

void QueueService::get(QxtWebRequestEvent* event)
{
  QVariantList tracks;
  MapleSession* s = session(event);
  int i = 0;
  for (const MediaItem& item : queueItems(s)) {
    QVariantHash h({
      { "id", item.id },
      { "name", item.name },
    });
    if (s->queuePos == i) {
      h["playing"] = true;
    }
    tracks << h;
    ++i;
  }
  postResponse(event, {{ "ok", true }, { "tracks", tracks }});
}

void QueueService::add(QxtWebRequestEvent* event)
{
  QUrlQuery query(event->url);
  MapleSession* s = session(event);
  int id = query.queryItemValue("id").toInt();
  MediaItem::ItemType type = query.hasQueryItem("type") ? MediaItem::ItemType(query.queryItemValue("type").toInt()) : MediaItem::Track;
  MediaItem item(type, id);
  item.loadMetadata();
  if (item.type == MediaItem::Invalid) {
    postResponse(event, {{ "ok", false }});
    return;
  } else if (item.type == MediaItem::Track) {
    s->enqueue(item.id);
  } else {
    for (const MediaItem& child : item.children(nullptr, false)) {
      s->enqueue(child.id);
    }
  }
  get(event);
}

void QueueService::clear(QxtWebRequestEvent* event)
{
  MapleSession* s = session(event);
  s->clearQueue();
  get(event);
}

void QueueService::seek(QxtWebRequestEvent* event)
{
  QUrlQuery query(event->url);
  MapleSession* s = session(event);
  bool ok = false;
  int pos = query.queryItemValue("pos").toInt(&ok);
  if (ok) {
    s->seekQueue(pos);
  }
  postResponse(event, {{ "ok", ok }});
}

void QueueService::remove(QxtWebRequestEvent* event)
{
  QUrlQuery query(event->url);
  MapleSession* s = session(event);
  bool ok = false;
  int pos = query.queryItemValue("pos").toInt(&ok);
  if (ok) {
    s->removeQueueAt(pos);
  }
  get(event);
}

void QueueService::postResponse(QxtWebRequestEvent* event, const QVariantHash& response)
{
  QByteArray responseData = QJsonDocument::fromVariant(response).toJson(QJsonDocument::Compact);
  QxtWebPageEvent* r = new QxtWebPageEvent(event->sessionID, event->requestID, responseData);
  r->contentType = "application/json;charset=utf-8";
  postEvent(r);
}

MapleSession* QueueService::session(QxtWebRequestEvent* event) const
{
  return MapleSession::findSession(MapleSession::Web, event->cookies.values("mapleS").first());
}
