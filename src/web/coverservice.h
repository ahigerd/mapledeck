#ifndef MD_COVERSERVICE_H
#define MD_COVERSERVICE_H

#include <QxtAbstractWebService>

class CoverService : public QxtAbstractWebService
{
Q_OBJECT
public:
  CoverService(QxtAbstractWebSessionManager* parent);

  void pageRequestedEvent(QxtWebRequestEvent* event);
};

#endif
