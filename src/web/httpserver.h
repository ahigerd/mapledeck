#ifndef MD_HTTPSERVER_H
#define MD_HTTPSERVER_H

#include <QxtHttpSessionManager>
class QxtHttpServerConnector;
class QxtWebServiceDirectory;
class BrowseService;
class StatusService;
class StreamService;
class StaticService;
class LoginService;
class MpdService;
class SubsonicService;
class EventService;
class CoverService;
class QueueService;

class HttpServer : public QxtHttpSessionManager
{
Q_OBJECT
public:
  static QString resourcePath(const QString& path, bool isStatic = false);

  HttpServer(QObject* parent);

private:
  QxtHttpServerConnector* connector;
  BrowseService* browse;
  StatusService* status;
  StreamService* stream;
  StaticService* statics;
  LoginService* login;
  MpdService* mpd;
  SubsonicService* sub;
  EventService* events;
  CoverService* covers;
  QueueService* queue;
};

#endif
