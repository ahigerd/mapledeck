#include "eventservice.h"
#include <QxtWebEvent>
#include <QxtWebContent>
#include <QWebSocketServer>
#include <QWebSocket>
#include <QtDebug>

EventService::EventService(QxtAbstractWebSessionManager* parent)
: QxtAbstractWebService(parent, parent)
{
  wss = new QWebSocketServer(QString(), QWebSocketServer::NonSecureMode, this);
}

void EventService::pageRequestedEvent(QxtWebRequestEvent* event)
{
  qDebug() << "EventService::pageRequested";
  postEvent(new QxtWebErrorEvent(event->sessionID, event->requestID, 404, "Not Found"));
}

void EventService::websocketEvent(QxtWebSocketEvent* event)
{
  qDebug() << "EventService::websocket";
}
