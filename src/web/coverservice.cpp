#include "coverservice.h"
#include "../mediaitem.h"
#include <QxtWebEvent>
#include <QMimeDatabase>
#include <QtDebug>

CoverService::CoverService(QxtAbstractWebSessionManager* parent)
: QxtAbstractWebService(parent, parent)
{
  // initializers only
}

void CoverService::pageRequestedEvent(QxtWebRequestEvent* event)
{
  int trackId = 0;
  for (const QString& part : event->url.path().split("/")) {
    trackId = part.toInt();
    if (trackId) {
      break;
    }
  }
  if (!trackId) {
    postEvent(new QxtWebErrorEvent(event->sessionID, event->requestID, 404, "Not Found"));
    return;
  }
  MediaItem item(MediaItem::Track, trackId);
  QByteArray image = item.coverArt();
  if (image.isEmpty()) {
    postEvent(new QxtWebErrorEvent(event->sessionID, event->requestID, 404, "Not Found"));
    return;
  }
  QString mimeType = QMimeDatabase().mimeTypeForData(image).name().toUtf8();
  QxtWebPageEvent* page = new QxtWebPageEvent(event->sessionID, event->requestID, image);
  page->contentType = mimeType.toUtf8();
  page->streaming = false;
  postEvent(page);
}
