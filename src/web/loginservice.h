#ifndef MD_LOGINSERVICE_H
#define MD_LOGINSERVICE_H

#include <QxtAbstractWebService>
#include <QPointer>
#include <QHash>
class QxtWebContent;
class MapleSession;

class LoginService : public QxtAbstractWebService
{
Q_OBJECT
public:
  LoginService(QxtAbstractWebSessionManager* parent);

  void pageRequestedEvent(QxtWebRequestEvent* event);

  bool validateSession(QxtWebRequestEvent* event);

private:
  void formReadyRead(int sessionID, int requestID, QxtWebContent* content);

  QHash<int, QPointer<MapleSession>> sessions;
};

#endif

