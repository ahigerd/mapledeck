#include "browseservice.h"
#include "streamservice.h"
#include "queueservice.h"
#include "httpserver.h"
#include "../mapledb.h"
#include "../maplesession.h"
#include "../metadata/metadataprovider.h"
#include <QxtHtmlTemplate>
#include <QxtWebEvent>
#include <QMimeDatabase>
#include <QUrlQuery>
#include <algorithm>

BrowseService::BrowseService(QxtAbstractWebSessionManager* parent)
: QxtAbstractWebService(parent, parent)
{
  // initializers only
}

QString BrowseService::renderBrowser(MediaItem::ItemType type, int id) const
{
  QList<MediaItem> items;
  int parentId = -1;
  if (type == MediaItem::Folder && id == 0) {
    items = mDB->rootFolders();
  } else if (type == MediaItem::Track) {
    return renderSong(id);
  } else if (id == 0) {
    items = MediaItem::getAll(type, true);
  } else {
    MediaItem item(type, id);
    if (item.type == MediaItem::Invalid) {
      return QString();
    }
    item.loadMetadata();
    items = item.children(nullptr, true);

    if (type == MediaItem::Folder) {
      parentId = item.parentId;
    }
  }

  QxtHtmlTemplate body;
  body.open(HttpServer::resourcePath("browse.html"));
  if (parentId >= 0) {
    body["parent"] = QString::number(parentId);
  }
  QxtHtmlTemplate line;
  line.open(HttpServer::resourcePath("browseitem.html"));
  QStringList result;
  bool queueAll = false;
  if (type == MediaItem::Folder || id < 1) {
    MediaItem::sort(items, QString());
  }
  for (const MediaItem& child : items) {
    if (child.type != MediaItem::Track && child.children(nullptr, false).isEmpty()) {
      continue;
    }
    if (child.type == MediaItem::Folder) {
      line["action"] = "browse";
    } else if (child.type == MediaItem::Album) {
      line["action"] = "browse/album";
    } else if (child.type == MediaItem::Artist) {
      line["action"] = "browse/artist";
    } else if (child.type == MediaItem::Playlist) {
      line["action"] = "browse/playlist";
    } else if (child.type == MediaItem::Track) {
      line["action"] = "browse/track";
      queueAll = true;
    } else {
      continue;
    }
    line["queue"] = (child.type == MediaItem::Track) ? "1" : "";
    line["id"] = QString::number(child.id);
    line["title"] = child.metadata.value("TITLE", child.name).toHtmlEscaped();
    QString cover = child.coverArtSource(nullptr, true);
    if (cover.isEmpty()) {
      line["cover"] = "";
    } else {
      line["cover"] = QStringLiteral("/rest/getCoverArt?id=%1").arg(cover);
    }
    result << line.render();
  }
  result << "</ul>";
  if (queueAll) {
    body["queueAll"] = "1";
    body["type"] = QString::number(int(type));
  }
  body["id"] = QString::number(id);
  body["items"] = result.join("");
  return body.render();
}

QString BrowseService::renderSong(int id) const
{
  MediaItem item(MediaItem::Track, id);
  item.loadMetadata();
  QxtHtmlTemplate tpl;
  tpl.open(HttpServer::resourcePath("song.html"));
  tpl["id"] = QString::number(item.id);
  tpl["title"] = item.metadata.value("TITLE", item.name).toHtmlEscaped();
  tpl["artist"] = item.metadata.value("ARTIST").toHtmlEscaped();
  QStringList keys = item.metadata.keys();
  std::sort(keys.begin(), keys.end());
  QString tagHtml = "<li> fullPath: " + item.fullPath + "</li>";
  for (const QString& key : keys) {
    tagHtml += "<li>" + key.toHtmlEscaped() + ": " + item.metadata.value(key).toHtmlEscaped() + "</li>";
  }
  auto images = MetadataProvider::readImages(item.fullPath);
  for (const QString& key : images.keys()) {
    tagHtml += QStringLiteral("<li>%2: <img src='/rest/getCoverArt?id=%1:%2' /></li>").arg(item.id).arg(key);
  }
  tpl["tags"] = tagHtml;
  return tpl.render();
}

void BrowseService::pageRequestedEvent(QxtWebRequestEvent* event)
{
  QString browseType = event->url.path().section('/', 1, 1);
  MediaItem::ItemType type = MediaItem::Folder;
  int id;
  if (browseType == "image") {
    MediaItem item(MediaItem::Track, event->url.path().section('/', -2, -2).toInt());
    QString key = event->url.path().section('/', -1, -1);
    auto images = MetadataProvider::readImages(item.fullPath);
    if (!images.isEmpty() && (key.isEmpty() || !images.contains(key))) {
      key = images.keys().first();
    }
    if (images.contains(key)) {
      QxtWebPageEvent* response = new QxtWebPageEvent(event->sessionID, event->requestID, images[key]);
      response->contentType = QMimeDatabase().mimeTypeForData(images[key]).name().toUtf8();
      postEvent(response);
    } else {
      postEvent(new QxtWebErrorEvent(event->sessionID, event->requestID, 404, "Not Found"));
    }
    return;
  } else if (browseType == "album") {
    type = MediaItem::Album;
  } else if (browseType == "artist") {
    type = MediaItem::Artist;
  } else if (browseType == "playlist") {
    type = MediaItem::Playlist;
  } else if (browseType == "track") {
    type = MediaItem::Track;
  }
  if (type == MediaItem::Folder) {
    id = browseType.toInt();
  } else {
    id = event->url.path().section('/', 2, 2).toInt();
  }

  QUrlQuery query(event->url);
  QString browser;
  if (browseType == "queue") {
    MapleSession* s = MapleSession::findSession(MapleSession::Web, event->cookies.values("mapleS").first());
    browser = QueueService::render(s);
  } else {
    browser = renderBrowser(type, id);
  }
  if (query.hasQueryItem("js")) {
    QxtWebPageEvent* response = new QxtWebPageEvent(event->sessionID, event->requestID, browser.toUtf8());
    response->contentType = "text/html;charset=utf-8";
    postEvent(response);
    return;
  }
  int nowPlaying = query.queryItemValue("nowPlaying").toInt();

  QxtHtmlTemplate tpl, content;
  tpl.open(HttpServer::resourcePath("template.html"));
  content.open(HttpServer::resourcePath("main.html"));
  content["browser"] = browser;

  MediaItem track(MediaItem::Track, nowPlaying);
  content["player"] = StreamService::renderPlayer(track);
  tpl["page"] = "main";
  tpl["content"] = content.render();
  QxtWebPageEvent* response = new QxtWebPageEvent(event->sessionID, event->requestID, tpl.render().toUtf8());
  response->contentType = "text/html;charset=utf-8";
  postEvent(response);
}
