#ifndef MD_QUEUESERVICE_H
#define MD_QUEUESERVICE_H

#include <QxtWebSlotService>
#include <QUrlQuery>
class MapleSession;

class QueueService : public QxtWebSlotService
{
Q_OBJECT
public:
  QueueService(QxtAbstractWebSessionManager* parent);

  static QString render(MapleSession* session);

public slots:
  void html(QxtWebRequestEvent* event);
  void get(QxtWebRequestEvent* event);
  void add(QxtWebRequestEvent* event);
  void remove(QxtWebRequestEvent* event);
  void clear(QxtWebRequestEvent* event);
  void seek(QxtWebRequestEvent* event);

private:
  MapleSession* session(QxtWebRequestEvent* event) const;
  void postResponse(QxtWebRequestEvent* event, const QVariantHash& response);
};

#endif
