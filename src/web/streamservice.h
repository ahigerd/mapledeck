#ifndef MD_STREAMSERVICE_H
#define MD_STREAMSERVICE_H

#include <QxtAbstractWebService>
#include "../mediaitem.h"

class StreamService : public QxtAbstractWebService
{
Q_OBJECT
public:
  StreamService(QxtAbstractWebSessionManager* parent);

  void pageRequestedEvent(QxtWebRequestEvent* event);
  static QString renderPlayer(MediaItem& item);
};

#endif
