HEADERS += src/web/httpserver.h   src/web/browseservice.h   src/web/statusservice.h   src/web/streamservice.h
SOURCES += src/web/httpserver.cpp src/web/browseservice.cpp src/web/statusservice.cpp src/web/streamservice.cpp

HEADERS += src/web/staticservice.h   src/web/eventservice.h   src/web/loginservice.h   src/web/mpdservice.h
SOURCES += src/web/staticservice.cpp src/web/eventservice.cpp src/web/loginservice.cpp src/web/mpdservice.cpp

HEADERS += src/web/subsonicservice.h   src/web/coverservice.h   src/web/queueservice.h
SOURCES += src/web/subsonicservice.cpp src/web/coverservice.cpp src/web/queueservice.cpp
