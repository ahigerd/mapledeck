#ifndef MD_BROWSESERVICE_H
#define MD_BROWSESERVICE_H

#include <QxtAbstractWebService>
#include "../mediaitem.h"

class BrowseService : public QxtAbstractWebService
{
Q_OBJECT
public:
  BrowseService(QxtAbstractWebSessionManager* parent);

  void pageRequestedEvent(QxtWebRequestEvent* event);

private:
  QString renderBrowser(MediaItem::ItemType type, int id) const;
  QString renderSong(int id) const;
  QString renderPlayer(int nowPlaying) const;
};

#endif
