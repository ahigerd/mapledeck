#include "jsonseq.h"
#include <cstring>

JsonSeq::JsonSeq(QObject* parent)
: JsonSeq(JsonSeq::Format_Seq, parent)
{
  // forwarded constructor only
}

JsonSeq::JsonSeq(JsonSeq::Format format, QObject* parent)
: QIODevice(parent), m_format(format)
{
  setOpenMode(QIODevice::ReadWrite);

  if (m_format == Format_Array) {
    queue << "[\n";
  }
}

JsonSeq::Format JsonSeq::format() const
{
  return m_format;
}

void JsonSeq::close()
{
  if (m_format == Format_Array) {
    queue << "]";
    emit bytesWritten(1);
    emit readyRead();
  }
  QIODevice::close();
}

void JsonSeq::push(const QByteArray& message)
{
  switch (m_format) {
    case Format_Seq:
      queue << ('\x1e' + message + '\n');
      break;
    case Format_Ld:
      queue << (message + '\n');
      break;
    case Format_Array:
      queue << ("  " + message + ",\n");
      break;
  }
  emit bytesWritten(queue.back().size());
  emit readyRead();
}

qint64 JsonSeq::readData(char* data, qint64 maxSize)
{
  qint64 written = 0;
  while (maxSize > 0 && queue.size() > 0) {
    QByteArray& msg = queue.first();
    qint64 size = msg.size();
    if (size <= maxSize) {
      (char*)std::memcpy(data, msg.constData(), size);
      queue.takeFirst();
      data += size;
      written += size;
      maxSize -= size;
    } else {
      std::memcpy(data, msg.constData(), maxSize);
      msg.remove(0, maxSize);
      written += maxSize;
      maxSize = 0;
    }
  }
  return written;
}

qint64 JsonSeq::writeData(const char*, qint64)
{
  // don't use write()
  return 0;
}

qint64 JsonSeq::bytesAvailable() const
{
  qint64 total = 0;
  for (const QByteArray& msg : queue) {
    total += msg.size();
  }
  return total;
}
