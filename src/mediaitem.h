#ifndef MD_MEDIAITEM_H
#define MD_MEDIAITEM_H

#include <QList>
#include <QString>
#include <QHash>
#include <QDateTime>
#include <functional>
class MdbxTxn;
class MdbxRecord;

class MediaItem
{
friend class MapleDB;

public:
  enum ItemType {
    Invalid,
    Folder,
    Track,
    Album,
    Playlist,
    Artist,
    Search,
    AnyCollection,
  };

  static MediaItem fromTaggedId(qint32 taggedId);
  static QList<MediaItem> getMultiple(ItemType type, const QList<qint64>& ids, bool getMeta = false, MdbxTxn* txn = nullptr);
  static QList<MediaItem> getAll(ItemType type, bool getMeta = false, MdbxTxn* txn = nullptr);
  static QByteArray getCoverArt(const QString& source, MdbxTxn* txn = nullptr);

  static void sort(QList<MediaItem>& items, const QString& key, Qt::SortOrder order = Qt::AscendingOrder);

  MediaItem(ItemType type = Invalid, qint64 id = -1);
  MediaItem(const MediaItem& other) = default;
  MediaItem(MediaItem&& other) = default;
  MediaItem& operator=(const MediaItem& other) = default;
  MediaItem& operator=(MediaItem&& other) = default;

  bool isValid() const;
  qint32 taggedId() const;
  ItemType type;
  qint64 id, pathId, parentId;
  QDateTime created, lastUpdated;
  QString name;
  QString fullPath;
  QHash<QString, QString> metadata;

  QList<MediaItem> children(MdbxTxn* txn = nullptr, bool getMeta = false) const;
  QList<MediaItem> containers(bool getMeta = false) const;
  QList<MediaItem> containers(MediaItem::ItemType type, bool getMeta = false) const;
  MediaItem album(bool getMeta = false) const;
  MediaItem artist(bool getMeta = false) const;
  QByteArray coverArt(MdbxTxn* txn = nullptr) const;
  QString coverArtSource(MdbxTxn* txn = nullptr, bool failFast = false) const;

  void loadMetadata(MdbxTxn* txn = nullptr, bool deep = true);
  bool isMetadataDirty() const;
  void saveMetadata(MdbxTxn* txn = nullptr);

private:
  MediaItem(ItemType type, const MdbxRecord& rec, bool getMeta);
  void loadMetadata(MdbxTxn* txn, const MdbxRecord& rec, bool deep = true);

  QList<qint64> childOrder;
  // QString mimeType;
  // double duration;
  bool loadedShallow, loadedDeep;
};

#endif
