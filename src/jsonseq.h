#ifndef MD_JSONSEQ_H
#define MD_JSONSEQ_H

#include <QIODevice>
#include <QJsonDocument>
#include <QHash>
#include <QMap>

class JsonSeq : public QIODevice
{
Q_OBJECT
public:
  enum Format {
    Format_Seq,
    Format_Ld,
    Format_Array,
  };
  JsonSeq(QObject* parent = nullptr);
  JsonSeq(Format format, QObject* parent = nullptr);

  Format format() const;

  void close();

  void push(const QByteArray& message);
  qint64 bytesAvailable() const;

protected:
  qint64 readData(char* data, qint64 maxSize);
  qint64 writeData(const char* data, qint64 maxSize);

private:
  QList<QByteArray> queue;
  Format m_format;
};

#endif
