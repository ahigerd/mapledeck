#include "mapleapp.h"

int main(int argc, char** argv)
{
  QCoreApplication::setApplicationName("Mapledeck");
  QCoreApplication::setApplicationVersion("0.0.1");
  QCoreApplication::setOrganizationName("Alkahest");
  QCoreApplication::setOrganizationDomain("com.alkahest");

  MapleApp app(argc, argv);
  return app.exec();
}
