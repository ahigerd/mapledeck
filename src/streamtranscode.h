#ifndef MD_STREAMTRANSCODE_H
#define MD_STREAMTRANSCODE_H

#include <QObject>
#include <QStringList>
#include <QPointer>
#include <QTimer>
#include "mediaitem.h"
class QIODevice;
class QProcess;

class StreamTranscode : public QObject
{
Q_OBJECT
public:
  enum OutputFormat {
    Copy,
    Wave,
    Vorbis,
    Opus,
    MP3,
    Riff,
  };
  static StreamTranscode* transcode(MediaItem& item, OutputFormat format, QObject* parent = nullptr);
  static QStringList splitCommand(const QString& cmd);

  StreamTranscode(QIODevice* source, OutputFormat format, QObject* parent = nullptr);
  ~StreamTranscode();

  QIODevice* output() const;
  QString mimeType() const;

protected:
  StreamTranscode(const QString& filename, const QString& decodeCmd, const QString& encodeCmd, QObject* parent = nullptr);
  StreamTranscode(const QString& filename, const QString& transcodeCmd, QObject* parent = nullptr);
  StreamTranscode(const QString& filename, QObject* parent = nullptr);

signals:
  void transcodeStarted();
  void transcodeFailed();
  void transcodeFinished();

private slots:
  void sourceReadyRead();
  void sourceWritten(qint64 bytes);
  void didWriteData();
  void handleStatus();

private:
  QString outputMime;
  QPointer<QIODevice> sourceDevice;
  QPointer<QIODevice> file;
  QPointer<QProcess> source;
  QPointer<QProcess> pipe;
  bool wroteData;
};

#endif
