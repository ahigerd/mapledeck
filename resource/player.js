import { query, ui, setHrefHandler } from "/static/mapledeck.js";
import { advance } from "/static/queue.js";

let seeking = false;
let loadStatusInterval = null;

function secToTime(sec) {
  const minutes = Math.floor(sec / 60);
  const seconds = ("0" + Math.floor(sec - (minutes * 60))).substr(-2);
  return minutes + ":" + seconds;
}

function updateTimer() {
  localStorage["time"] = ui.audio.currentTime;
  if (Math.round(ui.audio.currentTime * 10) > ui.timeSlider.max) {
    ui.timeSlider.max = Math.round(ui.audio.currentTime * 10);
  }
  ui.timer.innerText = secToTime(ui.audio.currentTime) + " / " + secToTime(ui.timeSlider.max / 10.0);
}

function onDurationChange() {
  // TODO: Firefox seems to not play nice when streaming
  if (isFinite(ui.audio.duration)) {
    ui.timeSlider.max = Math.round(ui.audio.duration * 10);
    updateTimer();
  }
}

function onTimeUpdate() {
  updateTimer();

  if (ui.audio.paused) {
    return;
  } else if (!seeking) {
    ui.timeSlider.value = ui.audio.currentTime * 10;
  } else if (ui.timeSlider.value == Math.round(ui.audio.currentTime * 10)) {
    seeking = false;
  }
}

function clearLoadStatusInterval() {
  if (loadStatusInterval) {
    window.clearInterval(loadStatusInterval);
    loadStatusInterval = null;
  }
}

async function onEnded() {
  clearLoadStatusInterval();
  console.log("ended", query.nowPlaying);
  localStorage["paused"] = query.nowPlaying;
  const nextTrack = await advance();
  if (nextTrack !== false) {
    play(nextTrack);
  } else {
    navigator.mediaSession.playbackState = "none";
  }
}

function onError(event) {
  console.log("error", event);
}

function checkLoadStatus(skipOnFail = true) {
  if (ui.audio.networkState == HTMLMediaElement.NETWORK_NO_SOURCE) {
    if (skipOnFail) {
      onEnded();
    }
  } else if (ui.audio.readyState != HTMLMediaElement.HAVE_NOTHING) {
    clearLoadStatusInterval();
  } else {
    return false;
  }
  return true;
}

function onLoadStart() {
  clearLoadStatusInterval();
  if (!checkLoadStatus(false)) {
    loadStatusInterval = window.setInterval(checkLoadStatus, 100);
  }
}

function onPlay() {
  clearLoadStatusInterval();
  ui.playButton.innerHTML = "Pause <span class='pauseIcon'></span>";
  console.log("paused clear");
  localStorage["paused"] = "";
  navigator.mediaSession.playbackState = "playing";
  updateSessionMetadata();
}

function onPaused() {
  clearLoadStatusInterval();
  ui.playButton.innerHTML = "Play <span class='playIcon'></span>";
  console.log("paused", query.nowPlaying);
  localStorage["paused"] = query.nowPlaying;
  navigator.mediaSession.playbackState = "paused";
  updateSessionMetadata();
}

function onPlayClicked(forceState = null) {
  console.log("click");
  clearLoadStatusInterval();
  seeking = false;
  if (ui.audio.paused || forceState === "play") {
    ui.audio.currentTime = ui.timeSlider.value / 10.0;
    ui.audio.play();
    updateSessionMetadata();
  } else {
    ui.audio.pause();
    if (forceState == "stop") {
      navigator.mediaSession.playbackState = "none";
      ui.timeSlider.value = 0;
    } else {
      ui.timeSlider.value = ui.audio.currentTime * 10;
    }
  }
}

function onTimeSlider() {
  seeking = true;
  const wasPaused = ui.audio.paused;
  if (!wasPaused) {
    ui.audio.pause();
  }
  if (ui.timeSlider.value == ui.timeSlider.max) {
    ui.audio.currentTime = isFinite(ui.audio.duration) ? ui.audio.duration : (ui.timeSlider.max / 10.0);
  } else {
    ui.audio.currentTime = ui.timeSlider.value / 10.0;
    if (!wasPaused) {
      ui.audio.play();
      updateSessionMetadata();
    }
  }
}

function onMediaSeek(time, isDelta = false) {
  if (isDelta) {
    ui.timeSlider.value += time * 10.0;
  } else {
    ui.timeSlider.value = time * 10.0;
  }
  onTimeSlider();
}

function updateSessionMetadata() {
  if (!ui.audio) {
    return;
  }
  navigator.mediaSession.playbackState = ui.audio.paused ? "paused" : "playing";
  if (ui.audioMeta) {
    navigator.mediaSession.metadata = new MediaMetadata(JSON.parse(ui.audioMeta.innerText));
  } else {
    navigator.mediaSession.metadata = new MediaMetadata();
  }
  navigator.mediaSession.setActionHandler("play", () => onPlayClicked("play"));
  navigator.mediaSession.setActionHandler("pause", () => onPlayClicked("pause"));
  navigator.mediaSession.setActionHandler("stop", () => onPlayClicked("stop"));
  navigator.mediaSession.setActionHandler("seekbackward", state => onMediaSeek(-state.seekOffset || -5, true));
  navigator.mediaSession.setActionHandler("seekforward", state => onMediaSeek(state.seekOffset || 5, true));
  navigator.mediaSession.setActionHandler("seekto", state => onMediaSeek(state.seekTime || 0, false));
  navigator.mediaSession.setActionHandler("nexttrack", state => onNextClicked());
  navigator.mediaSession.setActionHandler("previoustrack", state => onPrevClicked());
}

export function initPlayer(paused) {
  if (!Number(query.nowPlaying)) {
    if (ui.audio) {
      ui.audio.autoplay = false;
      ui.audio.pause();
    }
    onPaused();
    return;
  }

  ui.audio = null;
  ui.audioMeta = null;
  ui.scan();
  if (!ui.audio) {
    // Player doesn't have a current track
    return;
  }

  updateSessionMetadata();

  ui.audio.addEventListener("durationchange", onDurationChange);
  ui.audio.addEventListener("timeupdate", onTimeUpdate);
  ui.audio.addEventListener("ended", onEnded);
  ui.audio.addEventListener("play", onPlay);
  ui.audio.addEventListener("pause", onPaused);
  ui.audio.addEventListener("error", onError);
  ui.audio.addEventListener("loadstart", onLoadStart);

  ui.playButton.addEventListener("click", onPlayClicked);
  ui.timeSlider.addEventListener("input", onTimeSlider);

  if (localStorage["time"]) {
    ui.audio.currentTime = Number(localStorage["time"]);
  }
  onLoadStart();
  if (paused) {
    ui.audio.autoplay = false;
    ui.audio.pause();
    onPaused();
  } else {
    onPlay();
  }

  updateTimer();
}

function setNowPlaying(id) {
  query.nowPlaying = id;
  window.history.replaceState(window.history.state, "", `${window.location.href.split("?")[0]}?nowPlaying=${id}`);
}

function onNextClicked() {
  onEnded();
}

async function onPrevClicked() {
  const nextTrack = await advance(-1);
  if (nextTrack !== false) {
    play(nextTrack);
  } else {
    navigator.mediaSession.playbackState = "none";
  }
}

export async function play(id, queueIndex = -1) {
  const url = `/stream/${id}?js&queue=${queueIndex}`;
  const response = await fetch(url);
  ui.player.innerHTML = await response.text();
  localStorage["time"] = 0;
  setNowPlaying(id);
  initPlayer(false);
}

ui.player.play = play;
setHrefHandler("stream", (action, id) => play(id));

initPlayer(!!localStorage["paused"]);
