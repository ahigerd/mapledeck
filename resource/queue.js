import { query, ui, setHrefHandler } from "/static/mapledeck.js";

export const trackList = [];

async function updateTrackList(tracks) {
  trackList.splice(0, trackList.length, ...tracks);
  const current = tracks.findIndex(track => track.nowPlaying);
  const pending = (current >= 0) ? current - tracks.length : tracks.length;
  if (pending > 0) {
    ui.queueCount.innerHTML = ` (${pending})`;
  } else {
    ui.queueCount.innerHTML = "";
  }
  if (query.browse == "queue") {
    const response = await fetch("/queue/html");
    const text = await response.text();
    if (query.browse == "queue") {
      ui.browser.innerHTML = text;
    }
  }
}

export async function refresh() {
  const response = await fetch("/queue/get");
  const data = await response.json();
  await updateTrackList(data.tracks);
}

async function seek(index) {
  const response = await fetch(`/queue/seek?pos=${index}`);
  const data = await response.json();
  await refresh();
  if (data.ok) {
    const track = trackList.find(track => track.playing);
    if (track) {
      return track.id;
    }
  }
  return false;
}

export async function advance(offset = 1) {
  let next = trackList.findIndex(track => track.playing) + offset;
  if (next == 0 && trackList.length > 0 && trackList.id == query.nowPlaying) {
    next = 1;
  }
  return seek(next);
}

setHrefHandler("queue", async (action, id) => {
  const url = (id == "clear") ? "/queue/clear" : `/queue/add?id=${id}`;
  const response = await fetch(url);
  const data = await response.json();
  updateTrackList(data.tracks);
});

setHrefHandler("seekqueue", async (action, index) => {
  const trackId = await seek(index);
  if (trackId) {
    ui.player.play(trackId, index);
  }
});

setHrefHandler("delqueue", async (action, index) => {
  const url = `/queue/remove?pos=${index}`;
  const response = await fetch(url);
  const data = await response.json();
  if (data.ok) {
    updateTrackList(data.tracks);
  }
});
