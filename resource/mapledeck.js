class AutoUI {
  constructor(container) {
    this._container = container;
    this.scan();
  }

  _addKey(key, obj) {
    if (key && !this._keys.has(key) && key[0] != "_") {
      this._keys.add(key);
      this[key] = obj;
      return true;
    }
    return false;
  }

  _add(obj) {
    this._addKey(obj.id, obj) || this._addKey(obj.name, obj);
  }

  scan() {
    this._keys = new Set(["scan"]);

    // Scan the document for elements with id or name
    this._add(this._container);
    for (const el of this._container.querySelectorAll("[id],[name]")) {
      this._add(el);
    }

    // Release references to elements that are no longer in the document
    for (const key in this) {
      if (key[0] != "_" && !this._keys.has(key)) {
        this[key] = null;
      }
    }
  }
}
export const ui = new AutoUI(document.body);
window.ui = ui;

function parseQuery(url) {
  const q = url.indexOf("?");
  if (q < 0) {
    return {};
  }
  const result = {};
  for (const item of url.substr(q + 1).split("&")) {
    const colon = item.indexOf("=");
    if (colon < 0) {
      result[decodeURIComponent(item)] = true;
    } else {
      const key = decodeURIComponent(item.substr(0, colon));
      const value = decodeURIComponent(item.substr(colon + 1));
      result[key] = value;
    }
  }
  return result;
}

export const query = {
  ...(function() {
    const parts = window.location.href.split("/");
    if (parts.length < 4) {
      return {};
    }
    const data = { [parts[parts.length - 2]]: parts[parts.length - 1].split("?")[0] };
    if (parts[parts.length - 3] == "browse") {
      data["browse"] = parts[parts.length - 2] || "";
    }
    return data;
  })(),
  ...parseQuery(window.location.href),
};

export async function renderState(state) {
  if (!state.action && !ui) {
    return;
  }
  const url = `${state.action != "browse" ? "/browse/" : "/"}${state.action}/${state.id || ""}?js`;
  const response = await fetch(url);
  if (response.redirected && response.url.includes("login")) {
    window.location.href = response.url;
    return;
  }
  query.browse = state.action;
  query[state.action] = state.id;
  ui.browser.innerHTML = await response.text();
}

window.addEventListener("popstate", (event) => {
  renderState(event.state);
});

const hrefHandlers = {};

export function setHrefHandler(action, callback) {
  hrefHandlers[action] = callback;
}

document.body.addEventListener("click", function onBrowse(event) {
  let target = event.target;
  while (target && target.tagName != "A") {
    target = target.parentElement;
  }
  if (!target) {
    return;
  }
  event.preventDefault();
  event.stopPropagation();
  let href = target.href;
  if (href.includes("#")) {
    href = href.split("#")[1];
  } else {
    href = href.replace(window.location.origin, "");
  }
  const parts = href.split("/").filter(p => p && p != "#");
  const isBrowse = parts[0] == "browse";
  if (isBrowse && parts.length > 2) {
    parts.shift();
  }
  const [ action, id ] = parts;
  const handler = hrefHandlers[action];
  if (handler) {
    handler(action, id);
  } else {
    const qs = window.location.href.split("?")[1];
    const state = { action, id };
    query[action] = id || "";
    if (isBrowse) {
      delete query[query.browse];
      if (action == "browse") {
        query.browse = id || "";
      } else {
        query.browse = action || "";
      }
    } else {
      delete query.browse;
    }
    window.history.pushState(state, "", target.href + (qs ? "?" + qs : ""));
    renderState(state);
  }
});
